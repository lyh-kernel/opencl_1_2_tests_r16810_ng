/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#include "testBase.h"

extern cl_uint checksum;

int check_allocation_error(cl_context context, cl_device_id device_id, int error, cl_command_queue *queue);
double toMB(cl_ulong size_in);
size_t get_actual_allocation_size(cl_mem mem);


