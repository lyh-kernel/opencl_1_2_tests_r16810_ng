__kernel void sample_test( __global uint *buffer0,  __global uint *result, __global uint *array_sizes, uint per_item)
{
	int tid = get_global_id(0);
	uint r = 0;
	ulong i;
	for(i=tid*per_item; i<(1+tid)*per_item; i++) {
		if (i<array_sizes[0]) r += buffer0[i];
	}
	result[tid] = r;
}
