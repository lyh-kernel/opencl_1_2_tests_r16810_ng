/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef _testBase_h
#define _testBase_h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

#if !defined(_WIN32)
#include <unistd.h>
#endif

#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/typeWrappers.h"
#include "../../test_common/harness/testHarness.h"


#define MAX_NUMBER_TO_ALLOCATE 100

#define FAILED_CORRUPTED_QUEUE -2
#define FAILED_ABORT -1
#define FAILED_TOO_BIG 1
#define SUCCEEDED 0

#define BUFFER 1
#define IMAGE_READ 2
#define IMAGE_WRITE 4
#define BUFFER_NON_BLOCKING 8
#define IMAGE_READ_NON_BLOCKING 16
#define IMAGE_WRITE_NON_BLOCKING 32

#define test_error_abort(errCode,msg)	test_error_ret_abort(errCode,msg,errCode)
#define test_error_ret_abort(errCode,msg,retValue)	{ if( errCode != CL_SUCCESS ) { print_error( errCode, msg ); return FAILED_ABORT ; } }


#endif // _testBase_h



