/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/


#include "allocation_utils.h"

cl_command_queue reset_queue(cl_context context, cl_device_id device_id, cl_command_queue *queue, int *error)
{
  log_info("Invalid command queue. Releasing and recreating the command queue.\n");
  clReleaseCommandQueue(*queue);
	*queue = clCreateCommandQueue(context, device_id, 0, error);
  return *queue;
}

int check_allocation_error(cl_context context, cl_device_id device_id, int error, cl_command_queue *queue) {
  //log_info("check_allocation_error context=%p device_id=%p error=%d *queue=%p\n", context, device_id, error, *queue);  
  if ((error == CL_MEM_OBJECT_ALLOCATION_FAILURE ) || (error == CL_OUT_OF_RESOURCES ) || (error == CL_OUT_OF_HOST_MEMORY) || (error == CL_INVALID_IMAGE_SIZE)) {
    return FAILED_TOO_BIG;
  } else if (error == CL_INVALID_COMMAND_QUEUE) {
    *queue = reset_queue(context, device_id, queue, &error);
    if (CL_SUCCESS != error)
    {
      log_error("Failed to reset command queue after corrupted queue: %s\n", IGetErrorString(error));
      return FAILED_ABORT;
    }
    // Try again with smaller resources.
    return FAILED_TOO_BIG;
  } else if (error != CL_SUCCESS) {
    log_error("Allocation failed with %s.\n", IGetErrorString(error));
    return FAILED_ABORT;
  }
  return SUCCEEDED;   
}


double toMB(cl_ulong size_in) {
  return (double)size_in/(1024.0*1024.0);
}

size_t get_actual_allocation_size(cl_mem mem) {
  int error;
  cl_mem_object_type type;
  size_t size, width, height;
  
  error = clGetMemObjectInfo(mem, CL_MEM_TYPE, sizeof(type), &type, NULL);
  if (error) {
  	print_error(error, "clGetMemObjectInfo failed for CL_MEM_TYPE.");
    return 0;
  }
  
  if (type == CL_MEM_OBJECT_BUFFER) {
    error = clGetMemObjectInfo(mem, CL_MEM_SIZE, sizeof(size), &size, NULL);
    if (error) {
      print_error(error, "clGetMemObjectInfo failed for CL_MEM_SIZE.");
      return 0;
    }
    return size;
  } else if (type == CL_MEM_OBJECT_IMAGE2D) {
    error = clGetImageInfo(mem, CL_IMAGE_WIDTH, sizeof(width), &width, NULL);
    if (error) {
      print_error(error, "clGetMemObjectInfo failed for CL_IMAGE_WIDTH.");
      return 0;
    }
    error = clGetImageInfo(mem, CL_IMAGE_HEIGHT, sizeof(height), &height, NULL);
    if (error) {
      print_error(error, "clGetMemObjectInfo failed for CL_IMAGE_HEIGHT.");
      return 0;
    }
    return width*height*4*sizeof(cl_uint);
  }
  
  log_error("Invalid CL_MEM_TYPE: %d\n", type);
  return 0;
}


