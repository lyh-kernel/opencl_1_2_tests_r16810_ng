/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef TEST_CONFIG_H
#define TEST_CONFIG_H

#define MULTITHREAD 1

#define kVectorSizeCount    5
#define kStrangeVectorSizeCount 1
#define kMinVectorSize      0
#define kLargestVectorSize      (1 << (kVectorSizeCount-1))

#define kLastVectorSizeToTest (kVectorSizeCount + kStrangeVectorSizeCount)

// #define BUFFER_SIZE     (1024*1024)
// #define BUFFER_SIZE 
#define BUFFER_SIZE (64*1024) // minimum value for max constant buffer size
// 
extern size_t getBufferSize(cl_device_id device_id);
extern cl_ulong getBufferCount(cl_device_id device_id, size_t vecSize, size_t typeSize);
// could call
// CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
#define kPageSize       4096

extern int g_arrVecSizes[kVectorSizeCount+kStrangeVectorSizeCount];
extern int g_arrVecAligns[kLargestVectorSize+1];

#endif /* TEST_CONFIG_H */


