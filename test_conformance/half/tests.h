/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef TESTS_H
#define TESTS_H


int Test_vload_half( void );
int Test_vloada_half( void );
int Test_vstore_half( void );
int Test_vstorea_half( void );
int Test_vstore_half_rte( void );
int Test_vstorea_half_rte( void );
int Test_vstore_half_rtz( void );
int Test_vstorea_half_rtz( void );
int Test_vstore_half_rtp( void );
int Test_vstorea_half_rtp( void );
int Test_vstore_half_rtn( void );
int Test_vstorea_half_rtn( void );
int Test_roundTrip( void );

typedef cl_ushort (*f2h)( float );
typedef cl_ushort (*d2h)( double );
int Test_vStoreHalf_private( f2h referenceFunc, d2h referenceDoubleFunc, const char *roundName );
int Test_vStoreaHalf_private( f2h referenceFunc, d2h referenceDoubleFunc, const char *roundName );

#endif /* TESTS_H */


