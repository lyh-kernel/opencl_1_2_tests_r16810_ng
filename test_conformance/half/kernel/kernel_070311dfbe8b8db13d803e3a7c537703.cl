__kernel void test( const __global half *in, __global half *out )
{
   size_t i = get_global_id(0);
   vstore_half4( vload_half4(i, in),  i, out);
}
