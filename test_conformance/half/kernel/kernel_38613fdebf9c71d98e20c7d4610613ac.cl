__kernel void test( __global float4 *p, __global half *f )
{
   __local float4 data[32];
   size_t i = get_global_id(0);
   size_t lid = get_local_id(0);
   data[lid] = p[i];
   vstorea_half4_rtn( data[lid], i, f );
}
