__kernel void test( __global float2 *p, __global half *f )
{
   __private float2 data;
   size_t i = get_global_id(0);
   data = p[i];
   vstorea_half2( data, i, f );
}
