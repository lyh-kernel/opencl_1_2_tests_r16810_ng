__kernel void test( __global float *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstore_half( p[i], i, f );
}
