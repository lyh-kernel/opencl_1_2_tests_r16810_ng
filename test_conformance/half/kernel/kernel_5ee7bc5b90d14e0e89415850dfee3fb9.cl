__kernel void test( __global float2 *p, __global half *f )
{
   __local float2 data[32];
   size_t i = get_global_id(0);
   size_t lid = get_local_id(0);
   data[lid] = p[i];
   vstorea_half2( data[lid], i, f );
}
