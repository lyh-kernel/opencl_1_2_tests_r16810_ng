__kernel void test( __global float8 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstore_half8_rtz( p[i], i, f );
}
