__kernel void test( __global float3 *p, __global half *f )
{
   __private float3 data;
   size_t i = get_global_id(0);
   data = p[i];
   vstorea_half3_rtn( data, i, f );
   vstore_half_rtn( ((__global  float *)p)[4*i+3], 4*i+3, f);
}
