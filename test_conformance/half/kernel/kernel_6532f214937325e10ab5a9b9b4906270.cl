__kernel void test( __constant half *p, __global float2 *f )
{
   size_t i = get_global_id(0);
   f[i] = vloada_half2( i, p );
}
