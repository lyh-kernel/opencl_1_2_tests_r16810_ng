__kernel void test( const __global half *p, __global float3 *f )
{
   size_t i = get_global_id(0);
   f[i] = vloada_half3( i, p );
   ((__global float *)f)[4*i+3] = vloada_half(4*i+3,p);
}
