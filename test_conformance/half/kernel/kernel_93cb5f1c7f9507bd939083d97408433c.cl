__kernel void test( __global float3 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstorea_half3( p[i], i, f );
   vstore_half( ((__global  float *)p)[4*i+3], 4*i+3, f);
}
