__kernel void test( const __global half *p, __global float8 *f )
{
   size_t i = get_global_id(0);
   f[i] = vload_half8( i, p );
}
