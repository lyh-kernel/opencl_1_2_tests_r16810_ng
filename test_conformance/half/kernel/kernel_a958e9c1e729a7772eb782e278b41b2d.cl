__kernel void test( __global float8 *p, __global half *f )
{
   __local float8 data[32];
   size_t i = get_global_id(0);
   size_t lid = get_local_id(0);
   data[lid] = p[i];
   vstorea_half8_rte( data[lid], i, f );
}
