__kernel void test( __constant half *p, __global float8 *f )
{
   size_t i = get_global_id(0);
   f[i] = vloada_half8( i, p );
}
