__kernel void test( __global float4 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstorea_half4_rte( p[i], i, f );
}
