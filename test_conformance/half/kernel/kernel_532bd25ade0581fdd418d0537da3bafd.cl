__kernel void test( __global float3 *p, __global half *f )
{
   __local float3 data[32];
   size_t i = get_global_id(0);
   size_t lid = get_local_id(0);
   data[lid] = p[i];
   vstorea_half3_rtn( data[lid], i, f );
   vstore_half_rtn( ((__global float *)p)[4*i+3], 4*i+3, f);
}
