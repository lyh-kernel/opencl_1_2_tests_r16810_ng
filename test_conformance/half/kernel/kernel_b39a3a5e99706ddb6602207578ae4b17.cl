__kernel void test( __global float16 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstore_half16_rtp( p[i], i, f );
}
