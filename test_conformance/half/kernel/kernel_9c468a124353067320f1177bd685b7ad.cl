__kernel void test( __global float16 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstorea_half16( p[i], i, f );
}
