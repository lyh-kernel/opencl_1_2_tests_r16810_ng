__kernel void test( __constant half *p, __global float16 *f )
{
   size_t i = get_global_id(0);
   f[i] = vload_half16( i, p );
}
