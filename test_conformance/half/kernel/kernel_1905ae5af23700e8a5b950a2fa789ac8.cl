__kernel void test( __global float4 *p, __global half *f )
{
   size_t i = get_global_id(0);
   vstore_half4_rtn( p[i], i, f );
}
