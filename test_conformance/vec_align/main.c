/******************************************************************
//
//  OpenCL Conformance Tests
//
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include <stdio.h>
#include <stdlib.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <math.h>
#include <string.h>
#include "procs.h"
#include "../../test_common/harness/testHarness.h"

#if !defined(_WIN32)
#include <unistd.h>
#endif





basefn	basefn_list[] = {
    test_vec_align_array,
    test_vec_align_struct,
    test_vec_align_packed_struct,
    test_vec_align_struct_arr,
    test_vec_align_packed_struct_arr

};

const char    *basefn_names[] = {
    "vec_align_array",
    "vec_align_struct",
    "vec_align_packed_struct",
    "vec_align_struct_arr",
    "vec_align_packed_struct_arr",
    "all"
};

ct_assert((sizeof(basefn_names) / sizeof(basefn_names[0]) - 1) == (sizeof(basefn_list) / sizeof(basefn_list[0])));

int	num_fns = sizeof(basefn_names) / sizeof(char *);

int main(int argc, const char *argv[])
{
	return runTestHarness( argc, argv, num_fns, basefn_list, basefn_names, false, false, 0 );
}


