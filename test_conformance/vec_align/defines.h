/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/threadTesting.h"
#include "../../test_common/harness/typeWrappers.h"
#include "../../test_common/harness/conversions.h"
#include "../../test_common/harness/mt19937.h"


// 1,2,3,4,8,16 or
// 1,2,4,8,16,3
#define NUM_VECTOR_SIZES 6

extern int g_arrVecSizes[NUM_VECTOR_SIZES];
extern int g_arrVecSteps[NUM_VECTOR_SIZES];
extern bool g_wimpyMode;

extern const char * g_arrVecSizeNames[NUM_VECTOR_SIZES];
extern size_t g_arrVecAlignMasks[NUM_VECTOR_SIZES];

// Define the buffer size that we want to block our test with
#define BUFFER_SIZE (1024*1024)
#define KPAGESIZE 4096

extern ExplicitType types[];

extern const char *g_arrTypeNames[];
extern const size_t g_arrTypeSizes[];
