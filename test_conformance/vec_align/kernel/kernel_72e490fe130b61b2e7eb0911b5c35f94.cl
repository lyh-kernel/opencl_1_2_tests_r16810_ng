  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 s;    float2 vec;
float3 arrPost;
} testStruct;
__kernel void test_vec_align_packed_struct(__constant float2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
