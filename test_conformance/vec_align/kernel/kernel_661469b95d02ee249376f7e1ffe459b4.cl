  
typedef struct myUnpackedStruct { 
float arrPre[12];
    float3 vec;
float arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant float3 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
