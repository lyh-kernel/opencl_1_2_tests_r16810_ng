  
typedef struct myUnpackedStruct { 
    ulong4 vec;
ulong arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant ulong4 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
