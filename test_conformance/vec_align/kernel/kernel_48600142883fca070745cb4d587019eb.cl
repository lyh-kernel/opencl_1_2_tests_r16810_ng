  
__kernel void test_vec_align_array(__global int2 *source, __global ulong *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__global uchar *)(source+tid));
}
