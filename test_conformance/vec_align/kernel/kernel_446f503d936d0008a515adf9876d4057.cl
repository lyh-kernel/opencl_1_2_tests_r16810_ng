  
typedef struct myStruct { 
int3 tPre;
    int2 vec;
int arrPost[3];
} testStruct;
__kernel void test_vec_align_struct_arr(__global testStruct *source, __global ulong *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__global uchar *)&(source[tid].vec));
}
