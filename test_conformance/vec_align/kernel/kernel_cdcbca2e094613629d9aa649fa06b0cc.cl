  
typedef struct myUnpackedStruct { 
    ushort16 vec;
ushort arrPost[3];
} testStruct;
__kernel void test_vec_align_struct(__constant ushort16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
