  
typedef struct myUnpackedStruct { 
ushort3 tPre;
    ushort4 vec;
} testStruct;
__kernel void test_vec_align_struct(__constant ushort4 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
