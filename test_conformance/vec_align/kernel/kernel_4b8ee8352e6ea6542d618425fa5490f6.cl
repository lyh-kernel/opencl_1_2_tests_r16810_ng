  
typedef struct myUnpackedStruct { 
ushort arrPre[5];
    ushort3 vec;
ushort arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant ushort3 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
