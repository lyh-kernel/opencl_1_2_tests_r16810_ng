  
typedef struct myUnpackedStruct { 
ushort3 tPre;
    ushort2 vec;
ushort arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant ushort2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
