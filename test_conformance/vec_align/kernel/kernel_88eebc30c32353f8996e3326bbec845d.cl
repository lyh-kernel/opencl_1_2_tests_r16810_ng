  
typedef struct myUnpackedStruct { 
char c;
    ushort8 vec;
ushort arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant ushort8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
