  
typedef struct __attribute__ ((packed)) myPackedStruct { 
uint arrPre[5];
    uint8 vec;
} testStruct;
__kernel void test_vec_align_packed_struct(__constant uint8 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
