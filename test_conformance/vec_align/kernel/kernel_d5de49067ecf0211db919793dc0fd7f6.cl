  
typedef struct myUnpackedStruct { 
uint3 tPre;
    uint8 vec;
uint arrPost[3];
} testStruct;
__kernel void test_vec_align_struct(__constant uint8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
