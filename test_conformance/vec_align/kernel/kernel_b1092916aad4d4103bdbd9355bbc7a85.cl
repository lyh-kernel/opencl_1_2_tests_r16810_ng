  
typedef struct __attribute__ ((packed)) myPackedStruct { 
ushort arrPre[12];
    ushort3 vec;
ushort arrPost[5];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant ushort3 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
