  
typedef struct myUnpackedStruct { 
uchar arrPre[12];
    uchar2 vec;
uchar arrPost[3];
} testStruct;
__kernel void test_vec_align_struct(__constant uchar2 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
