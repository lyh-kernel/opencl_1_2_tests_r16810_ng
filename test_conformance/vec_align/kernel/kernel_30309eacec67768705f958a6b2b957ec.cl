  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 tPre;
    short2 vec;
short arrPost[12];
} testStruct;
__kernel void test_vec_align_packed_struct_arr(__global  testStruct *source, __global ulong *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__global uchar *)&(source[tid].vec) - (__global uchar *)&(source[0]));
}
