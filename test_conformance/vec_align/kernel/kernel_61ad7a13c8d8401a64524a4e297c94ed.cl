  
typedef struct __attribute__ ((packed)) myPackedStruct { 
ushort arrPre[5];
    ushort2 vec;
ushort arrPost[3];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant ushort2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
