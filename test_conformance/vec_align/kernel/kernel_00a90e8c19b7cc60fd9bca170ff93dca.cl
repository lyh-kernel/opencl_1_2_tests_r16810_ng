  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 s;    uint3 vec;
uint arrPost[5];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant uint3 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
