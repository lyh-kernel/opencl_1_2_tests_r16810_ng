  
typedef struct myUnpackedStruct { 
char c;
    float2 vec;
float3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant float2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
