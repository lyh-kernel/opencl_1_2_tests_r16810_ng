  
typedef struct myUnpackedStruct { 
int arrPre[5];
    int16 vec;
int arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant int16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
