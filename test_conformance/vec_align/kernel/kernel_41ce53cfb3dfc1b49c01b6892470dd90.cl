  
typedef struct myUnpackedStruct { 
short3 tPre;
    short2 vec;
short arrPost[3];
} testStruct;
__kernel void test_vec_align_struct(__constant short2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
