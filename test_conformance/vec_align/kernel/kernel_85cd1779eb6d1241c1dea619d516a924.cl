  
typedef struct myUnpackedStruct { 
short3 s;    ushort4 vec;
ushort arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant ushort4 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
