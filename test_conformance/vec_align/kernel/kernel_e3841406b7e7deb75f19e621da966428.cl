  
typedef struct myStruct { 
uchar arrPre[12];
    uchar2 vec;
uchar3 arrPost;
} testStruct;
__kernel void test_vec_align_struct_arr(__global testStruct *source, __global ulong *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__global uchar *)&(source[tid].vec));
}
