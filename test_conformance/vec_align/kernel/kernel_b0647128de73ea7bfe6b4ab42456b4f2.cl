  
typedef struct myUnpackedStruct { 
ulong arrPre[12];
    ulong8 vec;
ulong arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant ulong8 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
