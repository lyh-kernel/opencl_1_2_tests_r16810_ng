  
typedef struct myUnpackedStruct { 
float arrPre[12];
    float4 vec;
float arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant float4 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
