  
typedef struct myUnpackedStruct { 
float arrPre[5];
    float16 vec;
} testStruct;
__kernel void test_vec_align_struct(__constant float16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
