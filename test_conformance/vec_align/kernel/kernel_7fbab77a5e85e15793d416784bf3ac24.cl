  
typedef struct myUnpackedStruct { 
char c;
    uchar16 vec;
uchar arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant uchar16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
