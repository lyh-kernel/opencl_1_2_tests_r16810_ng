  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 s;    ushort4 vec;
ushort arrPost[3];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant ushort4 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
