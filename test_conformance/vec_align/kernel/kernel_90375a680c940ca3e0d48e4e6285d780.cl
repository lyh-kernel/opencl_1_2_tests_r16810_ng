  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 s;    ulong8 vec;
ulong3 arrPost;
} testStruct;
__kernel void test_vec_align_packed_struct(__constant ulong8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
