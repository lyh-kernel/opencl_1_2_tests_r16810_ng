  
typedef struct myUnpackedStruct { 
uint3 tPre;
    uint2 vec;
uint arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant uint2 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
