  
typedef struct myUnpackedStruct { 
short3 s;    float3 vec;
char cPost;
} testStruct;
__kernel void test_vec_align_struct(__constant float3 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
