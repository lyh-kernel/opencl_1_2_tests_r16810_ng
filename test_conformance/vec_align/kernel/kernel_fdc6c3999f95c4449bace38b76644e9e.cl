  
typedef struct myUnpackedStruct { 
long3 tPre;
    long8 vec;
long arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant long8 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
