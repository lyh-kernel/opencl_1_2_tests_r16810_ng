  
typedef struct myUnpackedStruct { 
long arrPre[5];
    long3 vec;
char cPost;
} testStruct;
__kernel void test_vec_align_struct(__constant long3 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
