  
typedef struct __attribute__ ((packed)) myPackedStruct { 
    ulong16 vec;
ulong arrPost[12];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant ulong16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
