  
typedef struct myUnpackedStruct { 
short3 s;    long8 vec;
long arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant long8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
