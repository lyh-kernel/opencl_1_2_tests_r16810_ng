  
typedef struct myUnpackedStruct { 
short3 s;    short8 vec;
char cPost;
} testStruct;
__kernel void test_vec_align_struct(__constant short8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
