  
typedef struct myUnpackedStruct { 
float3 tPre;
    float8 vec;
float3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant float8 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
