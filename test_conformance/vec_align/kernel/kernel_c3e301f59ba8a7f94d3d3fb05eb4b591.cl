  
typedef struct __attribute__ ((packed)) myPackedStruct { 
float arrPre[12];
    float3 vec;
float arrPost[12];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant float3 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
