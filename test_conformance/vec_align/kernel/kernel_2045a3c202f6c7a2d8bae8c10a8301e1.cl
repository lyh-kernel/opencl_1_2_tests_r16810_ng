  
typedef struct myUnpackedStruct { 
float arrPre[5];
    float16 vec;
char cPost;
} testStruct;
__kernel void test_vec_align_struct(__constant float16 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
