  
typedef struct myUnpackedStruct { 
long arrPre[5];
    long16 vec;
long arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant long16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
