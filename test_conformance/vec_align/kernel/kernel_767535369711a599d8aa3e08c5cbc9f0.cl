  
typedef struct myUnpackedStruct { 
ushort arrPre[5];
    ushort8 vec;
} testStruct;
__kernel void test_vec_align_struct(__constant ushort8 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
