  
typedef struct myUnpackedStruct { 
short3 s;    int8 vec;
int3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant int8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
