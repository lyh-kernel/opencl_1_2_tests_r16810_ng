  
typedef struct __attribute__ ((packed)) myPackedStruct { 
    float16 vec;
float arrPost[3];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant float16 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
