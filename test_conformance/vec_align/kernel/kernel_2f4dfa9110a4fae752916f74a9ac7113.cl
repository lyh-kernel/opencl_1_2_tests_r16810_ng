  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short3 s;    long4 vec;
long arrPost[5];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant long4 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
