  
typedef struct myUnpackedStruct { 
uchar3 tPre;
    uchar3 vec;
uchar arrPost[3];
} testStruct;
__kernel void test_vec_align_struct(__constant uchar3 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
