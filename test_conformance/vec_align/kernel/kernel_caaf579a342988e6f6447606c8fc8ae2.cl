  
typedef struct myUnpackedStruct { 
uchar3 tPre;
    uchar2 vec;
uchar3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant uchar2 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
