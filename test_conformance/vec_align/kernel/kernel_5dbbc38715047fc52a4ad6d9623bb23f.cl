  
typedef struct __attribute__ ((packed)) myPackedStruct { 
int arrPre[5];
    int4 vec;
int arrPost[12];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant int4 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec) - (__local uchar *)&test);
}
