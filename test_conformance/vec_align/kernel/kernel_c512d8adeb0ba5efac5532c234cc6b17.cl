  
typedef struct myUnpackedStruct { 
short3 s;    ulong16 vec;
ulong3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant ulong16 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
