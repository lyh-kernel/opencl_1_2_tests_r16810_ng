  
typedef struct __attribute__ ((packed)) myPackedStruct { 
short arrPre[5];
    short2 vec;
short arrPost[5];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant short2 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
