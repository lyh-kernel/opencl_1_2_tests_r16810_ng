  
typedef struct myUnpackedStruct { 
ulong arrPre[12];
    ulong2 vec;
ulong3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant ulong2 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
