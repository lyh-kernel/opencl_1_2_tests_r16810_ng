  
typedef struct myUnpackedStruct { 
short3 s;    char8 vec;
char arrPost[5];
} testStruct;
__kernel void test_vec_align_struct(__constant char8 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
