  
typedef struct myUnpackedStruct { 
char c;
    short16 vec;
short3 arrPost;
} testStruct;
__kernel void test_vec_align_struct(__constant short16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec));
}
