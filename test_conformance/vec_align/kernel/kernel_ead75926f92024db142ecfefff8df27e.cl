  
typedef struct myUnpackedStruct { 
int arrPre[12];
    int3 vec;
int arrPost[12];
} testStruct;
__kernel void test_vec_align_struct(__constant int3 *source, __global ulong *dest)
{
    __local testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__local uchar *)&(test.vec));
}
