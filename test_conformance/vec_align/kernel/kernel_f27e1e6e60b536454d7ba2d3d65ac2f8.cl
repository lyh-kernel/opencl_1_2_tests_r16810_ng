  
typedef struct __attribute__ ((packed)) myPackedStruct { 
char c;
    uchar16 vec;
uchar arrPost[12];
} testStruct;
__kernel void test_vec_align_packed_struct(__constant uchar16 *source, __global ulong *dest)
{
    __private testStruct test;
    int  tid = get_global_id(0);
    dest[tid] = (ulong)((__private uchar *)&(test.vec) - (__private uchar *)&test);
}
