/******************************************************************
//
//  OpenCL Conformance Tests
//
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include <stdlib.h>

size_t doReplace(char * dest, size_t destLength, const char * source, 
	      const char * stringToReplace1,  const char * replaceWith1,
	      const char * stringToReplace2, const char * replaceWith2);

size_t doSingleReplace(char * dest, size_t destLength, const char * source, 
		       const char * stringToReplace, const char * replaceWith);
