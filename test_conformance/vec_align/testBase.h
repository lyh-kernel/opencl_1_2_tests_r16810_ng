/******************************************************************
//
//  OpenCL Conformance Tests
//
//  Copyright:  (c) 2009-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef _testBase_h
#define _testBase_h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include "procs.h"

#endif // _testBase_h
