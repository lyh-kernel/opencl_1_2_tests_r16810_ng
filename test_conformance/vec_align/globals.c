/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "defines.h"


// 1,2,3,4,8,16 or
// 1,2,4,8,16,3
int g_arrVecSizes[NUM_VECTOR_SIZES] = {1,2,3,4,8,16};
int g_arrVecSteps[NUM_VECTOR_SIZES] = {1,2,4,4,8,16};
const char * g_arrVecSizeNames[NUM_VECTOR_SIZES] = {"", "2","3","4","8","16"};
size_t g_arrVecAlignMasks[NUM_VECTOR_SIZES] = {(size_t)0,
					       (size_t)0x1, // 2
					       (size_t)0x3, // 3
					       (size_t)0x3, // 4
					       (size_t)0x7, // 8
					       (size_t)0xf // 16
};

bool g_wimpyMode = false;

ExplicitType types[] = { kChar, kUChar, 
			 kShort, kUShort, 
			 kInt, kUInt, 
			 kLong, kULong, 
			 kFloat, kDouble,
			 kNumExplicitTypes };


const char *g_arrTypeNames[] = 
    {
	"char",  "uchar", 
	"short", "ushort", 
	"int",   "uint", 
	"long",  "ulong", 
	"float", "double"
    };

extern const size_t g_arrTypeSizes[] = 
    {
	1, 1,
	2, 2,
	4, 4,
	8, 8,
	4, 8
    };

