/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
 ******************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "procs.h"



int testBufferSize( cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements )
{
    cl_mem          memobj;
    cl_int          err;
    size_t          w = 32, h = 32, d = 32;
    size_t          retSize;
    size_t          elementSize = sizeof( cl_int );

    memobj = clCreateBuffer( context, (cl_mem_flags)(CL_MEM_READ_WRITE),  elementSize * w*h*d, NULL, &err);
    test_error(err, "clCreateBuffer failed.");

    err = clGetMemObjectInfo(memobj, CL_MEM_SIZE, sizeof( size_t ), (void *)&retSize, NULL);
    if ( err ){
        log_error( "Error calling clGetMemObjectInfo(): %d\n", err );
        clReleaseMemObject(memobj);
        return -1;
    }
    if ( (elementSize * w * h * d) != retSize ) {
        log_error( "Error in clGetMemObjectInfo() check of size\n" );
        clReleaseMemObject(memobj);
        return -1;
    }
    else{
        log_info( " CL_MEM_SIZE passed.\n" );
    }

    // cleanup
    clReleaseMemObject(memobj);

    return err;

}   // end testArrayElementSize()


// FIXME: need to test other flags

