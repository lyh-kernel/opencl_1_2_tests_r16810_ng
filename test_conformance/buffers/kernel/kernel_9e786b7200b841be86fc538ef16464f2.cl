__kernel void test_buffer_write_uchar8(__global uchar8 *src, __global uchar8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
