__kernel void test_buffer_read_int8(__global int8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = ((1<<16)+1);
}
