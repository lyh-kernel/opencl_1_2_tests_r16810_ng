__kernel void test_buffer_fill_uint4(__global uint4 *src, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
