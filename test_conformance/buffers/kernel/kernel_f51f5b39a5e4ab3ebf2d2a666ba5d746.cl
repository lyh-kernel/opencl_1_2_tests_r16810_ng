__kernel void test_buffer_read_int4(__global int4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = ((1<<16)+1);
}
