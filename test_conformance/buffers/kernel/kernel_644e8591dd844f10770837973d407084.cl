__kernel void test_buffer_read_uchar8(__global uchar8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (uchar)'w';
}
