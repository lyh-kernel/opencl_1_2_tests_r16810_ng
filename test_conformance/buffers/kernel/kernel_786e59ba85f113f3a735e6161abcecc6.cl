__kernel void test_buffer_fill_ulong(__global ulong *src, __global ulong *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
