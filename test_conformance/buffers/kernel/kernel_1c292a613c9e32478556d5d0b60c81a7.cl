__kernel void test_buffer_read_char(__global char *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (char)'w';
}
