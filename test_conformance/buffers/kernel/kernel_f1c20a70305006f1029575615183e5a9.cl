__kernel void test_buffer_fill_int2(__global int2 *src, __global int2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
