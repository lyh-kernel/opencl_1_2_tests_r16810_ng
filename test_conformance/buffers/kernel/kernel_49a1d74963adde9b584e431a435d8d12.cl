__kernel void test_buffer_fill_char(__global char *src, __global char *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
