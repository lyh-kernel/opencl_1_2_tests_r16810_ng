__kernel void readTest( __global char *inBuffer, __global char *outBuffer )
{
	int tid = get_global_id(0);
	outBuffer[ tid ] = inBuffer[ tid ];
}
