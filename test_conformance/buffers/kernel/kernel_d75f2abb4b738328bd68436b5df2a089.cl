__kernel void test_buffer_read_float8(__global float8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (float)3.40282346638528860e+38;
}
