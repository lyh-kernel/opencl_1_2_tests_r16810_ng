__kernel void test_buffer_fill_long2(__global long2 *src, __global long2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
