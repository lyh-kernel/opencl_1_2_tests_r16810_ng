__kernel void test_buffer_fill_short(__global short *src, __global short *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
