__kernel void test_buffer_read_short16(__global short16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (short)((1<<8)+1);
}
