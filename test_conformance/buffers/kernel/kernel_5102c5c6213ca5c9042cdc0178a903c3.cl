__kernel void test_buffer_fill_char16(__global char16 *src, __global char16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
