__kernel void test_buffer_fill_short8(__global short8 *src, __global short8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
