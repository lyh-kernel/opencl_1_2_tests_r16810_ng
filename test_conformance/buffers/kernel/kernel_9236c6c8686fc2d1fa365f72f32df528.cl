__kernel void test_mem_write(__global int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = dst[tid]+1;
}
