__kernel void test_buffer_fill_uint16(__global uint16 *src, __global uint16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
