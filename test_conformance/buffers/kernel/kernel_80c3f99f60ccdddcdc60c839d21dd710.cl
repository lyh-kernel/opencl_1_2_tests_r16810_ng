__kernel void test_buffer_write_uchar2(__global uchar2 *src, __global uchar2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
