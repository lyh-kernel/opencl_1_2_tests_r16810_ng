__kernel void test_buffer_fill_int8(__global int8 *src, __global int8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
