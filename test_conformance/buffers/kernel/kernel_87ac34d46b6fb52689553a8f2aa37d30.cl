__kernel void test_buffer_fill_ulong8(__global ulong8 *src, __global ulong8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
