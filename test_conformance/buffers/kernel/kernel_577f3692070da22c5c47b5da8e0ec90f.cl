__kernel void test_buffer_fill_uint2(__global uint2 *src, __global uint2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
