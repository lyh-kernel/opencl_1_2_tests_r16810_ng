__kernel void test_buffer_write_long8(__global long8 *src, __global long8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
