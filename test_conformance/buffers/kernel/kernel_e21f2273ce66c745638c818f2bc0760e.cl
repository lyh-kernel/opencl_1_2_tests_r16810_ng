__kernel void test_buffer_fill_uchar(__global uchar *src, __global uchar *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
