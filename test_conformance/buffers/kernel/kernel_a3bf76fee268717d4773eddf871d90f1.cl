__kernel void test_buffer_read_char8(__global char8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (char)'w';
}
