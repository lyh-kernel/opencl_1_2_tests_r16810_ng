__kernel void test_buffer_write_short16(__global short16 *src, __global short16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
