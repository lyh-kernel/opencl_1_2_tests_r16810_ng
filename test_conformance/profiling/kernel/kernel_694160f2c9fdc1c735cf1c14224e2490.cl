__kernel void test_stream_write_int2(__global int2 *src, __global int2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
