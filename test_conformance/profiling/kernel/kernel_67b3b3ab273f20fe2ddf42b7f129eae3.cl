__kernel void test_stream_write_char4(__global char4 *src, __global char4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
