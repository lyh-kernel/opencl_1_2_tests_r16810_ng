__kernel void test_stream_read_short8(__global short8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (short)((1<<8)+1);
}
