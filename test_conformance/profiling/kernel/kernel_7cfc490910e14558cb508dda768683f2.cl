__kernel void test_stream_write_char8(__global char8 *src, __global char8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
