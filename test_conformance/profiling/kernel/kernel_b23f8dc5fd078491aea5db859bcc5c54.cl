__kernel void test_stream_read_long2(__global long2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = ((1L<<32)+1L);
}
