__kernel void test_stream_read_ulong4(__global ulong4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = ((1UL<<32)+1UL);
}
