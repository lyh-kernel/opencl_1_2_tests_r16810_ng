__kernel void test_stream_write_float8(__global float8 *src, __global float8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
