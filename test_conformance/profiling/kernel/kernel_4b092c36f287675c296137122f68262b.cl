__kernel void test_stream_read_int16(__global int16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = ((1<<16)+1);
}
