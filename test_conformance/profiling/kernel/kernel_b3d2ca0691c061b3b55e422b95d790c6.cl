__kernel void test_stream_write_ushort2(__global ushort2 *src, __global ushort2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
