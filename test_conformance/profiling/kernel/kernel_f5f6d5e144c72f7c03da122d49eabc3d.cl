__kernel void test_stream_write_uint8(__global uint8 *src, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
