__kernel void test_stream_read_ushort2(__global ushort2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (ushort)((1<<8)+1);
}
