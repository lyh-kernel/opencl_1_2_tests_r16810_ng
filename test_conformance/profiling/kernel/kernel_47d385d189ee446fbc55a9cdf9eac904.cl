__kernel void test_stream_write_int16(__global int16 *src, __global int16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
