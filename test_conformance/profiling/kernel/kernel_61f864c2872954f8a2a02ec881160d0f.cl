__kernel void test_stream_write_ulong4(__global ulong4 *src, __global ulong4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
