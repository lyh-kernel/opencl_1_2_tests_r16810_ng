__kernel void test_stream_read_char2(__global char2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (char)'w';
}
