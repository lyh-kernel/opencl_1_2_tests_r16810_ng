__kernel void test_stream_read_char16(__global char16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (char)'w';
}
