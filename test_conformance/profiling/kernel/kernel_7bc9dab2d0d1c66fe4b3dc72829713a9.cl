__kernel void test_stream_write_ushort4(__global ushort4 *src, __global ushort4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
