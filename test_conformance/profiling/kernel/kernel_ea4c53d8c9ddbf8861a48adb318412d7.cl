__kernel void test_stream_write_ushort8(__global ushort8 *src, __global ushort8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
