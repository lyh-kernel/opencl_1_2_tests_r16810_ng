__kernel void test_stream_write_ushort16(__global ushort16 *src, __global ushort16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
