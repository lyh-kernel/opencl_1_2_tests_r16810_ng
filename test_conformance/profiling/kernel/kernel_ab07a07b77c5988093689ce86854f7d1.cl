__kernel void test_stream_write_short4(__global short4 *src, __global short4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
