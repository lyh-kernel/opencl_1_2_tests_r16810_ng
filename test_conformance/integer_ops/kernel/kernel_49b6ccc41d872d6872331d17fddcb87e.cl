__kernel void sample_test(__global int *sourceA, __global int *sourceB, __global int *sourceC, __global int *destValues)
{
    int  tid = get_global_id(0);
    int sA = sourceA[ tid ];
    int sB = sourceB[ tid ];
    int sC = sourceC[ tid ];
    int dst = mad_sat( sA, sB, sC );
	 destValues[ tid ] = dst;

}
