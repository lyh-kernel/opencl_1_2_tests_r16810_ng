__kernel void test( __global uchar *inOut, __global char * control )
{
	size_t tid = get_global_id(0);

   uchar inOutVal = inOut[tid];

   if( control[tid] == 0 )
		inOutVal++;
   else if( control[tid] == 1 )
		++inOutVal;
   else if( control[tid] == 2 )
		inOutVal--;
   else // if( control[tid] == 3 )
		--inOutVal;

   inOut[tid] = inOutVal;
}
