__kernel void sample_test(__global int *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    int4 tmp = vload4( tid, destValues );
    tmp -= ( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
