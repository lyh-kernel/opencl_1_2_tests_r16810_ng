__kernel void test_upsample(__global short *sourceA, __global ushort *sourceB, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
