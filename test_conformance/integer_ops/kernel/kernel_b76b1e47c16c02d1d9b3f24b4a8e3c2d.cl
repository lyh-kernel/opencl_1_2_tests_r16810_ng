__kernel void test_add_sat_uint16(__global uint16 *srcA, __global uint16 *srcB, __global uint16 *dst)
{
    int  tid = get_global_id(0);

    uint16 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
