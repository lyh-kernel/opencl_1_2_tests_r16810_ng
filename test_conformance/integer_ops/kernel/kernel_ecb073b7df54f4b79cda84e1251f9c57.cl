__kernel void test(__global ulong4 *srcA, __global ulong4 *srcB, __global ulong4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (srcA[tid].s0 < srcB[tid].s0) ? srcA[tid] : srcB[tid];
}
