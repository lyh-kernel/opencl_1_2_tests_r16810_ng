__kernel void test(__global ushort4 *srcA, __global ushort4 *srcB, __global ushort4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] << srcB[tid].s0;
}
