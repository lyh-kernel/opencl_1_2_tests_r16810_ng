__kernel void sample_test(__global ushort *sourceA, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    ushort8 tmp = vload8( tid, destValues );
    tmp /= ( vload8( tid, sourceA ) );
    vstore8( tmp, tid, destValues );

}
