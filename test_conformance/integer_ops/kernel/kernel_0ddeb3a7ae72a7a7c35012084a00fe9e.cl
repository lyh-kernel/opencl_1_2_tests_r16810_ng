__kernel void test_sub_sat_uint4(__global uint4 *srcA, __global uint4 *srcB, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    uint4 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
