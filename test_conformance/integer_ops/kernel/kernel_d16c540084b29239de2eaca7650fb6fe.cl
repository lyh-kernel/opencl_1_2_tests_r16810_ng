__kernel void test_upsample(__global int8 *sourceA, __global uint8 *sourceB, __global long8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
