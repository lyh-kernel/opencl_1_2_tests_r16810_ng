__kernel void sample_test(__global uchar *sourceA, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    uchar3 tmp = vload3( tid, destValues );
    tmp = clz( vload3( tid, sourceA ) );
    vstore3( tmp, tid, destValues );

}
