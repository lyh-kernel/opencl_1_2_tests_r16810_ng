__kernel void sample_test(__global char *sourceA, __global char *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] |= ( sourceA[tid] );
}
