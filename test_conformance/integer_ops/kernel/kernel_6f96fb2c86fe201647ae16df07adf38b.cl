__kernel void test_int16_mad24(__global int16 *srcA, __global int16 *srcB, __global int16 *srcC, __global int16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mad24(srcA[tid], srcB[tid], srcC[tid]);
}
