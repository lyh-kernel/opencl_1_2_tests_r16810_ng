__kernel void test_popcount_short3(__global short *srcA, __global short *dst)
{
    int  tid = get_global_id(0);

    short3 sA;
    sA = vload3( tid, srcA );
    short3 dstVal = popcount(sA);
	 vstore3( dstVal, tid, dst );
}
