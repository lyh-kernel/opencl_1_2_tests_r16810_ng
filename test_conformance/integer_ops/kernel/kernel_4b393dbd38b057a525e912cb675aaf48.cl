__kernel void test_abs_uint3(__global uint *srcA, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint3 tmp = abs(vload3(tid, srcA));
    vstore3(tmp, tid, dst);
}
