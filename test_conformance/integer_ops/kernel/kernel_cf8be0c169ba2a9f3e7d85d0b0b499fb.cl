__kernel void test_sub_sat_short8(__global short8 *srcA, __global short8 *srcB, __global short8 *dst)
{
    int  tid = get_global_id(0);

    short8 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
