__kernel void test(__global char *srcA, __global char *srcB, __global char *srcC, __global char *dst)
{
	int  tid = get_global_id(0);

	char3 valA = vload3( tid, srcA);
	char valB = srcB[ tid ];
	char valC = srcC[ tid ];
	char3 destVal = valC ? valA : valB;
	vstore3( destVal, tid, dst );
}
