__kernel void sample_test(__global long *sourceA, __global long *sourceB, __global long *sourceC, __global long *destValues)
{
    int  tid = get_global_id(0);
    long sA = sourceA[ tid ];
    long sB = sourceB[ tid ];
    long sC = sourceC[ tid ];
    long dst = mad_sat( sA, sB, sC );
	 destValues[ tid ] = dst;

}
