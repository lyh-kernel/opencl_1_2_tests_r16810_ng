__kernel void test_abs_int16(__global int16 *srcA, __global uint16 *dst)
{
    int  tid = get_global_id(0);

    uint16 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
