__kernel void test_int2_mul24(__global int2 *srcA, __global int2 *srcB, __global int2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mul24(srcA[tid], srcB[tid]);
}
