__kernel void test_absdiff_ushort4(__global ushort4 *srcA, __global ushort4 *srcB, __global ushort4 *dst)
{
    int  tid = get_global_id(0);

    ushort4 sA, sB;
    sA = srcA[tid];
    sB = srcB[tid];
    ushort4 dstVal = abs_diff(sA, sB);
	 dst[ tid ] = dstVal;
}
