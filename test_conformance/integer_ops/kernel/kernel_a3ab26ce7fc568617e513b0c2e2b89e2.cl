__kernel void test_abs_int(__global int *srcA, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
