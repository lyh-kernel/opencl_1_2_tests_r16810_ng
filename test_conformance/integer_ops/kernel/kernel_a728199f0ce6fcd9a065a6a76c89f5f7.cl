__kernel void test_add_sat_ushort2(__global ushort2 *srcA, __global ushort2 *srcB, __global ushort2 *dst)
{
    int  tid = get_global_id(0);

    ushort2 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
