__kernel void test_int3_mul24(__global int *srcA, __global int *srcB, __global int *dst)
{
    int  tid = get_global_id(0);
    int3 tmp = mul24(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
