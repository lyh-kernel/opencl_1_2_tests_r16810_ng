__kernel void sample_test(__global uchar *sourceA, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    uchar16 tmp = vload16( tid, destValues );
    tmp %= ( vload16( tid, sourceA ) );
    vstore16( tmp, tid, destValues );

}
