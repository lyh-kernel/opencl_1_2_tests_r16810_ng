__kernel void test_add_sat_uchar8(__global uchar8 *srcA, __global uchar8 *srcB, __global uchar8 *dst)
{
    int  tid = get_global_id(0);

    uchar8 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
