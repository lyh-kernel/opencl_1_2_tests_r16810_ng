__kernel void test_upsample(__global uchar *sourceA, __global uchar *sourceB, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    vstore3( upsample( vload3(tid,sourceA), vload3(tid, sourceB) ), tid, destValues);

}
