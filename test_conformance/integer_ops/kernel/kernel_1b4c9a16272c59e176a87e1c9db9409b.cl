__kernel void sample_test(__global ulong *sourceA, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    ulong4 tmp = vload4( tid, destValues );
    tmp -= ( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
