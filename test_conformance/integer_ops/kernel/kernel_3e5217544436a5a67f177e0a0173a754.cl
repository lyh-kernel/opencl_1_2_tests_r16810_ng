__kernel void sample_test(__global long *sourceA, __global long *destValues)
{
    int  tid = get_global_id(0);
    long3 tmp = vload3( tid, destValues );
    tmp ^= ( vload3( tid, sourceA ) );
    vstore3( tmp, tid, destValues );

}
