__kernel void test(__global char8 *srcA, __global char8 *srcB, __global char8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] != srcB[tid];
}
