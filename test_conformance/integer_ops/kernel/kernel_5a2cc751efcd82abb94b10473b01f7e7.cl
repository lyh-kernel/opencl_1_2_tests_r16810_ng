__kernel void test_int16_mul24(__global int16 *srcA, __global int16 *srcB, __global int16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mul24(srcA[tid], srcB[tid]);
}
