__kernel void test( __global long *inOut, __global char * control )
{
	size_t tid = get_global_id(0);

   long2 inOutVal = vload2( tid, inOut );

   if( control[tid] == 0 )
		inOutVal++;
   else if( control[tid] == 1 )
		++inOutVal;
   else if( control[tid] == 2 )
		inOutVal--;
   else // if( control[tid] == 3 )
		--inOutVal;

   vstore2( inOutVal, tid, inOut );
}
