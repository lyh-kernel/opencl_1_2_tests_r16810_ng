__kernel void sample_test(__global ushort *sourceA, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    ushort2 tmp = vload2( tid, destValues );
    tmp %= ( vload2( tid, sourceA ) );
    vstore2( tmp, tid, destValues );

}
