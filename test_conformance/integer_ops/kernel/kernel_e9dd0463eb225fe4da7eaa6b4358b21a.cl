__kernel void test(__global ulong2 *srcA, __global ulong2 *srcB, __global ulong2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] * srcB[tid];
}
