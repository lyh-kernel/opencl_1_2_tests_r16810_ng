__kernel void test_upsample(__global short16 *sourceA, __global ushort16 *sourceB, __global int16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
