__kernel void sample_test(__global ushort *sourceA, __global ushort *sourceB, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    ushort sA = sourceA[ tid ];
    ushort sB = sourceB[ tid ];
    ushort dst = max( sA, sB );
	 destValues[ tid ] = dst;

}
