__kernel void test_upsample(__global short2 *sourceA, __global ushort2 *sourceB, __global int2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
