__kernel void sample_test(__global uchar *sourceA, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] ^= ( sourceA[tid] );
}
