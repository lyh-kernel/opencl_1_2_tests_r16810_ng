__kernel void sample_test(__global uint *sourceA, __global uint *destValues)
{
    int  tid = get_global_id(0);
    uint4 tmp = vload4( tid, destValues );
    tmp -= ( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
