__kernel void test_sub_sat_char(__global char *srcA, __global char *srcB, __global char *dst)
{
    int  tid = get_global_id(0);

    char tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
