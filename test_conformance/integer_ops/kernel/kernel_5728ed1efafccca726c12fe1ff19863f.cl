__kernel void sample_test(__global long *sourceA, __global long *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = clz( sourceA[tid] );
}
