__kernel void test(__global ushort8 *srcA, __global ushort8 *srcB, __global short8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] == srcB[tid];
}
