__kernel void test_upsample(__global char16 *sourceA, __global uchar16 *sourceB, __global short16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
