__kernel void test_upsample(__global uint16 *sourceA, __global uint16 *sourceB, __global ulong16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
