__kernel void test_upsample(__global int *sourceA, __global uint *sourceB, __global long *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
