__kernel void sample_test(__global uint *sourceA, __global uint *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] /= ( sourceA[tid] );
}
