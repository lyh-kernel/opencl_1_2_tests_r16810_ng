__kernel void test(__global char16 *srcA, __global char16 *srcB, __global char16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] & srcB[tid];
}
