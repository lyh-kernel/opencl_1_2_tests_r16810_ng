__kernel void test_abs_long8(__global long8 *srcA, __global ulong8 *dst)
{
    int  tid = get_global_id(0);

    ulong8 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
