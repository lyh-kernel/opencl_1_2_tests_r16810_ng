__kernel void sample_test(__global uchar *sourceA, __global uchar *sourceB, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    uchar sA = sourceA[ tid ];
    uchar sB = sourceB[ tid ];
    uchar dst = min( sA, sB );
	 destValues[ tid ] = dst;

}
