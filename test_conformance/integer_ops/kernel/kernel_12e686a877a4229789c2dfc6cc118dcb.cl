__kernel void test_sub_sat_uchar3(__global uchar *srcA, __global uchar *srcB, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar3 tmp = sub_sat(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
