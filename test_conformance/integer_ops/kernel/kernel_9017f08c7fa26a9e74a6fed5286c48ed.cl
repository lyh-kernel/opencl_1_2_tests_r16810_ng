__kernel void sample_test(__global ulong *sourceA, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    ulong8 tmp = vload8( tid, destValues );
    tmp |= ( vload8( tid, sourceA ) );
    vstore8( tmp, tid, destValues );

}
