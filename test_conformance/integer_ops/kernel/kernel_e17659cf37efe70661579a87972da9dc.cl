__kernel void sample_test(__global uint16 *sourceA, __global uint16 *sourceB, __global uint16 *destValues)
{
    int  tid = get_global_id(0);
    uint16 sA = sourceA[ tid ];
    uint16 sB = sourceB[ tid ];
    uint16 dst = rotate( sA, sB );
	 destValues[ tid ] = dst;

}
