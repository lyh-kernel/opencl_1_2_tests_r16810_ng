__kernel void test_popcount_ulong(__global ulong *srcA, __global ulong *dst)
{
    int  tid = get_global_id(0);

    ulong sA;
    sA = srcA[tid];
    ulong dstVal = popcount(sA);
	 dst[ tid ] = dstVal;
}
