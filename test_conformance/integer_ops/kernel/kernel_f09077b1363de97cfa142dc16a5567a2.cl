__kernel void test_abs_ushort4(__global ushort4 *srcA, __global ushort4 *dst)
{
    int  tid = get_global_id(0);

    ushort4 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
