__kernel void sample_test(__global ulong *sourceA, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = clz( sourceA[tid] );
}
