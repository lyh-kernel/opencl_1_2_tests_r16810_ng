__kernel void test(__global short16 *srcA, __global short16 *srcB, __global short16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (srcA[tid].s0 < srcB[tid].s0) ? srcA[tid] : srcB[tid];
}
