__kernel void test_sub_sat_uint3(__global uint *srcA, __global uint *srcB, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint3 tmp = sub_sat(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
