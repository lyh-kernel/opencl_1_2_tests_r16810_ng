__kernel void test_abs_uchar3(__global uchar *srcA, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar3 tmp = abs(vload3(tid, srcA));
    vstore3(tmp, tid, dst);
}
