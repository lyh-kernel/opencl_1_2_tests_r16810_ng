__kernel void test_int8_mul24(__global int8 *srcA, __global int8 *srcB, __global int8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mul24(srcA[tid], srcB[tid]);
}
