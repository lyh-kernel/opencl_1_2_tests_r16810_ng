__kernel void test_popcount_long8(__global long8 *srcA, __global long8 *dst)
{
    int  tid = get_global_id(0);

    long8 sA;
    sA = srcA[tid];
    long8 dstVal = popcount(sA);
	 dst[ tid ] = dstVal;
}
