__kernel void test_absdiff_ushort8(__global ushort8 *srcA, __global ushort8 *srcB, __global ushort8 *dst)
{
    int  tid = get_global_id(0);

    ushort8 sA, sB;
    sA = srcA[tid];
    sB = srcB[tid];
    ushort8 dstVal = abs_diff(sA, sB);
	 dst[ tid ] = dstVal;
}
