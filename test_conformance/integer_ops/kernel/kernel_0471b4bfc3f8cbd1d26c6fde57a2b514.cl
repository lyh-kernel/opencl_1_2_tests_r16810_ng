__kernel void test_absdiff_uchar8(__global uchar8 *srcA, __global uchar8 *srcB, __global uchar8 *dst)
{
    int  tid = get_global_id(0);

    uchar8 sA, sB;
    sA = srcA[tid];
    sB = srcB[tid];
    uchar8 dstVal = abs_diff(sA, sB);
	 dst[ tid ] = dstVal;
}
