__kernel void test_upsample(__global uchar *sourceA, __global uchar *sourceB, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
