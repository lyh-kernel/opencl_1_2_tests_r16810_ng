__kernel void sample_test(__global long2 *sourceA, __global long2 *sourceB, __global long2 *destValues)
{
    int  tid = get_global_id(0);
    long2 sA = sourceA[ tid ];
    long2 sB = sourceB[ tid ];
    long2 dst = max( sA, sB );
	 destValues[ tid ] = dst;

}
