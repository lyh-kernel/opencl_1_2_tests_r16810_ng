__kernel void test_upsample(__global uint8 *sourceA, __global uint8 *sourceB, __global ulong8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
