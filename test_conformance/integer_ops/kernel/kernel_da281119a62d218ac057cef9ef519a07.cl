__kernel void test_absdiff_uchar3(__global uchar *srcA, __global uchar *srcB, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar3 sA, sB;
    sA = vload3( tid, srcA );
    sB = vload3( tid, srcB );
    uchar3 dstVal = abs_diff(sA, sB);
	 vstore3( dstVal, tid, dst );
}
