__kernel void test_upsample(__global char8 *sourceA, __global uchar8 *sourceB, __global short8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
