__kernel void test_upsample(__global uchar8 *sourceA, __global uchar8 *sourceB, __global ushort8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
