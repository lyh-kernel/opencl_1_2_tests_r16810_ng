__kernel void sample_test(__global ulong *sourceA, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    ulong2 tmp = vload2( tid, destValues );
    tmp |= ( vload2( tid, sourceA ) );
    vstore2( tmp, tid, destValues );

}
