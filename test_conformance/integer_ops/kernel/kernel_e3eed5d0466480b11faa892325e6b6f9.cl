__kernel void test_sub_sat_uchar16(__global uchar16 *srcA, __global uchar16 *srcB, __global uchar16 *dst)
{
    int  tid = get_global_id(0);

    uchar16 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
