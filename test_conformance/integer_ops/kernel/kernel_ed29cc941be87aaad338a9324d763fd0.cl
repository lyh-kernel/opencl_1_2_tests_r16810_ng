__kernel void sample_test(__global short *sourceA, __global short *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] /= ( sourceA[tid] );
}
