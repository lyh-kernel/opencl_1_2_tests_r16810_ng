__kernel void sample_test(__global ulong8 *sourceA, __global ulong8 *sourceB, __global ulong8 *destValues)
{
    int  tid = get_global_id(0);
    ulong8 sA = sourceA[ tid ];
    ulong8 sB = sourceB[ tid ];
    ulong8 dst = rotate( sA, sB );
	 destValues[ tid ] = dst;

}
