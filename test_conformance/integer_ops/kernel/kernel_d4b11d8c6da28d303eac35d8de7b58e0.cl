__kernel void sample_test(__global char *sourceA, __global char *destValues)
{
    int  tid = get_global_id(0);
    char8 tmp = vload8( tid, destValues );
    tmp /= ( vload8( tid, sourceA ) );
    vstore8( tmp, tid, destValues );

}
