__kernel void test(__global ulong16 *srcA, __global ulong16 *srcB, __global long16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] && srcB[tid];
}
