__kernel void sample_test(__global short *sourceA, __global short *destValues)
{
    int  tid = get_global_id(0);
    short8 tmp = vload8( tid, destValues );
    tmp -= ( vload8( tid, sourceA ) );
    vstore8( tmp, tid, destValues );

}
