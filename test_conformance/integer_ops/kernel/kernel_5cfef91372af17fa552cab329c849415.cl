__kernel void test(__global short8 *srcA, __global short8 *srcB, __global short8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] && srcB[tid];
}
