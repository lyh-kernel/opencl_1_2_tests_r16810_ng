__kernel void sample_test(__global char8 *sourceA, __global char8 *sourceB, __global char8 *destValues)
{
    int  tid = get_global_id(0);
    char8 sA = sourceA[ tid ];
    char8 sB = sourceB[ tid ];
    char8 dst = max( sA, sB );
	 destValues[ tid ] = dst;

}
