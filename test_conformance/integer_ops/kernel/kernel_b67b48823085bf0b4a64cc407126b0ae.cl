__kernel void test_sub_sat_ushort3(__global ushort *srcA, __global ushort *srcB, __global ushort *dst)
{
    int  tid = get_global_id(0);

    ushort3 tmp = sub_sat(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
