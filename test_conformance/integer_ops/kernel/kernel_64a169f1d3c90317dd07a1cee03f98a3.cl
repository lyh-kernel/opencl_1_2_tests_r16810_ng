__kernel void test_add_sat_short16(__global short16 *srcA, __global short16 *srcB, __global short16 *dst)
{
    int  tid = get_global_id(0);

    short16 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
