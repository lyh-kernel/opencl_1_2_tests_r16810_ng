__kernel void test_sub_sat_int4(__global int4 *srcA, __global int4 *srcB, __global int4 *dst)
{
    int  tid = get_global_id(0);

    int4 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
