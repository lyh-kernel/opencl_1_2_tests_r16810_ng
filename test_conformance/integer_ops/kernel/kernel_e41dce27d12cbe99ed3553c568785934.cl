__kernel void sample_test(__global int *sourceA, __global int *sourceB, __global int *destValues)
{
    int  tid = get_global_id(0);
    int3 sA = vload3( tid, sourceA );
    int3 sB = vload3( tid, sourceB );
    int3 dst = rhadd( sA, sB );
	 vstore3( dst, tid, destValues );

}
