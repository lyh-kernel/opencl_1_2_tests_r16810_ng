__kernel void test(__global char4 *srcA, __global char4 *srcB, __global char *srcC, __global char4 *dst)
{
	int  tid = get_global_id(0);

	char4 valA = srcA[ tid ];
	char4 valB = srcB[ tid ];
	char valC = srcC[ tid ];
	char4 destVal = valC ? valA : valB;
	dst[ tid ] = destVal;
}
