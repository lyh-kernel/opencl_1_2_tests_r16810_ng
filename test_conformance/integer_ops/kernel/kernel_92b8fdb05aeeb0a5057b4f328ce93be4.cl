__kernel void sample_test(__global uchar8 *sourceA, __global uchar8 *sourceB, __global uchar8 *sourceC, __global uchar8 *destValues)
{
    int  tid = get_global_id(0);
    uchar8 sA = sourceA[ tid ];
    uchar8 sB = sourceB[ tid ];
    uchar8 sC = sourceC[ tid ];
    uchar8 dst = mad_sat( sA, sB, sC );
	 destValues[ tid ] = dst;

}
