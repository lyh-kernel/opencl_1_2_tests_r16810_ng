__kernel void test_abs_uchar2(__global uchar2 *srcA, __global uchar2 *dst)
{
    int  tid = get_global_id(0);

    uchar2 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
