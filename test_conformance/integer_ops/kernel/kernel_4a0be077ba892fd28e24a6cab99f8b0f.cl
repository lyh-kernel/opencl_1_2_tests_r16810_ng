__kernel void test_sub_sat_ulong16(__global ulong16 *srcA, __global ulong16 *srcB, __global ulong16 *dst)
{
    int  tid = get_global_id(0);

    ulong16 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
