__kernel void test(__global int8 *srcA, __global int8 *srcB, __global int8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] / srcB[tid];
}
