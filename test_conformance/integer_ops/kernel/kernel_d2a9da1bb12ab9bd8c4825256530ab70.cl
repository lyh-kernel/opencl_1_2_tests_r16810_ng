__kernel void test_add_sat_short4(__global short4 *srcA, __global short4 *srcB, __global short4 *dst)
{
    int  tid = get_global_id(0);

    short4 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
