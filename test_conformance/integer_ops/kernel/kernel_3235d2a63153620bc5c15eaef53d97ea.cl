__kernel void test_popcount_long(__global long *srcA, __global long *dst)
{
    int  tid = get_global_id(0);

    long sA;
    sA = srcA[tid];
    long dstVal = popcount(sA);
	 dst[ tid ] = dstVal;
}
