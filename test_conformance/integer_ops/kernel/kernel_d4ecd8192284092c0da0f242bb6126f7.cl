__kernel void test_absdiff_ushort16(__global ushort16 *srcA, __global ushort16 *srcB, __global ushort16 *dst)
{
    int  tid = get_global_id(0);

    ushort16 sA, sB;
    sA = srcA[tid];
    sB = srcB[tid];
    ushort16 dstVal = abs_diff(sA, sB);
	 dst[ tid ] = dstVal;
}
