__kernel void test(__global uint4 *srcA, __global uint4 *srcB, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (srcA[tid].s0 < srcB[tid].s0) ? srcA[tid] : srcB[tid];
}
