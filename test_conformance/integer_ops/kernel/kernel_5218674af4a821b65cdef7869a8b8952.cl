__kernel void test_int8_mul24(__global uint8 *srcA, __global uint8 *srcB, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mul24(srcA[tid], srcB[tid]);
}
