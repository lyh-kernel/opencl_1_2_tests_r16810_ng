__kernel void test_sub_sat_short2(__global short2 *srcA, __global short2 *srcB, __global short2 *dst)
{
    int  tid = get_global_id(0);

    short2 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
