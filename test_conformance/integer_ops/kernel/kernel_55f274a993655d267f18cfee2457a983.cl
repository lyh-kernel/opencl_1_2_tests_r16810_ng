__kernel void test_add_sat_long3(__global long *srcA, __global long *srcB, __global long *dst)
{
    int  tid = get_global_id(0);

    long3 tmp = add_sat(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
