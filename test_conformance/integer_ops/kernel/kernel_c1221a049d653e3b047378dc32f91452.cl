__kernel void test_upsample(__global ushort *sourceA, __global ushort *sourceB, __global uint *destValues)
{
    int  tid = get_global_id(0);
    vstore3( upsample( vload3(tid,sourceA), vload3(tid, sourceB) ), tid, destValues);

}
