__kernel void test_abs_ushort2(__global ushort2 *srcA, __global ushort2 *dst)
{
    int  tid = get_global_id(0);

    ushort2 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
