__kernel void sample_test(__global ulong4 *sourceA, __global ulong4 *sourceB, __global ulong4 *sourceC, __global ulong4 *destValues)
{
    int  tid = get_global_id(0);
    ulong4 sA = sourceA[ tid ];
    ulong4 sB = sourceB[ tid ];
    ulong4 sC = sourceC[ tid ];
    ulong4 dst = mad_sat( sA, sB, sC );
	 destValues[ tid ] = dst;

}
