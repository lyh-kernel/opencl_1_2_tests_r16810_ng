__kernel void sample_test(__global short *sourceA, __global short *destValues)
{
    int  tid = get_global_id(0);
    short2 tmp = vload2( tid, destValues );
    tmp = clz( vload2( tid, sourceA ) );
    vstore2( tmp, tid, destValues );

}
