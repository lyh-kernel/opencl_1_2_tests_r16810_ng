__kernel void test(__global char16 *srcA, __global char *srcB, __global char *srcC, __global char16 *dst)
{
	int  tid = get_global_id(0);

	char16 valA = srcA[ tid ];
	char valB = srcB[ tid ];
	char valC = srcC[ tid ];
	char16 destVal = valC ? valA : valB;
	dst[ tid ] = destVal;
}
