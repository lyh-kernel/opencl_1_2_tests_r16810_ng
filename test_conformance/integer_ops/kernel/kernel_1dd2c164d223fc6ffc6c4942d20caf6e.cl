__kernel void test_abs_long(__global long *srcA, __global ulong *dst)
{
    int  tid = get_global_id(0);

    ulong tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
