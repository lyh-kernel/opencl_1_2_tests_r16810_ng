__kernel void test(__global char8 *srcA, __global char *srcB, __global char *srcC, __global char8 *dst)
{
	int  tid = get_global_id(0);

	char8 valA = srcA[ tid ];
	char valB = srcB[ tid ];
	char valC = srcC[ tid ];
	char8 destVal = valC ? valA : valB;
	dst[ tid ] = destVal;
}
