__kernel void test_abs_uint8(__global uint8 *srcA, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    uint8 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
