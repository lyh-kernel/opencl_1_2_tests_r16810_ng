__kernel void test_upsample(__global short8 *sourceA, __global ushort8 *sourceB, __global int8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
