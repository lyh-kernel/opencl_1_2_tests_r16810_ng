__kernel void test(__global uchar *srcA, __global uchar *srcB, __global uchar *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] << srcB[tid];
}
