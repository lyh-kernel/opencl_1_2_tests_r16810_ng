__kernel void sample_test(__global ushort *sourceA, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] ^= ( sourceA[tid] );
}
