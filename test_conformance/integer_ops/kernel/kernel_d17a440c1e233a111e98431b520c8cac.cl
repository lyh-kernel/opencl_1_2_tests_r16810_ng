__kernel void sample_test(__global ushort *sourceA, __global ushort *destValues)
{
    int  tid = get_global_id(0);
    ushort4 tmp = vload4( tid, destValues );
    tmp = clz( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
