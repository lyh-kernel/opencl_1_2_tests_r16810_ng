__kernel void sample_test(__global ulong4 *sourceA, __global ulong4 *sourceB, __global ulong4 *destValues)
{
    int  tid = get_global_id(0);
    ulong4 sA = sourceA[ tid ];
    ulong4 sB = sourceB[ tid ];
    ulong4 dst = mul_hi( sA, sB );
	 destValues[ tid ] = dst;

}
