__kernel void test_upsample(__global ushort2 *sourceA, __global ushort2 *sourceB, __global uint2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
