__kernel void test_abs_int2(__global int2 *srcA, __global uint2 *dst)
{
    int  tid = get_global_id(0);

    uint2 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
