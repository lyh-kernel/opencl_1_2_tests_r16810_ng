__kernel void test_upsample(__global int2 *sourceA, __global uint2 *sourceB, __global long2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
