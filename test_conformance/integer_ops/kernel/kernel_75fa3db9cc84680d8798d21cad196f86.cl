__kernel void sample_test(__global int *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] ^= ( sourceA[tid] );
}
