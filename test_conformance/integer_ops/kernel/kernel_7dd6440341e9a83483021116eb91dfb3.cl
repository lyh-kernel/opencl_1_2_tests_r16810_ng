__kernel void test(__global uint8 *srcA, __global uint8 *srcB, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] << srcB[tid].s0;
}
