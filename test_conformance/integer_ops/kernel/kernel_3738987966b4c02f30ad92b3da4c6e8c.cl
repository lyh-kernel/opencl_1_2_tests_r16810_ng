__kernel void test_int_mul24(__global int *srcA, __global int *srcB, __global int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mul24(srcA[tid], srcB[tid]);
}
