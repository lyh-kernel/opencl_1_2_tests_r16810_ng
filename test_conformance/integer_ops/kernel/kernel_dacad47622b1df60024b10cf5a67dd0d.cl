__kernel void test_popcount_uint3(__global uint *srcA, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint3 sA;
    sA = vload3( tid, srcA );
    uint3 dstVal = popcount(sA);
	 vstore3( dstVal, tid, dst );
}
