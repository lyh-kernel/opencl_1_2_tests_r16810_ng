__kernel void test_add_sat_int2(__global int2 *srcA, __global int2 *srcB, __global int2 *dst)
{
    int  tid = get_global_id(0);

    int2 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
