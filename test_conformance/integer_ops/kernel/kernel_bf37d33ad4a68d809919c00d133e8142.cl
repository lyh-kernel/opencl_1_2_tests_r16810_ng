__kernel void test_add_sat_uchar2(__global uchar2 *srcA, __global uchar2 *srcB, __global uchar2 *dst)
{
    int  tid = get_global_id(0);

    uchar2 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
