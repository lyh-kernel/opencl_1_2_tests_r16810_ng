__kernel void test(__global ulong8 *srcA, __global ulong8 *srcB, __global ulong8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (srcA[tid].s0 < srcB[tid].s0) ? srcA[tid] : srcB[tid];
}
