__kernel void test_upsample(__global int4 *sourceA, __global uint4 *sourceB, __global long4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
