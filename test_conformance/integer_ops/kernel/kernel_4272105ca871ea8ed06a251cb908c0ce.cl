__kernel void test(__global long16 *srcA, __global long16 *srcB, __global long16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] << srcB[tid];
}
