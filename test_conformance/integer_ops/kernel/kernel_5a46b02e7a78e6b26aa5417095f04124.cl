__kernel void test_absdiff_int3(__global int *srcA, __global int *srcB, __global uint *dst)
{
    int  tid = get_global_id(0);

    int3 sA, sB;
    sA = vload3( tid, srcA );
    sB = vload3( tid, srcB );
    uint3 dstVal = abs_diff(sA, sB);
	 vstore3( dstVal, tid, dst );
}
