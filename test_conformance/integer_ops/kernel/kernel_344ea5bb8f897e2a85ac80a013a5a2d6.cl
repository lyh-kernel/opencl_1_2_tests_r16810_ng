__kernel void sample_test(__global uchar *sourceA, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    uchar4 tmp = vload4( tid, destValues );
    tmp *= ( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
