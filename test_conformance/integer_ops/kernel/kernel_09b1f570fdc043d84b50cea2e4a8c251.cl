__kernel void test_add_sat_uchar(__global uchar *srcA, __global uchar *srcB, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
