__kernel void test_sub_sat_int3(__global int *srcA, __global int *srcB, __global int *dst)
{
    int  tid = get_global_id(0);

    int3 tmp = sub_sat(vload3(tid, srcA), vload3(tid, srcB));
    vstore3(tmp, tid, dst);
}
