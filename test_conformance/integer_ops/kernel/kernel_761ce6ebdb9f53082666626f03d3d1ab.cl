__kernel void test_popcount_char(__global char *srcA, __global char *dst)
{
    int  tid = get_global_id(0);

    char sA;
    sA = srcA[tid];
    char dstVal = popcount(sA);
	 dst[ tid ] = dstVal;
}
