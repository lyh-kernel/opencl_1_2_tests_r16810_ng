__kernel void test_popcount_uchar3(__global uchar *srcA, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar3 sA;
    sA = vload3( tid, srcA );
    uchar3 dstVal = popcount(sA);
	 vstore3( dstVal, tid, dst );
}
