__kernel void test_add_sat_long16(__global long16 *srcA, __global long16 *srcB, __global long16 *dst)
{
    int  tid = get_global_id(0);

    long16 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
