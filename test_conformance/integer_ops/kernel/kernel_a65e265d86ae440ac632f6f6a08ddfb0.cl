__kernel void sample_test(__global short *sourceA, __global short *destValues)
{
    int  tid = get_global_id(0);
    short3 tmp = vload3( tid, destValues );
    tmp += ( vload3( tid, sourceA ) );
    vstore3( tmp, tid, destValues );

}
