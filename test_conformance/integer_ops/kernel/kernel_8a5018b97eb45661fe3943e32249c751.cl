__kernel void test_sub_sat_char8(__global char8 *srcA, __global char8 *srcB, __global char8 *dst)
{
    int  tid = get_global_id(0);

    char8 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
