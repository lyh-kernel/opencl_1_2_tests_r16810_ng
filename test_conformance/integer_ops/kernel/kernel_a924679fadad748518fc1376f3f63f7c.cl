__kernel void sample_test(__global long *sourceA, __global long *destValues)
{
    int  tid = get_global_id(0);
    long2 tmp = vload2( tid, destValues );
    tmp *= ( vload2( tid, sourceA ) );
    vstore2( tmp, tid, destValues );

}
