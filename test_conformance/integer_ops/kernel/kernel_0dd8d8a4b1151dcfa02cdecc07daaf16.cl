__kernel void test_sub_sat_char16(__global char16 *srcA, __global char16 *srcB, __global char16 *dst)
{
    int  tid = get_global_id(0);

    char16 tmp = sub_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
