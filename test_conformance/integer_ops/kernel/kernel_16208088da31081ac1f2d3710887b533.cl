__kernel void test_add_sat_uint8(__global uint8 *srcA, __global uint8 *srcB, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    uint8 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
