__kernel void test_abs_long16(__global long16 *srcA, __global ulong16 *dst)
{
    int  tid = get_global_id(0);

    ulong16 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
