__kernel void test_abs_short(__global short *srcA, __global ushort *dst)
{
    int  tid = get_global_id(0);

    ushort tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
