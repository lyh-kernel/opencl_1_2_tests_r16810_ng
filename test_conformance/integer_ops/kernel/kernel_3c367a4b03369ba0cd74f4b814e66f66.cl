__kernel void sample_test(__global long *sourceA, __global long *destValues)
{
    int  tid = get_global_id(0);
    long8 tmp = vload8( tid, destValues );
    tmp *= ( vload8( tid, sourceA ) );
    vstore8( tmp, tid, destValues );

}
