__kernel void test(__global char *srcA, __global char2 *srcB, __global char2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] - srcB[tid];
}
