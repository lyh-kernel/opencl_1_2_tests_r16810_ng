__kernel void test_abs_char(__global char *srcA, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
