__kernel void sample_test(__global long *sourceA, __global long *sourceB, __global long *destValues)
{
    int  tid = get_global_id(0);
    long sA = sourceA[ tid ];
    long sB = sourceB[ tid ];
    long dst = rotate( sA, sB );
	 destValues[ tid ] = dst;

}
