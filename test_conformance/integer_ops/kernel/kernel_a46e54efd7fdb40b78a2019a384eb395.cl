__kernel void sample_test(__global char *sourceA, __global char *destValues)
{
    int  tid = get_global_id(0);
    char16 tmp = vload16( tid, destValues );
    tmp = clz( vload16( tid, sourceA ) );
    vstore16( tmp, tid, destValues );

}
