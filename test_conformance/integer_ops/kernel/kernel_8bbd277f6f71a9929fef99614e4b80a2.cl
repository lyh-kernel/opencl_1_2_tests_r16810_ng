__kernel void test(__global long8 *srcA, __global long8 *srcB, __global long8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] | srcB[tid];
}
