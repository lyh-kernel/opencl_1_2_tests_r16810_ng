__kernel void test_upsample(__global uchar2 *sourceA, __global uchar2 *sourceB, __global ushort2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
