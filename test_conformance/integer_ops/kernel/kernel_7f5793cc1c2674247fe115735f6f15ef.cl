__kernel void test_upsample(__global short4 *sourceA, __global ushort4 *sourceB, __global int4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
