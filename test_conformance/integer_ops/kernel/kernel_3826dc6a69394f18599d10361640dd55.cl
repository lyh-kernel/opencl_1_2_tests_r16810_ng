__kernel void test_upsample(__global char4 *sourceA, __global uchar4 *sourceB, __global short4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
