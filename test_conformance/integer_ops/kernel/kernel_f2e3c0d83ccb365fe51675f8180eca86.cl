__kernel void sample_test(__global int4 *sourceA, __global int4 *sourceB, __global int4 *destValues)
{
    int  tid = get_global_id(0);
    int4 sA = sourceA[ tid ];
    int4 sB = sourceB[ tid ];
    int4 dst = mul_hi( sA, sB );
	 destValues[ tid ] = dst;

}
