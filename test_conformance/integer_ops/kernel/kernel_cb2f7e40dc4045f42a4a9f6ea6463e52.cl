__kernel void test_abs_int4(__global int4 *srcA, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    uint4 tmp = abs(srcA[tid]);
    dst[tid] = tmp;
}
