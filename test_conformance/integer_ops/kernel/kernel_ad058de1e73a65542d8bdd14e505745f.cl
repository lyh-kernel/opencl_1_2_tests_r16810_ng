__kernel void test(__global ushort16 *srcA, __global ushort16 *srcB, __global ushort16 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (srcA[tid].s0 < srcB[tid].s0) ? srcA[tid] : srcB[tid];
}
