__kernel void test(__global ulong *srcA, __global ulong *srcB, __global ulong *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] - srcB[tid];
}
