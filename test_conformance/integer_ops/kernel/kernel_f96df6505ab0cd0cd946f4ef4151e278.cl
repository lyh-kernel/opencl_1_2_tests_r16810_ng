__kernel void test(__global ulong4 *srcA, __global ulong4 *srcB, __global long4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] <= srcB[tid];
}
