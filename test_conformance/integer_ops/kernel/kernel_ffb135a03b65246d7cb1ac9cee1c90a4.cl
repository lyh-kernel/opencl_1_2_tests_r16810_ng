__kernel void sample_test(__global short *sourceA, __global short *destValues)
{
    int  tid = get_global_id(0);
    short4 tmp = vload4( tid, destValues );
    tmp &= ( vload4( tid, sourceA ) );
    vstore4( tmp, tid, destValues );

}
