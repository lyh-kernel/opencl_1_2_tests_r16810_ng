__kernel void test(__global char *srcA, __global char4 *srcB, __global char4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] ^ srcB[tid];
}
