__kernel void sample_test(__global short2 *sourceA, __global short2 *sourceB, __global short2 *sourceC, __global short2 *destValues)
{
    int  tid = get_global_id(0);
    short2 sA = sourceA[ tid ];
    short2 sB = sourceB[ tid ];
    short2 sC = sourceC[ tid ];
    short2 dst = mad_sat( sA, sB, sC );
	 destValues[ tid ] = dst;

}
