__kernel void test_add_sat_ulong2(__global ulong2 *srcA, __global ulong2 *srcB, __global ulong2 *dst)
{
    int  tid = get_global_id(0);

    ulong2 tmp = add_sat(srcA[tid], srcB[tid]);
    dst[tid] = tmp;
}
