__kernel void test_upsample(__global char2 *sourceA, __global uchar2 *sourceB, __global short2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
