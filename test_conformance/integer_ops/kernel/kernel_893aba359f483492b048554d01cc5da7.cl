__kernel void test_upsample(__global uint *sourceA, __global uint *sourceB, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
