__kernel void sample_test(__global char *sourceA, __global char *destValues)
{
    int  tid = get_global_id(0);
    char2 tmp = vload2( tid, destValues );
    tmp = clz( vload2( tid, sourceA ) );
    vstore2( tmp, tid, destValues );

}
