__kernel void sample_test(__global short *sourceA, __global short *sourceB, __global short *destValues)
{
    int  tid = get_global_id(0);
    short sA = sourceA[ tid ];
    short sB = sourceB[ tid ];
    short dst = max( sA, sB );
	 destValues[ tid ] = dst;

}
