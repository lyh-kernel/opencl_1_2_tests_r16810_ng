__kernel void sample_test(__global ushort8 *sourceA, __global ushort8 *sourceB, __global ushort8 *destValues)
{
    int  tid = get_global_id(0);
    ushort8 sA = sourceA[ tid ];
    ushort8 sB = sourceB[ tid ];
    ushort8 dst = hadd( sA, sB );
	 destValues[ tid ] = dst;

}
