__kernel void test_popcount_ushort(__global ushort *srcA, __global ushort *dst)
{
    int  tid = get_global_id(0);

    ushort sA;
    sA = srcA[tid];
    ushort dstVal = popcount(sA);
	 dst[ tid ] = dstVal;
}
