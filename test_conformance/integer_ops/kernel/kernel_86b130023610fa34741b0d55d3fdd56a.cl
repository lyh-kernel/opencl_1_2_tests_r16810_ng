__kernel void test_upsample(__global char *sourceA, __global uchar *sourceB, __global short *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = upsample( sourceA[tid], sourceB[tid] );

}
