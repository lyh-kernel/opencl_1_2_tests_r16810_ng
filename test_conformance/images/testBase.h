/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef _testBase_h
#define _testBase_h

#include "../../test_common/harness/testHarness.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/clImageHelper.h"

#include "image_helpers.h"

// Amount to offset pixels for checking normalized reads
#define NORM_OFFSET 0.1f

enum TypesToTest
{
	kTestInt = ( 1 << 0 ),
	kTestUInt = ( 1 << 1 ),
	kTestFloat = ( 1 << 2 ),
	kTestAllTypes = kTestInt | kTestUInt | kTestFloat
};

// For the clCopyImage test
enum MethodsToTest
{
    k1D = ( 1 << 0 ),
	k2D	= ( 1 << 1 ),
    k1DArray = ( 1 << 2 ),
    k2DArray = ( 1 << 3 ),
	k3D = ( 1 << 4 ),
	k2DTo3D = ( 1 << 5 ),
	k3DTo2D = ( 1 << 6 ),
    k2DArrayTo2D = ( 1 << 7 ),
    k2DTo2DArray = ( 1 << 8 ),
    k2DArrayTo3D = ( 1 << 9 ),
    k3DTo2DArray = ( 1 << 10 ),
};



typedef int (*test_format_set_fn)( cl_device_id device, cl_image_format *formatList, bool *filterFlags, unsigned int numFormats,
								  image_sampler_data *imageSampler, ExplicitType outputType, cl_mem_object_type imageType );

extern int test_read_image_formats( cl_device_id device, cl_image_format *formatList, bool *filterFlags, unsigned int numFormats,
								   image_sampler_data *imageSampler, ExplicitType outputType, cl_mem_object_type imageType );
extern int test_write_image_formats( cl_device_id device, cl_image_format *formatList, bool *filterFlags, unsigned int numFormats,
								   image_sampler_data *imageSampler, ExplicitType outputType, cl_mem_object_type imageType );

#endif // _testBase_h



