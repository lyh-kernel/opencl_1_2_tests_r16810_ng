/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/typeWrappers.h"
#include "../../test_common/harness/mt19937.h"

extern int		test_multiple_contexts_same_device(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_two_contexts_same_device(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_three_contexts_same_device(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_four_contexts_same_device(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);

extern int		test_two_devices(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_max_devices(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);

extern int		test_hundred_queues(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);


