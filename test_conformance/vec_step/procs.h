/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/threadTesting.h"
#include "../../test_common/harness/typeWrappers.h"
#include "../../test_common/harness/conversions.h"
#include "../../test_common/harness/mt19937.h"

// The number of errors to print out for each test in the shuffle tests
#define MAX_ERRORS_TO_PRINT 1


extern int      create_program_and_kernel(const char *source, const char *kernel_name, cl_program *program_ret, cl_kernel *kernel_ret);


/*
    test_step_type,
    test_step_var,
    test_step_typedef_type,
    test_step_typedef_var,
*/

extern int test_step_type(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);

extern int test_step_var(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);

extern int test_step_typedef_type(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);

extern int test_step_typedef_var(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
