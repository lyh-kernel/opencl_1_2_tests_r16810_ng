
 typedef ushort TypeToTest;
__kernel void test_step_typedef_type(__global TypeToTest *source, __global int *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = vec_step(TypeToTest);

}
