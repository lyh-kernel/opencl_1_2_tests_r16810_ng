
__kernel void test_step_var(__global long4 *source, __global int *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = vec_step(source[tid]);

}
