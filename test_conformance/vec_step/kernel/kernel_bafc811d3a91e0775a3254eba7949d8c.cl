
__kernel void test_step_type(__global ushort *source, __global int *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = vec_step(ushort);

}
