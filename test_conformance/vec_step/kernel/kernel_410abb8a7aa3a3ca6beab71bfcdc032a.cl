
__kernel void test_step_var(__global uint4 *source, __global int *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = vec_step(source[tid]);

}
