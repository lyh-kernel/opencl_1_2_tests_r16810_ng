
__kernel void test_step_type(__global uchar16 *source, __global int *dest)
{
    int  tid = get_global_id(0);
    dest[tid] = vec_step(uchar16);

}
