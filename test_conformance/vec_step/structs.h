/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/


#include "testBase.h"


#include "../../test_common/harness/conversions.h"
#include "../../test_common/harness/typeWrappers.h"
#include<math.h>
#include<float.h>

typedef struct _clState
{
    cl_device_id m_device;
    cl_context m_context; 
    cl_command_queue m_queue;

    cl_program m_program;
    cl_kernel m_kernel;
    size_t m_numThreads;
} clState;

clState * newClState(cl_device_id device, cl_context context, cl_command_queue queue);
clState * destroyClState(clState * pState);

int clStateMakeProgram(clState * pState, const char * prog,
		       const char * kernelName);
void clStateDestroyProgramAndKernel(clState * pState);

int runKernel(clState * pState, size_t numThreads);

typedef struct _bufferStruct 
{
    void * m_pIn;
    void * m_pOut;
    
    cl_mem m_outBuffer;
    cl_mem m_inBuffer;

    size_t m_bufSizeIn, m_bufSizeOut;
} bufferStruct;


bufferStruct * newBufferStruct(size_t inSize, size_t outSize, clState * pClState);

bufferStruct * destroyBufferStruct(bufferStruct * destroyMe, clState * pClState);

void initContents(bufferStruct * pBufferStruct, clState * pClState, 
		     size_t typeSize,
		     size_t vecWidth);

int pushArgs(bufferStruct * pBufferStruct, clState * pClState);
int retrieveResults(bufferStruct * pBufferStruct, clState * pClState);

int checkCorrectness(bufferStruct * pBufferStruct, clState * pClState,
		     size_t typeSize,
		     size_t vecWidth);
