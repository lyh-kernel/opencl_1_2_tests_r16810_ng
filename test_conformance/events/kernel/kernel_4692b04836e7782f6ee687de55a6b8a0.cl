__kernel void sample_test(__global float *src, __global int *dst)
{
    int  tid = get_global_id(0);
	 int  i;

    for( i = 0; i < 10000; i++ )
    {
        dst[tid] = (int)src[tid] * 3;
    }

}
