/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#include "testBase.h"
#include "action_classes.h"
#include "../../test_common/harness/conversions.h"
#include "../../test_common/harness/genericThread.h"

#if !defined (_MSC_VER)
	#include <unistd.h>
#endif // !_MSC_VER

class releaseEvent_thread : public genericThread
{
	public:
		releaseEvent_thread( cl_event *event ) : mEvent( event ) {}
	
		cl_event * mEvent;
	
	protected:
		virtual void *	IRun( void )
		{
			usleep( 1000000 );
			log_info( "\tTriggering gate from separate thread...\n" );
			clSetUserEventStatus( *mEvent, CL_COMPLETE );
			return NULL;
		}
};

int test_userevents_multithreaded( cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements )
{
	cl_int error;
	
	
	// Set up a user event to act as a gate
	clEventWrapper gateEvent = clCreateUserEvent( context, &error );
	test_error( error, "Unable to create user gate event" );
	
	// Set up a few actions gated on the user event
	NDRangeKernelAction action1;
	ReadBufferAction action2;
	WriteBufferAction action3;

	clEventWrapper actionEvents[ 3 ];
	Action * actions[] = { &action1, &action2, &action3, NULL };

	for( int i = 0; actions[ i ] != NULL; i++ )
	{
		error = actions[ i ]->Setup( deviceID, context, queue );
		test_error( error, "Unable to set up test action" );
		
		error = actions[ i ]->Execute( queue, 1, &gateEvent, &actionEvents[ i ] );
		test_error( error, "Unable to execute test action" );
	}
	
	// Now, instead of releasing the gate, we spawn a separate thread to do so
	releaseEvent_thread thread( &gateEvent );
	log_info( "\tStarting trigger thread...\n" );
	thread.Start();
	
	log_info( "\tWaiting for actions...\n" );
	error = clWaitForEvents( 3, &actionEvents[ 0 ] );
	test_error( error, "Unable to wait for action events" );

	log_info( "\tActions completed.\n" );
	
	// If we got here without error, we're good
	return 0;
}

