__constant int addFactor = 1024;
__kernel void sample_test(__global int *src1, __global int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src1[tid] + addFactor;

}
