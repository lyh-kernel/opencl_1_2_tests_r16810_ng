__kernel void sample_test(__global char2 *src1, __global char8 *src2, __global short8 *src3, __global char2 *dst1, __global char8 *dst2, __global short8 *dst3 )
{
    int tid = get_global_id(0);
    dst1[tid] = src1[tid];
    dst2[tid] = src2[tid];
    dst3[tid] = src3[tid];
}
