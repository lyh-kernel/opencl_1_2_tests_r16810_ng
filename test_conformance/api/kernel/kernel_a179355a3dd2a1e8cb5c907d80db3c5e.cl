__kernel void sample_test(__global int4 *src1, __global float4 *src2, __global int2 *src3, __global int4 *dst1, __global float4 *dst2, __global int2 *dst3 )
{
    int tid = get_global_id(0);
    dst1[tid] = src1[tid];
    dst2[tid] = src2[tid];
    dst3[tid] = src3[tid];
}
