__kernel void sample_test(__constant int *src1, __global int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src1[tid];

}
