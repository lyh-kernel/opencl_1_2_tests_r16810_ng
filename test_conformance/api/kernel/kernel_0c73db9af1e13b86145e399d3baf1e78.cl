__kernel void sample_test(__constant int *src1 , __constant int *src2, __constant int *src3, __constant int *src4, __constant int *src5, __constant int *src6, __constant int *src7, __constant int *src8, __global int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src1[tid];
	dst[tid] += src2[tid];
	dst[tid] += src3[tid];
	dst[tid] += src4[tid];
	dst[tid] += src5[tid];
	dst[tid] += src6[tid];
	dst[tid] += src7[tid];
	dst[tid] += src8[tid];

}
