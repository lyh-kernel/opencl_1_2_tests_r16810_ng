/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "testBase.h"
#include "../../test_common/harness/testHarness.h"


const char *kernel_with_bool[] = {
    "__kernel void kernel_with_bool(__global float *src, __global int *dst)\n"
    "{\n"
    "    int  tid = get_global_id(0);\n"
    "\n"
    "    bool myBool = (src[tid] < 0.5f) && (src[tid] > -0.5f);\n"
    "    if(myBool)\n"
    "    {\n"
    "        dst[tid] = (int)src[tid];\n"
    "    }\n"
    "    else\n"
    "    {\n"
    "        dst[tid] = 0;\n"
    "    }\n"
    "\n"
    "}\n" 
};

int test_for_bool_type(cl_device_id deviceID, cl_context context, 
		       cl_command_queue queue, int num_elements)
{

    cl_program program;
    cl_kernel kernel;

    int err = create_single_kernel_helper(context,
					  &program, 
					  &kernel, 
					  1, kernel_with_bool, 
					  "kernel_with_bool" );
    return err;
}

