
__kernel void test_fn(__global float *x, __global float *y, __global float *dst)
{
    int  tid = get_global_id(0);

    vstore3(min(vload3(tid,x), y[tid] ), tid, dst);
}
