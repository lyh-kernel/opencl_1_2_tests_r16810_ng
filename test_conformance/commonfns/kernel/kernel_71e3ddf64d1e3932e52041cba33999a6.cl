__kernel void test_fmax(__global float *srcA, __global float *srcB, __global float *dst)
{
    int  tid = get_global_id(0);
    dst[tid] = fmax(srcA[tid], srcB[tid]);
}
