__kernel void test_fmax3(__global float *srcA, __global float *srcB, __global float *dst)
{
    int  tid = get_global_id(0);
    vstore3(fmax(vload3(tid,srcA), srcB[tid]),tid,dst);
}
