__kernel void test_smoothstep4(__global float4 *edge0, __global float4 *edge1, __global float4 *x, __global float4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = smoothstep(edge0[tid], edge1[tid], x[tid]);
}
