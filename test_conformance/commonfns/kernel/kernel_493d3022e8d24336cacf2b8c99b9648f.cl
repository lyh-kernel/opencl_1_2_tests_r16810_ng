__kernel void test_fmin3(__global float *srcA, __global float *srcB, __global float *dst)
{
    int  tid = get_global_id(0);
    vstore3(fmin(vload3(tid,srcA), srcB[tid]),tid,dst);
}
