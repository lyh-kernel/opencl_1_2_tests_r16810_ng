 __kernel void test_clamp(__global float8 *x, __global float8 *minval, __global float8 *maxval, __global float8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = clamp(x[tid], minval[tid], maxval[tid]);
}
