__kernel void test_sign2(__global float2 *src, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = sign(src[tid]);
}
