__kernel void test_fmax4(__global float4 *srcA, __global float *srcB, __global float4 *dst)
{
    int  tid = get_global_id(0);
    dst[tid] = fmax(srcA[tid], srcB[tid]);
}
