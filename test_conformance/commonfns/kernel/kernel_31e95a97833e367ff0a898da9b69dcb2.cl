__kernel void test_radians4(__global float4 *src, __global float4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = radians(src[tid]);
}
