__kernel void test_step2(__global float2 *srcA, __global float2 *srcB, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = step(srcA[tid], srcB[tid]);
}
