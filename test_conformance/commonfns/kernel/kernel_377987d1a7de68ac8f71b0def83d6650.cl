__kernel void test_fmin(__global float *srcA, __global float *srcB, __global float *dst)
{
    int  tid = get_global_id(0);
    dst[tid] = fmin(srcA[tid], srcB[tid]);
}
