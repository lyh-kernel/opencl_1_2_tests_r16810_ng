
__kernel void test_fn(__global float8 *x, __global float8 *y, __global float8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = min(x[tid], y[tid]);
}
