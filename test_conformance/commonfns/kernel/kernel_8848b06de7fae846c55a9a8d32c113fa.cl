__kernel void test_step8(__global float *srcA, __global float8 *srcB, __global float8 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = step(srcA[tid], srcB[tid]);
}
