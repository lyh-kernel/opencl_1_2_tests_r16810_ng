__kernel void test_step4(__global float4 *srcA, __global float4 *srcB, __global float4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = step(srcA[tid], srcB[tid]);
}
