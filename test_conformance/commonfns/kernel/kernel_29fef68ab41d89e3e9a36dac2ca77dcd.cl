__kernel void test_mix(__global float *srcA, __global float *srcB, __global float *srcC, __global float *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = mix(srcA[tid], srcB[tid], srcC[tid]);
}
