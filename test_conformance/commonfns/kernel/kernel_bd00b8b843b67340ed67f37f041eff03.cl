 __kernel void test_clamp(__global float2 *x, __global float2 *minval, __global float2 *maxval, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = clamp(x[tid], minval[tid], maxval[tid]);
}
