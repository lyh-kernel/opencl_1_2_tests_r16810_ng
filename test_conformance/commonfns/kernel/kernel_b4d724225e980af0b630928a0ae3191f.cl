__kernel void test_sign(__global float *src, __global float *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = sign(src[tid]);
}
