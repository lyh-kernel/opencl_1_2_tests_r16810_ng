__kernel void test_degrees2(__global float2 *src, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = degrees(src[tid]);
}
