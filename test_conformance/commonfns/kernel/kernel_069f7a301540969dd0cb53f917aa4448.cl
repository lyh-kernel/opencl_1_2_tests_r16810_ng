
__kernel void test_fn(__global float2 *x, __global float2 *y, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = max(x[tid], y[tid]);
}
