/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include "procs.h"
#include "../../test_common/harness/errorHelpers.h"

static int min_verify_float( float *x, float *y, float *out, int numElements, int vecSize )
{
	for( int i = 0; i < numElements; i++ )
	{
		for( int j = 0; j < vecSize; j++ )
		{
			float v = ( y[ i ] < x[ i * vecSize + j ] ) ? y[ i ] : x[ i * vecSize + j ];
			if( v != out[ i * vecSize + j ] )
            {
                log_error( "Failure for vector size %d at position %d, element %d:\n\t min(%a, %a) = *%a vs %a\n", vecSize, i, j, x[ i * vecSize + j ], y[i], v,  out[ i * vecSize + j ] );
				return -1;
            }
		}
	}
	return 0;
}

static int min_verify_double( double *x, double *y, double *out, int numElements, int vecSize )
{
    int maxFail = 1;
    int numFails = 0;
	for( int i = 0; i < numElements; i++ )
	{
		for( int j = 0; j < vecSize; j++ )
		{
			double v = ( y[ i ] < x[ i * vecSize + j ] ) ? y[ i ] : x[ i * vecSize + j ];
			if(	v != out[ i * vecSize + j ] )
            {
                log_error( "Failure for vector size %d at position %d, element %d:\n\t min(%a, %a) = *%a vs %a\n", vecSize, i, j, x[ i * vecSize + j ], y[i], v,  out[ i * vecSize + j ] );
			    ++numFails;
			    if(numFails >= maxFail) {
				return -1;
            }
		}
	}
	}
	return 0;
}

int test_minf(cl_device_id device, cl_context context, cl_command_queue queue, int n_elems)
{
	return test_binary_fn( device, context, queue, n_elems, "min", false, min_verify_float, min_verify_double );
}


