/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include "../../test_common/harness/testHarness.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/conversions.h"
#include "../../test_common/harness/mt19937.h"

#define kVectorSizeCount 5
#define kStrangeVectorSizeCount 1
#define kTotalVecCount (kVectorSizeCount + kStrangeVectorSizeCount)

extern int g_arrVecSizes[kVectorSizeCount + kStrangeVectorSizeCount];
// int g_arrStrangeVectorSizes[kStrangeVectorSizeCount] = {3};

extern int		test_clamp(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_degrees(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_fmax(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_fmaxf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_fmin(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_fminf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_max(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_maxf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_min(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_minf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_mix(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_radians(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_step(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_stepf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_smoothstep(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_smoothstepf(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);
extern int		test_sign(cl_device_id device, cl_context context, cl_command_queue queue, int num_elements);

typedef int     (*binary_verify_float_fn)( float *x, float *y, float *out, int numElements, int vecSize );
typedef int     (*binary_verify_double_fn)( double *x, double *y, double *out, int numElements, int vecSize );

extern int      test_binary_fn( cl_device_id device, cl_context context, cl_command_queue queue, int n_elems,
						   const char *fnName, bool vectorSecondParam,
						   binary_verify_float_fn floatVerifyFn, binary_verify_double_fn doubleVerifyFn );


