/****************************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2011 by Advanced Micro Devices, Inc. All Rights Reserved.
 //
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <math.h>
#include <string.h>
#include "procs.h"
#include "../../test_common/harness/testHarness.h"
#include "../../test_common/harness/mt19937.h"

#if !defined(_WIN32)
#include <unistd.h>
#endif

basefn	basefn_list[] = {
            test_partition_equally,
            test_partition_by_counts,
            test_partition_by_affinity_domain_numa,
            test_partition_by_affinity_domain_l4_cache,
            test_partition_by_affinity_domain_l3_cache,
            test_partition_by_affinity_domain_l2_cache,
            test_partition_by_affinity_domain_l1_cache,
            test_partition_by_affinity_domain_next_partitionable,
            test_partition
};


const char    *basefn_names[] = {
            "device_partition_equally",
            "device_partition_by_counts",
            "device_partition_by_affinity_domain_numa",
            "device_partition_by_affinity_domain_l4_cache",
            "device_partition_by_affinity_domain_l3_cache",
            "device_partition_by_affinity_domain_l2_cache",
            "device_partition_by_affinity_domain_l1_cache",
            "device_partition_by_affinity_domain_next_partitionable",
            "device_partition_all",
            "all"
};

ct_assert((sizeof(basefn_names) / sizeof(basefn_names[0]) - 1) == (sizeof(basefn_list) / sizeof(basefn_list[0])));

int	num_fns = sizeof(basefn_names) / sizeof(char *);

int main(int argc, const char *argv[])
{
	return runTestHarness( argc, argv, num_fns, basefn_list, basefn_names, false, true, 0 );
}
