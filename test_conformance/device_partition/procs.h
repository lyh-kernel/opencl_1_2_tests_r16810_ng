/****************************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2011 by Advanced Micro Devices, Inc. All Rights Reserved.
 //
 ****************************************************************************/

#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/kernelHelpers.h"
#include "../../test_common/harness/typeWrappers.h"
#include "../../test_common/harness/mt19937.h"

extern int      test_partition(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_equally(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_counts(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_numa(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_l4_cache(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_l3_cache(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_l2_cache(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_l1_cache(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
extern int      test_partition_by_affinity_domain_next_partitionable(cl_device_id deviceID, cl_context context, cl_command_queue queue, int num_elements);
