
__kernel void sample_test(__global long16 *sourceA, __global long16 *sourceB, __global long16 *sourceC, __global long16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
