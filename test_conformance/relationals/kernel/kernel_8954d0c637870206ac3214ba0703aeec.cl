long16 shuffle_fn( long16 source );
long16 shuffle_fn( long16 source ) { return source; }

__kernel void sample_test( __global long16 *source, __global long8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //long16 src1 /*, src2*/;
  long8 tmp;
		tmp = (long8)((long)0);
		tmp.s0 = shuffle_fn( source[0] ).s4;
		dest[0] = tmp;
		tmp = (long8)((long)0);
		tmp.s3 = shuffle_fn( source[1] ).SE;
		dest[1] = tmp;
		tmp = (long8)((long)0);
		tmp.S1 = shuffle_fn( source[2] ).SA;
		dest[2] = tmp;
		tmp = (long8)((long)0);
		tmp.S5 = shuffle_fn( source[3] ).sc;
		dest[3] = tmp;
		tmp = (long8)((long)0);
		tmp.S52 = shuffle_fn( source[4] ).S3B;
		dest[4] = tmp;
		tmp = (long8)((long)0);
		tmp.s35 = shuffle_fn( source[5] ).sA9;
		dest[5] = tmp;
		tmp = (long8)((long)0);
		tmp.S06 = shuffle_fn( source[6] ).s2E;
		dest[6] = tmp;
		tmp = (long8)((long)0);
		tmp.s67 = shuffle_fn( source[7] ).s6B;
		dest[7] = tmp;
		tmp = (long8)((long)0);
		tmp.s30 = shuffle_fn( source[8] ).S20;
		dest[8] = tmp;
		tmp = (long8)((long)0);
		tmp.S15 = shuffle_fn( source[9] ).s09;
		dest[9] = tmp;
		tmp = (long8)((long)0);
		tmp.s20 = shuffle_fn( source[10] ).se4;
		dest[10] = tmp;
		tmp = (long8)((long)0);
		tmp.S36 = shuffle_fn( source[11] ).sFA;
		dest[11] = tmp;
		tmp = (long8)((long)0);
		tmp.S6047 = shuffle_fn( source[12] ).S503F;
		dest[12] = tmp;
		tmp = (long8)((long)0);
		tmp.s4052 = shuffle_fn( source[13] ).s2113;
		dest[13] = tmp;
		tmp = (long8)((long)0);
		tmp.S3745 = shuffle_fn( source[14] ).sC632;
		dest[14] = tmp;
		tmp = (long8)((long)0);
		tmp.S4162 = shuffle_fn( source[15] ).Se52B;
		dest[15] = tmp;
		tmp = (long8)((long)0);
		tmp.S0375 = shuffle_fn( source[16] ).s9a48;
		dest[16] = tmp;
		tmp = (long8)((long)0);
		tmp.s2307 = shuffle_fn( source[17] ).sB3c4;
		dest[17] = tmp;
		tmp = (long8)((long)0);
		tmp.S3261 = shuffle_fn( source[18] ).S5C4b;
		dest[18] = tmp;
		tmp = (long8)((long)0);
		tmp.S3705 = shuffle_fn( source[19] ).SE930;
		dest[19] = tmp;
		tmp = (long8)((long)0);
		tmp.s1740 = shuffle_fn( source[20] ).Sbcf2;
		dest[20] = tmp;
		tmp = (long8)((long)0);
		tmp.s0412 = shuffle_fn( source[21] ).S6182;
		dest[21] = tmp;
		tmp = (long8)((long)0);
		tmp.S1267 = shuffle_fn( source[22] ).s950C;
		dest[22] = tmp;
		tmp = (long8)((long)0);
		tmp.s6052 = shuffle_fn( source[23] ).S320E;
		dest[23] = tmp;
		tmp = (long8)((long)0);
		tmp.s2740 = shuffle_fn( source[24] ).SE9c7;
		dest[24] = tmp;
		tmp = (long8)((long)0);
		tmp.S5741 = shuffle_fn( source[25] ).sef00;
		dest[25] = tmp;
		tmp = (long8)((long)0);
		tmp.s7362 = shuffle_fn( source[26] ).S6178;
		dest[26] = tmp;
		tmp = (long8)((long)0);
		tmp.s2053 = shuffle_fn( source[27] ).S87aF;
		dest[27] = tmp;
		tmp = (long8)((long)0);
		tmp.S03765412 = shuffle_fn( source[28] ).SacE5E316;
		dest[28] = tmp;
		tmp = (long8)((long)0);
		tmp.s05327416 = shuffle_fn( source[29] ).S75C052bD;
		dest[29] = tmp;
		tmp = (long8)((long)0);
		tmp.S47250163 = shuffle_fn( source[30] ).s73Eb0606;
		dest[30] = tmp;
		tmp = (long8)((long)0);
		tmp.S46370125 = shuffle_fn( source[31] ).s83F511Ee;
		dest[31] = tmp;
}
