
__kernel void sample_test(__global long *sourceA, __global long *sourceB, __global long *sourceC, __global long *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
