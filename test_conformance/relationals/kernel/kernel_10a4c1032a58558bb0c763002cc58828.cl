ulong4 shuffle_fn( ulong4 source );
ulong4 shuffle_fn( ulong4 source ) { return source; }

__kernel void sample_test( __global ulong4 *source, __global ulong4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //ulong4 src1 /*, src2*/;
  ulong4 tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1 = shuffle_fn( source[0] ).S0;
		dest[0] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s0 = shuffle_fn( source[1] ).S0;
		dest[1] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1 = shuffle_fn( source[2] ).s3;
		dest[2] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S3 = shuffle_fn( source[3] ).S2;
		dest[3] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s0 = shuffle_fn( source[4] ).S1;
		dest[4] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s0 = shuffle_fn( source[5] ).s3;
		dest[5] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1 = shuffle_fn( source[6] ).S2;
		dest[6] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S2 = shuffle_fn( source[7] ).s2;
		dest[7] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s30 = shuffle_fn( source[8] ).S21;
		dest[8] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S21 = shuffle_fn( source[9] ).s13;
		dest[9] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s21 = shuffle_fn( source[10] ).S30;
		dest[10] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s10 = shuffle_fn( source[11] ).S10;
		dest[11] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S10 = shuffle_fn( source[12] ).s21;
		dest[12] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s01 = shuffle_fn( source[13] ).S03;
		dest[13] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S12 = shuffle_fn( source[14] ).s31;
		dest[14] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s32 = shuffle_fn( source[15] ).S22;
		dest[15] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s30 = shuffle_fn( source[16] ).S32;
		dest[16] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s03 = shuffle_fn( source[17] ).s01;
		dest[17] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s32 = shuffle_fn( source[18] ).s30;
		dest[18] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S30 = shuffle_fn( source[19] ).s30;
		dest[19] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S20 = shuffle_fn( source[20] ).S11;
		dest[20] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s03 = shuffle_fn( source[21] ).S00;
		dest[21] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S03 = shuffle_fn( source[22] ).s23;
		dest[22] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S10 = shuffle_fn( source[23] ).s32;
		dest[23] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1230 = shuffle_fn( source[24] ).S0013;
		dest[24] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s2130 = shuffle_fn( source[25] ).S0110;
		dest[25] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S0123 = shuffle_fn( source[26] ).s0220;
		dest[26] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s0213 = shuffle_fn( source[27] ).s0021;
		dest[27] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1230 = shuffle_fn( source[28] ).S2330;
		dest[28] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S1320 = shuffle_fn( source[29] ).S3002;
		dest[29] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.S0312 = shuffle_fn( source[30] ).s0331;
		dest[30] = tmp;
		tmp = (ulong4)((ulong)0);
		tmp.s1032 = shuffle_fn( source[31] ).s0001;
		dest[31] = tmp;
}
