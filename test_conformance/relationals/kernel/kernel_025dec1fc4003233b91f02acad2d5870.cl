uchar3 shuffle_fn( uchar3 source );
uchar3 shuffle_fn( uchar3 source ) { return source; }

__kernel void sample_test( __global uchar *source, __global uchar4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uchar3 src1 /*, src2*/;
  uchar4 tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S2 = shuffle_fn( vload3(0, source) ).s2;
		dest[0] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s0 = shuffle_fn( vload3(1, source) ).s2;
		dest[1] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S3 = shuffle_fn( vload3(2, source) ).S0;
		dest[2] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S0 = shuffle_fn( vload3(3, source) ).S2;
		dest[3] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S2 = shuffle_fn( vload3(4, source) ).S0;
		dest[4] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S2 = shuffle_fn( vload3(5, source) ).s1;
		dest[5] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s1 = shuffle_fn( vload3(6, source) ).S0;
		dest[6] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s3 = shuffle_fn( vload3(7, source) ).S2;
		dest[7] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S1 = shuffle_fn( vload3(8, source) ).s0;
		dest[8] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s1 = shuffle_fn( vload3(9, source) ).S0;
		dest[9] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s3 = shuffle_fn( vload3(10, source) ).s1;
		dest[10] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S23 = shuffle_fn( vload3(11, source) ).s20;
		dest[11] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s03 = shuffle_fn( vload3(12, source) ).S12;
		dest[12] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S10 = shuffle_fn( vload3(13, source) ).s12;
		dest[13] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S31 = shuffle_fn( vload3(14, source) ).S01;
		dest[14] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S02 = shuffle_fn( vload3(15, source) ).S00;
		dest[15] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S10 = shuffle_fn( vload3(16, source) ).s20;
		dest[16] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S21 = shuffle_fn( vload3(17, source) ).s21;
		dest[17] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s10 = shuffle_fn( vload3(18, source) ).s21;
		dest[18] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s03 = shuffle_fn( vload3(19, source) ).s01;
		dest[19] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S21 = shuffle_fn( vload3(20, source) ).S10;
		dest[20] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S12 = shuffle_fn( vload3(21, source) ).S10;
		dest[21] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s01 = shuffle_fn( vload3(22, source) ).S10;
		dest[22] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S10 = shuffle_fn( vload3(23, source) ).S10;
		dest[23] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s02 = shuffle_fn( vload3(24, source) ).S02;
		dest[24] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s13 = shuffle_fn( vload3(25, source) ).s11;
		dest[25] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S21 = shuffle_fn( vload3(26, source) ).s01;
		dest[26] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s01 = shuffle_fn( vload3(27, source) ).S00;
		dest[27] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s32 = shuffle_fn( vload3(28, source) ).S12;
		dest[28] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s23 = shuffle_fn( vload3(29, source) ).s22;
		dest[29] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s32 = shuffle_fn( vload3(30, source) ).S00;
		dest[30] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S32 = shuffle_fn( vload3(31, source) ).s01;
		dest[31] = tmp;
}
