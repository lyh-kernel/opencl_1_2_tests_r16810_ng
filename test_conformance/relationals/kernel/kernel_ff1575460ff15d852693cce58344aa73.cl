
__kernel void sample_test(__global uint16 *sourceA, __global uint16 *sourceB, __global uint16 *sourceC, __global uint16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
