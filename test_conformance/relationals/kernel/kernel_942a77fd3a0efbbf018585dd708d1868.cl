
__kernel void sample_test(__global float2 *sourceA, __global float2 *sourceB, __global float2 *sourceC, __global float2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
