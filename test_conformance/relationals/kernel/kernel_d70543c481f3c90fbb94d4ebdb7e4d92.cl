
__kernel void sample_test(__global char2 *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = any( sourceA[tid] );

}
