__kernel void sample_test( __global short2 *secondSource, __global short2 *source, __global short8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //short2 src1 , src2;
  short8 tmp;
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 0 ];
			short2 src2 = secondSource[ 0 ];
			ushort8 mask = (ushort8)( 3, 3, 0, 3, 2, 3, 1, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 1 ];
			short2 src2 = secondSource[ 1 ];
			ushort8 mask = (ushort8)( 0, 1, 1, 3, 0, 3, 0, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 2 ];
			short2 src2 = secondSource[ 2 ];
			ushort8 mask = (ushort8)( 2, 0, 0, 1, 0, 3, 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 3 ];
			short2 src2 = secondSource[ 3 ];
			ushort8 mask = (ushort8)( 0, 2, 1, 3, 1, 2, 0, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 4 ];
			short2 src2 = secondSource[ 4 ];
			ushort8 mask = (ushort8)( 2, 2, 2, 2, 2, 1, 3, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 5 ];
			short2 src2 = secondSource[ 5 ];
			ushort8 mask = (ushort8)( 0, 0, 0, 2, 2, 0, 1, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 6 ];
			short2 src2 = secondSource[ 6 ];
			ushort8 mask = (ushort8)( 3, 3, 1, 3, 1, 3, 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 7 ];
			short2 src2 = secondSource[ 7 ];
			ushort8 mask = (ushort8)( 2, 2, 0, 1, 0, 2, 2, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 8 ];
			short2 src2 = secondSource[ 8 ];
			ushort8 mask = (ushort8)( 1, 2, 2, 1, 2, 1, 0, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 9 ];
			short2 src2 = secondSource[ 9 ];
			ushort8 mask = (ushort8)( 0, 3, 3, 2, 0, 3, 2, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 10 ];
			short2 src2 = secondSource[ 10 ];
			ushort8 mask = (ushort8)( 3, 3, 3, 2, 3, 0, 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 11 ];
			short2 src2 = secondSource[ 11 ];
			ushort8 mask = (ushort8)( 3, 0, 0, 3, 3, 3, 1, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 12 ];
			short2 src2 = secondSource[ 12 ];
			ushort8 mask = (ushort8)( 0, 0, 2, 2, 0, 3, 0, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 13 ];
			short2 src2 = secondSource[ 13 ];
			ushort8 mask = (ushort8)( 1, 2, 1, 0, 3, 2, 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 14 ];
			short2 src2 = secondSource[ 14 ];
			ushort8 mask = (ushort8)( 3, 0, 0, 2, 3, 3, 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 15 ];
			short2 src2 = secondSource[ 15 ];
			ushort8 mask = (ushort8)( 1, 2, 0, 3, 3, 0, 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 16 ];
			short2 src2 = secondSource[ 16 ];
			ushort8 mask = (ushort8)( 1, 1, 2, 2, 1, 2, 0, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 17 ];
			short2 src2 = secondSource[ 17 ];
			ushort8 mask = (ushort8)( 0, 3, 2, 1, 1, 0, 3, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 18 ];
			short2 src2 = secondSource[ 18 ];
			ushort8 mask = (ushort8)( 2, 3, 2, 2, 0, 1, 3, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 19 ];
			short2 src2 = secondSource[ 19 ];
			ushort8 mask = (ushort8)( 2, 3, 1, 1, 2, 0, 2, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 20 ];
			short2 src2 = secondSource[ 20 ];
			ushort8 mask = (ushort8)( 1, 2, 2, 1, 0, 0, 3, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 21 ];
			short2 src2 = secondSource[ 21 ];
			ushort8 mask = (ushort8)( 2, 2, 3, 2, 0, 1, 2, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 22 ];
			short2 src2 = secondSource[ 22 ];
			ushort8 mask = (ushort8)( 2, 3, 2, 1, 0, 3, 0, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 23 ];
			short2 src2 = secondSource[ 23 ];
			ushort8 mask = (ushort8)( 2, 2, 3, 3, 3, 2, 2, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 24 ];
			short2 src2 = secondSource[ 24 ];
			ushort8 mask = (ushort8)( 0, 2, 1, 0, 0, 2, 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 25 ];
			short2 src2 = secondSource[ 25 ];
			ushort8 mask = (ushort8)( 3, 1, 1, 3, 0, 0, 0, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 26 ];
			short2 src2 = secondSource[ 26 ];
			ushort8 mask = (ushort8)( 2, 3, 2, 0, 0, 2, 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 27 ];
			short2 src2 = secondSource[ 27 ];
			ushort8 mask = (ushort8)( 1, 3, 2, 0, 1, 0, 2, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 28 ];
			short2 src2 = secondSource[ 28 ];
			ushort8 mask = (ushort8)( 1, 1, 3, 1, 1, 1, 2, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 29 ];
			short2 src2 = secondSource[ 29 ];
			ushort8 mask = (ushort8)( 3, 0, 1, 1, 2, 1, 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 30 ];
			short2 src2 = secondSource[ 30 ];
			ushort8 mask = (ushort8)( 2, 3, 0, 1, 2, 0, 3, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (short8)((short)0);
		{
			short2 src1 = source[ 31 ];
			short2 src2 = secondSource[ 31 ];
			ushort8 mask = (ushort8)( 3, 2, 3, 1, 2, 3, 3, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
