
__kernel void sample_test(__global char2 *sourceA, __global char2 *sourceB, __global char2 *sourceC, __global char2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
