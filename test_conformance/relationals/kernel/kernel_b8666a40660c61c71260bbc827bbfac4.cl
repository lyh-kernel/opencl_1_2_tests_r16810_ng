ulong3 shuffle_fn( ulong3 source );
ulong3 shuffle_fn( ulong3 source ) { return source; }

__kernel void sample_test( __global ulong *source, __global ulong *dest )
{
    if (get_global_id(0) != 0) return;
	 //ulong3 src1 /*, src2*/;
  ulong3 tmp;
		tmp = (ulong)((ulong)0);
		tmp.S2 = shuffle_fn(vload3(0, source) ).S0;
               vstore3(tmp, 0, dest);
		tmp = (ulong)((ulong)0);
		tmp.S0 = shuffle_fn(vload3(1, source) ).s0;
               vstore3(tmp, 1, dest);
		tmp = (ulong)((ulong)0);
		tmp.S1 = shuffle_fn(vload3(2, source) ).s2;
               vstore3(tmp, 2, dest);
		tmp = (ulong)((ulong)0);
		tmp.S0 = shuffle_fn(vload3(3, source) ).S1;
               vstore3(tmp, 3, dest);
		tmp = (ulong)((ulong)0);
		tmp.s0 = shuffle_fn(vload3(4, source) ).s1;
               vstore3(tmp, 4, dest);
		tmp = (ulong)((ulong)0);
		tmp.s0 = shuffle_fn(vload3(5, source) ).S0;
               vstore3(tmp, 5, dest);
		tmp = (ulong)((ulong)0);
		tmp.S0 = shuffle_fn(vload3(6, source) ).S0;
               vstore3(tmp, 6, dest);
		tmp = (ulong)((ulong)0);
		tmp.S0 = shuffle_fn(vload3(7, source) ).S1;
               vstore3(tmp, 7, dest);
		tmp = (ulong)((ulong)0);
		tmp.S2 = shuffle_fn(vload3(8, source) ).s1;
               vstore3(tmp, 8, dest);
		tmp = (ulong)((ulong)0);
		tmp.S0 = shuffle_fn(vload3(9, source) ).s1;
               vstore3(tmp, 9, dest);
		tmp = (ulong)((ulong)0);
		tmp.s2 = shuffle_fn(vload3(10, source) ).s2;
               vstore3(tmp, 10, dest);
		tmp = (ulong)((ulong)0);
		tmp.s01 = shuffle_fn(vload3(11, source) ).S12;
               vstore3(tmp, 11, dest);
		tmp = (ulong)((ulong)0);
		tmp.s12 = shuffle_fn(vload3(12, source) ).S00;
               vstore3(tmp, 12, dest);
		tmp = (ulong)((ulong)0);
		tmp.s02 = shuffle_fn(vload3(13, source) ).S20;
               vstore3(tmp, 13, dest);
		tmp = (ulong)((ulong)0);
		tmp.s20 = shuffle_fn(vload3(14, source) ).s00;
               vstore3(tmp, 14, dest);
		tmp = (ulong)((ulong)0);
		tmp.S20 = shuffle_fn(vload3(15, source) ).S00;
               vstore3(tmp, 15, dest);
		tmp = (ulong)((ulong)0);
		tmp.S01 = shuffle_fn(vload3(16, source) ).S12;
               vstore3(tmp, 16, dest);
		tmp = (ulong)((ulong)0);
		tmp.s20 = shuffle_fn(vload3(17, source) ).s12;
               vstore3(tmp, 17, dest);
		tmp = (ulong)((ulong)0);
		tmp.S01 = shuffle_fn(vload3(18, source) ).s10;
               vstore3(tmp, 18, dest);
		tmp = (ulong)((ulong)0);
		tmp.S01 = shuffle_fn(vload3(19, source) ).s10;
               vstore3(tmp, 19, dest);
		tmp = (ulong)((ulong)0);
		tmp.S02 = shuffle_fn(vload3(20, source) ).s20;
               vstore3(tmp, 20, dest);
		tmp = (ulong)((ulong)0);
		tmp.S02 = shuffle_fn(vload3(21, source) ).s01;
               vstore3(tmp, 21, dest);
		tmp = (ulong)((ulong)0);
		tmp.S12 = shuffle_fn(vload3(22, source) ).S22;
               vstore3(tmp, 22, dest);
		tmp = (ulong)((ulong)0);
		tmp.s21 = shuffle_fn(vload3(23, source) ).s10;
               vstore3(tmp, 23, dest);
		tmp = (ulong)((ulong)0);
		tmp.s20 = shuffle_fn(vload3(24, source) ).S01;
               vstore3(tmp, 24, dest);
		tmp = (ulong)((ulong)0);
		tmp.s20 = shuffle_fn(vload3(25, source) ).s01;
               vstore3(tmp, 25, dest);
		tmp = (ulong)((ulong)0);
		tmp.s02 = shuffle_fn(vload3(26, source) ).S02;
               vstore3(tmp, 26, dest);
		tmp = (ulong)((ulong)0);
		tmp.s01 = shuffle_fn(vload3(27, source) ).s12;
               vstore3(tmp, 27, dest);
		tmp = (ulong)((ulong)0);
		tmp.s20 = shuffle_fn(vload3(28, source) ).s11;
               vstore3(tmp, 28, dest);
		tmp = (ulong)((ulong)0);
		tmp.s02 = shuffle_fn(vload3(29, source) ).S01;
               vstore3(tmp, 29, dest);
		tmp = (ulong)((ulong)0);
		tmp.S20 = shuffle_fn(vload3(30, source) ).s21;
               vstore3(tmp, 30, dest);
		tmp = (ulong)((ulong)0);
		tmp.S20 = shuffle_fn(vload3(31, source) ).S20;
               vstore3(tmp, 31, dest);
}
