char3 shuffle_fn( char3 source );
char3 shuffle_fn( char3 source ) { return source; }

__kernel void sample_test( __global char *source, __global char4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //char3 src1 /*, src2*/;
  char4 tmp;
		tmp = (char4)((char)0);
		tmp.S0 = shuffle_fn( vload3(0, source) ).S0;
		dest[0] = tmp;
		tmp = (char4)((char)0);
		tmp.s1 = shuffle_fn( vload3(1, source) ).S1;
		dest[1] = tmp;
		tmp = (char4)((char)0);
		tmp.S1 = shuffle_fn( vload3(2, source) ).S0;
		dest[2] = tmp;
		tmp = (char4)((char)0);
		tmp.s3 = shuffle_fn( vload3(3, source) ).s1;
		dest[3] = tmp;
		tmp = (char4)((char)0);
		tmp.s0 = shuffle_fn( vload3(4, source) ).s1;
		dest[4] = tmp;
		tmp = (char4)((char)0);
		tmp.S0 = shuffle_fn( vload3(5, source) ).S0;
		dest[5] = tmp;
		tmp = (char4)((char)0);
		tmp.S0 = shuffle_fn( vload3(6, source) ).S1;
		dest[6] = tmp;
		tmp = (char4)((char)0);
		tmp.S2 = shuffle_fn( vload3(7, source) ).S0;
		dest[7] = tmp;
		tmp = (char4)((char)0);
		tmp.s1 = shuffle_fn( vload3(8, source) ).S1;
		dest[8] = tmp;
		tmp = (char4)((char)0);
		tmp.S1 = shuffle_fn( vload3(9, source) ).S0;
		dest[9] = tmp;
		tmp = (char4)((char)0);
		tmp.s0 = shuffle_fn( vload3(10, source) ).s1;
		dest[10] = tmp;
		tmp = (char4)((char)0);
		tmp.s01 = shuffle_fn( vload3(11, source) ).s01;
		dest[11] = tmp;
		tmp = (char4)((char)0);
		tmp.s20 = shuffle_fn( vload3(12, source) ).S20;
		dest[12] = tmp;
		tmp = (char4)((char)0);
		tmp.s20 = shuffle_fn( vload3(13, source) ).s02;
		dest[13] = tmp;
		tmp = (char4)((char)0);
		tmp.s32 = shuffle_fn( vload3(14, source) ).s10;
		dest[14] = tmp;
		tmp = (char4)((char)0);
		tmp.S32 = shuffle_fn( vload3(15, source) ).s20;
		dest[15] = tmp;
		tmp = (char4)((char)0);
		tmp.S31 = shuffle_fn( vload3(16, source) ).s01;
		dest[16] = tmp;
		tmp = (char4)((char)0);
		tmp.s02 = shuffle_fn( vload3(17, source) ).s02;
		dest[17] = tmp;
		tmp = (char4)((char)0);
		tmp.S12 = shuffle_fn( vload3(18, source) ).S12;
		dest[18] = tmp;
		tmp = (char4)((char)0);
		tmp.s20 = shuffle_fn( vload3(19, source) ).S12;
		dest[19] = tmp;
		tmp = (char4)((char)0);
		tmp.S13 = shuffle_fn( vload3(20, source) ).S21;
		dest[20] = tmp;
		tmp = (char4)((char)0);
		tmp.S12 = shuffle_fn( vload3(21, source) ).S02;
		dest[21] = tmp;
		tmp = (char4)((char)0);
		tmp.S20 = shuffle_fn( vload3(22, source) ).s10;
		dest[22] = tmp;
		tmp = (char4)((char)0);
		tmp.S01 = shuffle_fn( vload3(23, source) ).S01;
		dest[23] = tmp;
		tmp = (char4)((char)0);
		tmp.s02 = shuffle_fn( vload3(24, source) ).S12;
		dest[24] = tmp;
		tmp = (char4)((char)0);
		tmp.s23 = shuffle_fn( vload3(25, source) ).S11;
		dest[25] = tmp;
		tmp = (char4)((char)0);
		tmp.s13 = shuffle_fn( vload3(26, source) ).S20;
		dest[26] = tmp;
		tmp = (char4)((char)0);
		tmp.S13 = shuffle_fn( vload3(27, source) ).S21;
		dest[27] = tmp;
		tmp = (char4)((char)0);
		tmp.s02 = shuffle_fn( vload3(28, source) ).s00;
		dest[28] = tmp;
		tmp = (char4)((char)0);
		tmp.s01 = shuffle_fn( vload3(29, source) ).s22;
		dest[29] = tmp;
		tmp = (char4)((char)0);
		tmp.S20 = shuffle_fn( vload3(30, source) ).S11;
		dest[30] = tmp;
		tmp = (char4)((char)0);
		tmp.s12 = shuffle_fn( vload3(31, source) ).s21;
		dest[31] = tmp;
}
