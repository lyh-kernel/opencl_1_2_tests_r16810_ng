
__kernel void sample_test(__global long3 *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = all(vload3(tid, (__global long *)sourceA));

}
