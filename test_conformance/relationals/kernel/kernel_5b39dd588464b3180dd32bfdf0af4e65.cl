
__kernel void sample_test(__global ulong16 *sourceA, __global ulong16 *sourceB, __global ulong16 *sourceC, __global ulong16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
