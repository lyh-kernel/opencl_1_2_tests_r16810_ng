
__kernel void sample_test(__global long4 *sourceA, __global long4 *sourceB, __global long4 *sourceC, __global long4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
