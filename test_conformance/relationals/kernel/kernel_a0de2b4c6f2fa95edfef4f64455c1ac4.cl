uint8 shuffle_fn( uint8 source );
uint8 shuffle_fn( uint8 source ) { return source; }

__kernel void sample_test( __global uint8 *source, __global uint4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uint8 src1 /*, src2*/;
  uint4 tmp;
		tmp = (uint4)((uint)0);
		tmp.s2 = shuffle_fn( source[0] ).s3;
		dest[0] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S3 = shuffle_fn( source[1] ).s7;
		dest[1] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S2 = shuffle_fn( source[2] ).s3;
		dest[2] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0 = shuffle_fn( source[3] ).S0;
		dest[3] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s1 = shuffle_fn( source[4] ).s6;
		dest[4] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2 = shuffle_fn( source[5] ).s3;
		dest[5] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S3 = shuffle_fn( source[6] ).s3;
		dest[6] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s1 = shuffle_fn( source[7] ).s6;
		dest[7] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S31 = shuffle_fn( source[8] ).s02;
		dest[8] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s20 = shuffle_fn( source[9] ).S23;
		dest[9] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S21 = shuffle_fn( source[10] ).S20;
		dest[10] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s01 = shuffle_fn( source[11] ).s44;
		dest[11] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s02 = shuffle_fn( source[12] ).S63;
		dest[12] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S01 = shuffle_fn( source[13] ).S21;
		dest[13] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s03 = shuffle_fn( source[14] ).s11;
		dest[14] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s20 = shuffle_fn( source[15] ).s50;
		dest[15] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s03 = shuffle_fn( source[16] ).S17;
		dest[16] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s23 = shuffle_fn( source[17] ).s72;
		dest[17] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S23 = shuffle_fn( source[18] ).s11;
		dest[18] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S02 = shuffle_fn( source[19] ).S50;
		dest[19] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S32 = shuffle_fn( source[20] ).S56;
		dest[20] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s01 = shuffle_fn( source[21] ).S17;
		dest[21] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s12 = shuffle_fn( source[22] ).S25;
		dest[22] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S02 = shuffle_fn( source[23] ).s14;
		dest[23] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2310 = shuffle_fn( source[24] ).s1072;
		dest[24] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0132 = shuffle_fn( source[25] ).s6126;
		dest[25] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S3012 = shuffle_fn( source[26] ).s2246;
		dest[26] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2031 = shuffle_fn( source[27] ).s2731;
		dest[27] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2130 = shuffle_fn( source[28] ).s0506;
		dest[28] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0132 = shuffle_fn( source[29] ).s2646;
		dest[29] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s1230 = shuffle_fn( source[30] ).S1330;
		dest[30] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s3021 = shuffle_fn( source[31] ).S5413;
		dest[31] = tmp;
}
