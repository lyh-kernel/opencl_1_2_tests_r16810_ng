__kernel void sample_test(__global float8 *sourceA, __global float8 *sourceB, __global int8 *destValues, __global int8 *destValuesB)
{
    int  tid = get_global_id(0);
    destValues[tid] = isequal( sourceA[tid], sourceB[tid] );
    destValuesB[tid] = sourceA[tid] == sourceB[tid];

}
