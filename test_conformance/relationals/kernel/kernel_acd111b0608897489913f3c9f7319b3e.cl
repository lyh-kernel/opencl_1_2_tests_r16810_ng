int2 shuffle_fn( int2 source );
int2 shuffle_fn( int2 source ) { return source; }

__kernel void sample_test( __global int2 *source, __global int16 *dest )
{
    if (get_global_id(0) != 0) return;
	 //int2 src1 /*, src2*/;
  int16 tmp;
		tmp = (int16)((int)0);
		tmp.S6 = shuffle_fn( source[0] ).S1;
		dest[0] = tmp;
		tmp = (int16)((int)0);
		tmp.s3 = shuffle_fn( source[1] ).s1;
		dest[1] = tmp;
		tmp = (int16)((int)0);
		tmp.s4 = shuffle_fn( source[2] ).s0;
		dest[2] = tmp;
		tmp = (int16)((int)0);
		tmp.S3 = shuffle_fn( source[3] ).s0;
		dest[3] = tmp;
		tmp = (int16)((int)0);
		tmp.s8 = shuffle_fn( source[4] ).S0;
		dest[4] = tmp;
		tmp = (int16)((int)0);
		tmp.sb = shuffle_fn( source[5] ).S0;
		dest[5] = tmp;
		tmp = (int16)((int)0);
		tmp.S1 = shuffle_fn( source[6] ).S0;
		dest[6] = tmp;
		tmp = (int16)((int)0);
		tmp.S2 = shuffle_fn( source[7] ).s0;
		dest[7] = tmp;
		tmp = (int16)((int)0);
		tmp.S5 = shuffle_fn( source[8] ).s1;
		dest[8] = tmp;
		tmp = (int16)((int)0);
		tmp.Sf = shuffle_fn( source[9] ).s0;
		dest[9] = tmp;
		tmp = (int16)((int)0);
		tmp.Sc = shuffle_fn( source[10] ).S0;
		dest[10] = tmp;
		tmp = (int16)((int)0);
		tmp.s1 = shuffle_fn( source[11] ).S0;
		dest[11] = tmp;
		tmp = (int16)((int)0);
		tmp.s5 = shuffle_fn( source[12] ).s0;
		dest[12] = tmp;
		tmp = (int16)((int)0);
		tmp.sb = shuffle_fn( source[13] ).s1;
		dest[13] = tmp;
		tmp = (int16)((int)0);
		tmp.S6 = shuffle_fn( source[14] ).s1;
		dest[14] = tmp;
		tmp = (int16)((int)0);
		tmp.S6 = shuffle_fn( source[15] ).s0;
		dest[15] = tmp;
		tmp = (int16)((int)0);
		tmp.S9D = shuffle_fn( source[16] ).s00;
		dest[16] = tmp;
		tmp = (int16)((int)0);
		tmp.s8e = shuffle_fn( source[17] ).s11;
		dest[17] = tmp;
		tmp = (int16)((int)0);
		tmp.sD2 = shuffle_fn( source[18] ).s11;
		dest[18] = tmp;
		tmp = (int16)((int)0);
		tmp.SF5 = shuffle_fn( source[19] ).S00;
		dest[19] = tmp;
		tmp = (int16)((int)0);
		tmp.s1D = shuffle_fn( source[20] ).S00;
		dest[20] = tmp;
		tmp = (int16)((int)0);
		tmp.S9B = shuffle_fn( source[21] ).s01;
		dest[21] = tmp;
		tmp = (int16)((int)0);
		tmp.Sb8 = shuffle_fn( source[22] ).S01;
		dest[22] = tmp;
		tmp = (int16)((int)0);
		tmp.sD5 = shuffle_fn( source[23] ).S11;
		dest[23] = tmp;
		tmp = (int16)((int)0);
		tmp.sf4 = shuffle_fn( source[24] ).s00;
		dest[24] = tmp;
		tmp = (int16)((int)0);
		tmp.SC6 = shuffle_fn( source[25] ).S11;
		dest[25] = tmp;
		tmp = (int16)((int)0);
		tmp.sd2 = shuffle_fn( source[26] ).S10;
		dest[26] = tmp;
		tmp = (int16)((int)0);
		tmp.s87 = shuffle_fn( source[27] ).S11;
		dest[27] = tmp;
		tmp = (int16)((int)0);
		tmp.Sfc = shuffle_fn( source[28] ).s10;
		dest[28] = tmp;
		tmp = (int16)((int)0);
		tmp.S23 = shuffle_fn( source[29] ).s00;
		dest[29] = tmp;
		tmp = (int16)((int)0);
		tmp.s53 = shuffle_fn( source[30] ).s00;
		dest[30] = tmp;
		tmp = (int16)((int)0);
		tmp.s74 = shuffle_fn( source[31] ).s00;
		dest[31] = tmp;
}
