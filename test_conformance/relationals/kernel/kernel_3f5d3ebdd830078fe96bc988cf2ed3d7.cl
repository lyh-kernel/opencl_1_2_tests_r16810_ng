
__kernel void sample_test(__global ushort8 *sourceA, __global ushort8 *sourceB, __global ushort8 *sourceC, __global ushort8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
