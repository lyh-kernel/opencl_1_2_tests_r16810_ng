
__kernel void sample_test(__global char8 *sourceA, __global char8 *sourceB, __global char8 *sourceC, __global char8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
