
__kernel void sample_test(__global float4 *sourceA, __global float4 *sourceB, __global float4 *sourceC, __global float4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
