short2 shuffle_fn( short2 source );
short2 shuffle_fn( short2 source ) { return source; }

__kernel void sample_test( __global short2 *source, __global short *dest )
{
    if (get_global_id(0) != 0) return;
	 //short2 src1 /*, src2*/;
  short3 tmp;
		tmp = (short)((short)0);
		tmp.S2 = shuffle_fn( source[0] ).s1;
               vstore3(tmp, 0, dest);
		tmp = (short)((short)0);
		tmp.S0 = shuffle_fn( source[1] ).s0;
               vstore3(tmp, 1, dest);
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[2] ).s1;
               vstore3(tmp, 2, dest);
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[3] ).S0;
               vstore3(tmp, 3, dest);
		tmp = (short)((short)0);
		tmp.S2 = shuffle_fn( source[4] ).s0;
               vstore3(tmp, 4, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[5] ).s0;
               vstore3(tmp, 5, dest);
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[6] ).s0;
               vstore3(tmp, 6, dest);
		tmp = (short)((short)0);
		tmp.S0 = shuffle_fn( source[7] ).S0;
               vstore3(tmp, 7, dest);
		tmp = (short)((short)0);
		tmp.S0 = shuffle_fn( source[8] ).S0;
               vstore3(tmp, 8, dest);
		tmp = (short)((short)0);
		tmp.S2 = shuffle_fn( source[9] ).s0;
               vstore3(tmp, 9, dest);
		tmp = (short)((short)0);
		tmp.s1 = shuffle_fn( source[10] ).S1;
               vstore3(tmp, 10, dest);
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[11] ).S0;
               vstore3(tmp, 11, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[12] ).s1;
               vstore3(tmp, 12, dest);
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[13] ).S1;
               vstore3(tmp, 13, dest);
		tmp = (short)((short)0);
		tmp.S1 = shuffle_fn( source[14] ).s0;
               vstore3(tmp, 14, dest);
		tmp = (short)((short)0);
		tmp.S2 = shuffle_fn( source[15] ).s1;
               vstore3(tmp, 15, dest);
		tmp = (short)((short)0);
		tmp.s21 = shuffle_fn( source[16] ).S10;
               vstore3(tmp, 16, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[17] ).S01;
               vstore3(tmp, 17, dest);
		tmp = (short)((short)0);
		tmp.S01 = shuffle_fn( source[18] ).S11;
               vstore3(tmp, 18, dest);
		tmp = (short)((short)0);
		tmp.S01 = shuffle_fn( source[19] ).S01;
               vstore3(tmp, 19, dest);
		tmp = (short)((short)0);
		tmp.s12 = shuffle_fn( source[20] ).S00;
               vstore3(tmp, 20, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[21] ).S10;
               vstore3(tmp, 21, dest);
		tmp = (short)((short)0);
		tmp.S20 = shuffle_fn( source[22] ).s01;
               vstore3(tmp, 22, dest);
		tmp = (short)((short)0);
		tmp.s01 = shuffle_fn( source[23] ).s01;
               vstore3(tmp, 23, dest);
		tmp = (short)((short)0);
		tmp.S12 = shuffle_fn( source[24] ).S00;
               vstore3(tmp, 24, dest);
		tmp = (short)((short)0);
		tmp.s20 = shuffle_fn( source[25] ).S10;
               vstore3(tmp, 25, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[26] ).S11;
               vstore3(tmp, 26, dest);
		tmp = (short)((short)0);
		tmp.S20 = shuffle_fn( source[27] ).S10;
               vstore3(tmp, 27, dest);
		tmp = (short)((short)0);
		tmp.s12 = shuffle_fn( source[28] ).s01;
               vstore3(tmp, 28, dest);
		tmp = (short)((short)0);
		tmp.S12 = shuffle_fn( source[29] ).s11;
               vstore3(tmp, 29, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[30] ).S00;
               vstore3(tmp, 30, dest);
		tmp = (short)((short)0);
		tmp.S12 = shuffle_fn( source[31] ).S01;
               vstore3(tmp, 31, dest);
}
