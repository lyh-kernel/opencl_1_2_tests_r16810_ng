__kernel void sample_test(__global float4 *sourceA, __global float4 *sourceB, __global int4 *destValues, __global int4 *destValuesB)
{
    int  tid = get_global_id(0);
    destValues[tid] = isequal( sourceA[tid], sourceB[tid] );
    destValuesB[tid] = sourceA[tid] == sourceB[tid];

}
