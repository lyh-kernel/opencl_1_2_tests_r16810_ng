short16 shuffle_fn( short16 source );
short16 shuffle_fn( short16 source ) { return source; }

__kernel void sample_test( __global short16 *source, __global short4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //short16 src1 /*, src2*/;
  short4 tmp;
		tmp = (short4)((short)0);
		tmp.s0 = shuffle_fn( source[0] ).s0;
		dest[0] = tmp;
		tmp = (short4)((short)0);
		tmp.s3 = shuffle_fn( source[1] ).s7;
		dest[1] = tmp;
		tmp = (short4)((short)0);
		tmp.s2 = shuffle_fn( source[2] ).S4;
		dest[2] = tmp;
		tmp = (short4)((short)0);
		tmp.s0 = shuffle_fn( source[3] ).S2;
		dest[3] = tmp;
		tmp = (short4)((short)0);
		tmp.S0 = shuffle_fn( source[4] ).S2;
		dest[4] = tmp;
		tmp = (short4)((short)0);
		tmp.S0 = shuffle_fn( source[5] ).sa;
		dest[5] = tmp;
		tmp = (short4)((short)0);
		tmp.S3 = shuffle_fn( source[6] ).sF;
		dest[6] = tmp;
		tmp = (short4)((short)0);
		tmp.s2 = shuffle_fn( source[7] ).sF;
		dest[7] = tmp;
		tmp = (short4)((short)0);
		tmp.s01 = shuffle_fn( source[8] ).Sa3;
		dest[8] = tmp;
		tmp = (short4)((short)0);
		tmp.S31 = shuffle_fn( source[9] ).s26;
		dest[9] = tmp;
		tmp = (short4)((short)0);
		tmp.S20 = shuffle_fn( source[10] ).Sea;
		dest[10] = tmp;
		tmp = (short4)((short)0);
		tmp.s13 = shuffle_fn( source[11] ).S29;
		dest[11] = tmp;
		tmp = (short4)((short)0);
		tmp.s02 = shuffle_fn( source[12] ).s44;
		dest[12] = tmp;
		tmp = (short4)((short)0);
		tmp.S23 = shuffle_fn( source[13] ).S02;
		dest[13] = tmp;
		tmp = (short4)((short)0);
		tmp.S32 = shuffle_fn( source[14] ).s95;
		dest[14] = tmp;
		tmp = (short4)((short)0);
		tmp.S31 = shuffle_fn( source[15] ).sBE;
		dest[15] = tmp;
		tmp = (short4)((short)0);
		tmp.s32 = shuffle_fn( source[16] ).s76;
		dest[16] = tmp;
		tmp = (short4)((short)0);
		tmp.S02 = shuffle_fn( source[17] ).s95;
		dest[17] = tmp;
		tmp = (short4)((short)0);
		tmp.S30 = shuffle_fn( source[18] ).S35;
		dest[18] = tmp;
		tmp = (short4)((short)0);
		tmp.S31 = shuffle_fn( source[19] ).S81;
		dest[19] = tmp;
		tmp = (short4)((short)0);
		tmp.s03 = shuffle_fn( source[20] ).se0;
		dest[20] = tmp;
		tmp = (short4)((short)0);
		tmp.s30 = shuffle_fn( source[21] ).sbd;
		dest[21] = tmp;
		tmp = (short4)((short)0);
		tmp.s31 = shuffle_fn( source[22] ).Scd;
		dest[22] = tmp;
		tmp = (short4)((short)0);
		tmp.S03 = shuffle_fn( source[23] ).s77;
		dest[23] = tmp;
		tmp = (short4)((short)0);
		tmp.s3012 = shuffle_fn( source[24] ).SC3e7;
		dest[24] = tmp;
		tmp = (short4)((short)0);
		tmp.s3201 = shuffle_fn( source[25] ).S179A;
		dest[25] = tmp;
		tmp = (short4)((short)0);
		tmp.s0132 = shuffle_fn( source[26] ).SFa17;
		dest[26] = tmp;
		tmp = (short4)((short)0);
		tmp.s2301 = shuffle_fn( source[27] ).S7478;
		dest[27] = tmp;
		tmp = (short4)((short)0);
		tmp.S2301 = shuffle_fn( source[28] ).saD8C;
		dest[28] = tmp;
		tmp = (short4)((short)0);
		tmp.s3120 = shuffle_fn( source[29] ).sB2F5;
		dest[29] = tmp;
		tmp = (short4)((short)0);
		tmp.s3210 = shuffle_fn( source[30] ).S4343;
		dest[30] = tmp;
		tmp = (short4)((short)0);
		tmp.S0312 = shuffle_fn( source[31] ).SC6a7;
		dest[31] = tmp;
}
