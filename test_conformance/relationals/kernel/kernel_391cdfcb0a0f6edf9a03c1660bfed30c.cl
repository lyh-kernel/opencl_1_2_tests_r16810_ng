int8 shuffle_fn( int8 source );
int8 shuffle_fn( int8 source ) { return source; }

__kernel void sample_test( __global int8 *source, __global int *dest )
{
    if (get_global_id(0) != 0) return;
	 //int8 src1 /*, src2*/;
  int3 tmp;
		tmp = (int)((int)0);
		tmp.s0 = shuffle_fn( source[0] ).S0;
               vstore3(tmp, 0, dest);
		tmp = (int)((int)0);
		tmp.s2 = shuffle_fn( source[1] ).S5;
               vstore3(tmp, 1, dest);
		tmp = (int)((int)0);
		tmp.S1 = shuffle_fn( source[2] ).s3;
               vstore3(tmp, 2, dest);
		tmp = (int)((int)0);
		tmp.S0 = shuffle_fn( source[3] ).s6;
               vstore3(tmp, 3, dest);
		tmp = (int)((int)0);
		tmp.s2 = shuffle_fn( source[4] ).S7;
               vstore3(tmp, 4, dest);
		tmp = (int)((int)0);
		tmp.S2 = shuffle_fn( source[5] ).s7;
               vstore3(tmp, 5, dest);
		tmp = (int)((int)0);
		tmp.S0 = shuffle_fn( source[6] ).S2;
               vstore3(tmp, 6, dest);
		tmp = (int)((int)0);
		tmp.s2 = shuffle_fn( source[7] ).S0;
               vstore3(tmp, 7, dest);
		tmp = (int)((int)0);
		tmp.S0 = shuffle_fn( source[8] ).s3;
               vstore3(tmp, 8, dest);
		tmp = (int)((int)0);
		tmp.S2 = shuffle_fn( source[9] ).s0;
               vstore3(tmp, 9, dest);
		tmp = (int)((int)0);
		tmp.s2 = shuffle_fn( source[10] ).s3;
               vstore3(tmp, 10, dest);
		tmp = (int)((int)0);
		tmp.S12 = shuffle_fn( source[11] ).s27;
               vstore3(tmp, 11, dest);
		tmp = (int)((int)0);
		tmp.s21 = shuffle_fn( source[12] ).S74;
               vstore3(tmp, 12, dest);
		tmp = (int)((int)0);
		tmp.s10 = shuffle_fn( source[13] ).S61;
               vstore3(tmp, 13, dest);
		tmp = (int)((int)0);
		tmp.s21 = shuffle_fn( source[14] ).S47;
               vstore3(tmp, 14, dest);
		tmp = (int)((int)0);
		tmp.S10 = shuffle_fn( source[15] ).S23;
               vstore3(tmp, 15, dest);
		tmp = (int)((int)0);
		tmp.S21 = shuffle_fn( source[16] ).s24;
               vstore3(tmp, 16, dest);
		tmp = (int)((int)0);
		tmp.S01 = shuffle_fn( source[17] ).s37;
               vstore3(tmp, 17, dest);
		tmp = (int)((int)0);
		tmp.S02 = shuffle_fn( source[18] ).S25;
               vstore3(tmp, 18, dest);
		tmp = (int)((int)0);
		tmp.s20 = shuffle_fn( source[19] ).s23;
               vstore3(tmp, 19, dest);
		tmp = (int)((int)0);
		tmp.S02 = shuffle_fn( source[20] ).s61;
               vstore3(tmp, 20, dest);
		tmp = (int)((int)0);
		tmp.S01 = shuffle_fn( source[21] ).s55;
               vstore3(tmp, 21, dest);
		tmp = (int)((int)0);
		tmp.s02 = shuffle_fn( source[22] ).S75;
               vstore3(tmp, 22, dest);
		tmp = (int)((int)0);
		tmp.s12 = shuffle_fn( source[23] ).S23;
               vstore3(tmp, 23, dest);
		tmp = (int)((int)0);
		tmp.S01 = shuffle_fn( source[24] ).S06;
               vstore3(tmp, 24, dest);
		tmp = (int)((int)0);
		tmp.s12 = shuffle_fn( source[25] ).S67;
               vstore3(tmp, 25, dest);
		tmp = (int)((int)0);
		tmp.s20 = shuffle_fn( source[26] ).S04;
               vstore3(tmp, 26, dest);
		tmp = (int)((int)0);
		tmp.s02 = shuffle_fn( source[27] ).S14;
               vstore3(tmp, 27, dest);
		tmp = (int)((int)0);
		tmp.s01 = shuffle_fn( source[28] ).S57;
               vstore3(tmp, 28, dest);
		tmp = (int)((int)0);
		tmp.S12 = shuffle_fn( source[29] ).s13;
               vstore3(tmp, 29, dest);
		tmp = (int)((int)0);
		tmp.s21 = shuffle_fn( source[30] ).S77;
               vstore3(tmp, 30, dest);
		tmp = (int)((int)0);
		tmp.s12 = shuffle_fn( source[31] ).S16;
               vstore3(tmp, 31, dest);
}
