long3 shuffle_fn( long3 source );
long3 shuffle_fn( long3 source ) { return source; }

__kernel void sample_test( __global long *source, __global long8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //long3 src1 /*, src2*/;
  long8 tmp;
		tmp = (long8)((long)0);
		tmp.S6 = shuffle_fn( vload3(0, source) ).S1;
		dest[0] = tmp;
		tmp = (long8)((long)0);
		tmp.S1 = shuffle_fn( vload3(1, source) ).S2;
		dest[1] = tmp;
		tmp = (long8)((long)0);
		tmp.s4 = shuffle_fn( vload3(2, source) ).s0;
		dest[2] = tmp;
		tmp = (long8)((long)0);
		tmp.s2 = shuffle_fn( vload3(3, source) ).S0;
		dest[3] = tmp;
		tmp = (long8)((long)0);
		tmp.s0 = shuffle_fn( vload3(4, source) ).s2;
		dest[4] = tmp;
		tmp = (long8)((long)0);
		tmp.S4 = shuffle_fn( vload3(5, source) ).s0;
		dest[5] = tmp;
		tmp = (long8)((long)0);
		tmp.s7 = shuffle_fn( vload3(6, source) ).s1;
		dest[6] = tmp;
		tmp = (long8)((long)0);
		tmp.s4 = shuffle_fn( vload3(7, source) ).s0;
		dest[7] = tmp;
		tmp = (long8)((long)0);
		tmp.s5 = shuffle_fn( vload3(8, source) ).s1;
		dest[8] = tmp;
		tmp = (long8)((long)0);
		tmp.s0 = shuffle_fn( vload3(9, source) ).s2;
		dest[9] = tmp;
		tmp = (long8)((long)0);
		tmp.S5 = shuffle_fn( vload3(10, source) ).S0;
		dest[10] = tmp;
		tmp = (long8)((long)0);
		tmp.s70 = shuffle_fn( vload3(11, source) ).s01;
		dest[11] = tmp;
		tmp = (long8)((long)0);
		tmp.S67 = shuffle_fn( vload3(12, source) ).s21;
		dest[12] = tmp;
		tmp = (long8)((long)0);
		tmp.S41 = shuffle_fn( vload3(13, source) ).S20;
		dest[13] = tmp;
		tmp = (long8)((long)0);
		tmp.s12 = shuffle_fn( vload3(14, source) ).s12;
		dest[14] = tmp;
		tmp = (long8)((long)0);
		tmp.S17 = shuffle_fn( vload3(15, source) ).S02;
		dest[15] = tmp;
		tmp = (long8)((long)0);
		tmp.S24 = shuffle_fn( vload3(16, source) ).S12;
		dest[16] = tmp;
		tmp = (long8)((long)0);
		tmp.s36 = shuffle_fn( vload3(17, source) ).S01;
		dest[17] = tmp;
		tmp = (long8)((long)0);
		tmp.s71 = shuffle_fn( vload3(18, source) ).S10;
		dest[18] = tmp;
		tmp = (long8)((long)0);
		tmp.s37 = shuffle_fn( vload3(19, source) ).S10;
		dest[19] = tmp;
		tmp = (long8)((long)0);
		tmp.S17 = shuffle_fn( vload3(20, source) ).s01;
		dest[20] = tmp;
		tmp = (long8)((long)0);
		tmp.S76 = shuffle_fn( vload3(21, source) ).S00;
		dest[21] = tmp;
		tmp = (long8)((long)0);
		tmp.S30 = shuffle_fn( vload3(22, source) ).s22;
		dest[22] = tmp;
		tmp = (long8)((long)0);
		tmp.s62 = shuffle_fn( vload3(23, source) ).s00;
		dest[23] = tmp;
		tmp = (long8)((long)0);
		tmp.S60 = shuffle_fn( vload3(24, source) ).S00;
		dest[24] = tmp;
		tmp = (long8)((long)0);
		tmp.s54 = shuffle_fn( vload3(25, source) ).s20;
		dest[25] = tmp;
		tmp = (long8)((long)0);
		tmp.S30 = shuffle_fn( vload3(26, source) ).s10;
		dest[26] = tmp;
		tmp = (long8)((long)0);
		tmp.S30 = shuffle_fn( vload3(27, source) ).s10;
		dest[27] = tmp;
		tmp = (long8)((long)0);
		tmp.S01 = shuffle_fn( vload3(28, source) ).s00;
		dest[28] = tmp;
		tmp = (long8)((long)0);
		tmp.s43 = shuffle_fn( vload3(29, source) ).s01;
		dest[29] = tmp;
		tmp = (long8)((long)0);
		tmp.S07 = shuffle_fn( vload3(30, source) ).s10;
		dest[30] = tmp;
		tmp = (long8)((long)0);
		tmp.s43 = shuffle_fn( vload3(31, source) ).S02;
		dest[31] = tmp;
}
