__kernel void sample_test( __global float2 *source, __global float8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //float2 src1 /*, src2*/;
  float8 tmp;
		tmp = (float8)((float)0);
     tmp.s1 = ((__global float16 *)source)[0].s0;
     dest[0] = tmp;
		tmp = (float8)((float)0);
     tmp.s3 = ((__global float16 *)source)[0].s2;
     dest[1] = tmp;
		tmp = (float8)((float)0);
     tmp.s6 = ((__global float16 *)source)[0].s4;
     dest[2] = tmp;
		tmp = (float8)((float)0);
     tmp.S3 = ((__global float16 *)source)[0].s6;
     dest[3] = tmp;
		tmp = (float8)((float)0);
     tmp.S4 = ((__global float16 *)source)[0].s9;
     dest[4] = tmp;
		tmp = (float8)((float)0);
     tmp.s2 = ((__global float16 *)source)[0].Sb;
     dest[5] = tmp;
		tmp = (float8)((float)0);
     tmp.S2 = ((__global float16 *)source)[0].SD;
     dest[6] = tmp;
		tmp = (float8)((float)0);
     tmp.s2 = ((__global float16 *)source)[0].SF;
     dest[7] = tmp;
		tmp = (float8)((float)0);
     tmp.s6 = ((__global float16 *)source)[1].S1;
     dest[8] = tmp;
		tmp = (float8)((float)0);
     tmp.S6 = ((__global float16 *)source)[1].s2;
     dest[9] = tmp;
		tmp = (float8)((float)0);
     tmp.s2 = ((__global float16 *)source)[1].S4;
     dest[10] = tmp;
		tmp = (float8)((float)0);
     tmp.S0 = ((__global float16 *)source)[1].s6;
     dest[11] = tmp;
		tmp = (float8)((float)0);
     tmp.s3 = ((__global float16 *)source)[1].s9;
     dest[12] = tmp;
		tmp = (float8)((float)0);
     tmp.s5 = ((__global float16 *)source)[1].Sa;
     dest[13] = tmp;
		tmp = (float8)((float)0);
     tmp.S0 = ((__global float16 *)source)[1].SC;
     dest[14] = tmp;
		tmp = (float8)((float)0);
     tmp.S0 = ((__global float16 *)source)[1].se;
     dest[15] = tmp;
		tmp = (float8)((float)0);
     tmp.S6 = ((__global float16 *)source)[2].S0;
     tmp.S0 = ((__global float16 *)source)[2].S1;
     dest[16] = tmp;
		tmp = (float8)((float)0);
     tmp.s1 = ((__global float16 *)source)[2].s3;
     tmp.s2 = ((__global float16 *)source)[2].s2;
     dest[17] = tmp;
		tmp = (float8)((float)0);
     tmp.S4 = ((__global float16 *)source)[2].S5;
     tmp.S5 = ((__global float16 *)source)[2].S4;
     dest[18] = tmp;
		tmp = (float8)((float)0);
     tmp.s2 = ((__global float16 *)source)[2].S7;
     tmp.S5 = ((__global float16 *)source)[2].s7;
     dest[19] = tmp;
		tmp = (float8)((float)0);
     tmp.s0 = ((__global float16 *)source)[2].s8;
     tmp.S7 = ((__global float16 *)source)[2].S8;
     dest[20] = tmp;
		tmp = (float8)((float)0);
     tmp.s2 = ((__global float16 *)source)[2].SB;
     tmp.S5 = ((__global float16 *)source)[2].sA;
     dest[21] = tmp;
		tmp = (float8)((float)0);
     tmp.S5 = ((__global float16 *)source)[2].sD;
     tmp.s3 = ((__global float16 *)source)[2].Sd;
     dest[22] = tmp;
		tmp = (float8)((float)0);
     tmp.s5 = ((__global float16 *)source)[2].sf;
     tmp.s4 = ((__global float16 *)source)[2].Sf;
     dest[23] = tmp;
		tmp = (float8)((float)0);
     tmp.s7 = ((__global float16 *)source)[3].s1;
     tmp.s6 = ((__global float16 *)source)[3].S0;
     dest[24] = tmp;
		tmp = (float8)((float)0);
     tmp.S1 = ((__global float16 *)source)[3].S2;
     tmp.S0 = ((__global float16 *)source)[3].S2;
     dest[25] = tmp;
		tmp = (float8)((float)0);
     tmp.s5 = ((__global float16 *)source)[3].S4;
     tmp.s2 = ((__global float16 *)source)[3].s4;
     dest[26] = tmp;
		tmp = (float8)((float)0);
     tmp.s6 = ((__global float16 *)source)[3].S7;
     tmp.S4 = ((__global float16 *)source)[3].s6;
     dest[27] = tmp;
		tmp = (float8)((float)0);
     tmp.S2 = ((__global float16 *)source)[3].s8;
     tmp.S5 = ((__global float16 *)source)[3].S9;
     dest[28] = tmp;
		tmp = (float8)((float)0);
     tmp.S5 = ((__global float16 *)source)[3].sB;
     tmp.S4 = ((__global float16 *)source)[3].SB;
     dest[29] = tmp;
		tmp = (float8)((float)0);
     tmp.S4 = ((__global float16 *)source)[3].sc;
     tmp.s2 = ((__global float16 *)source)[3].Sd;
     dest[30] = tmp;
		tmp = (float8)((float)0);
     tmp.S5 = ((__global float16 *)source)[3].se;
     tmp.s2 = ((__global float16 *)source)[3].SE;
     dest[31] = tmp;
}
