
__kernel void sample_test(__global short16 *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = all( sourceA[tid] );

}
