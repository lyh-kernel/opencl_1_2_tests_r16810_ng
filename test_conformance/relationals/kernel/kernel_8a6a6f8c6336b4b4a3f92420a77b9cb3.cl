__kernel void sample_test( __global float2 *source, __global float4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //float2 src1 /*, src2*/;
  float4 tmp;
		tmp = (float4)((float)0);
     tmp.S2 = ((__global float16 *)source)[0].s1;
     dest[0] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[0].S2;
     dest[1] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[0].s5;
     dest[2] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[0].s7;
     dest[3] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[0].s8;
     dest[4] = tmp;
		tmp = (float4)((float)0);
     tmp.s3 = ((__global float16 *)source)[0].sA;
     dest[5] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[0].sc;
     dest[6] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[0].sE;
     dest[7] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[1].S1;
     dest[8] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[1].S3;
     dest[9] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[1].S4;
     dest[10] = tmp;
		tmp = (float4)((float)0);
     tmp.s3 = ((__global float16 *)source)[1].S6;
     dest[11] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[1].S8;
     dest[12] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[1].sa;
     dest[13] = tmp;
		tmp = (float4)((float)0);
     tmp.S2 = ((__global float16 *)source)[1].sd;
     dest[14] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[1].sE;
     dest[15] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[2].s1;
     tmp.s3 = ((__global float16 *)source)[2].S1;
     dest[16] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[2].S2;
     tmp.S1 = ((__global float16 *)source)[2].s3;
     dest[17] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[2].s4;
     tmp.S0 = ((__global float16 *)source)[2].s4;
     dest[18] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[2].S7;
     tmp.s2 = ((__global float16 *)source)[2].S6;
     dest[19] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[2].S8;
     tmp.S2 = ((__global float16 *)source)[2].S8;
     dest[20] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[2].sb;
     tmp.s3 = ((__global float16 *)source)[2].SA;
     dest[21] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[2].Sc;
     tmp.S1 = ((__global float16 *)source)[2].sC;
     dest[22] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[2].SE;
     tmp.s3 = ((__global float16 *)source)[2].Se;
     dest[23] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[3].S0;
     tmp.S3 = ((__global float16 *)source)[3].S0;
     dest[24] = tmp;
		tmp = (float4)((float)0);
     tmp.S2 = ((__global float16 *)source)[3].S2;
     tmp.S1 = ((__global float16 *)source)[3].S3;
     dest[25] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[3].s5;
     tmp.S0 = ((__global float16 *)source)[3].S5;
     dest[26] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[3].s6;
     tmp.S3 = ((__global float16 *)source)[3].S7;
     dest[27] = tmp;
		tmp = (float4)((float)0);
     tmp.S2 = ((__global float16 *)source)[3].s9;
     tmp.s1 = ((__global float16 *)source)[3].S8;
     dest[28] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[3].sA;
     tmp.S1 = ((__global float16 *)source)[3].Sb;
     dest[29] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[3].Sc;
     tmp.s3 = ((__global float16 *)source)[3].sC;
     dest[30] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[3].sf;
     tmp.s1 = ((__global float16 *)source)[3].SE;
     dest[31] = tmp;
}
