
__kernel void sample_test(__global uchar *sourceA, __global uchar *sourceB, __global uchar *sourceC, __global uchar *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
