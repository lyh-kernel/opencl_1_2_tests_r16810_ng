uint16 shuffle_fn( uint16 source );
uint16 shuffle_fn( uint16 source ) { return source; }

__kernel void sample_test( __global uint16 *source, __global uint2 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uint16 src1 /*, src2*/;
  uint2 tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[0] ).s5;
		dest[0] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s1 = shuffle_fn( source[1] ).s9;
		dest[1] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S1 = shuffle_fn( source[2] ).S5;
		dest[2] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[3] ).s3;
		dest[3] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S1 = shuffle_fn( source[4] ).S0;
		dest[4] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S0 = shuffle_fn( source[5] ).s0;
		dest[5] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[6] ).S8;
		dest[6] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S0 = shuffle_fn( source[7] ).Se;
		dest[7] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[8] ).S7;
		dest[8] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[9] ).S0;
		dest[9] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S0 = shuffle_fn( source[10] ).Sb;
		dest[10] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s0 = shuffle_fn( source[11] ).sf;
		dest[11] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S1 = shuffle_fn( source[12] ).s9;
		dest[12] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S0 = shuffle_fn( source[13] ).S9;
		dest[13] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S0 = shuffle_fn( source[14] ).s7;
		dest[14] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s1 = shuffle_fn( source[15] ).s5;
		dest[15] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s10 = shuffle_fn( source[16] ).s52;
		dest[16] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s10 = shuffle_fn( source[17] ).S25;
		dest[17] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s01 = shuffle_fn( source[18] ).s5c;
		dest[18] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S10 = shuffle_fn( source[19] ).sC1;
		dest[19] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S01 = shuffle_fn( source[20] ).sEE;
		dest[20] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S01 = shuffle_fn( source[21] ).sFb;
		dest[21] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s01 = shuffle_fn( source[22] ).s2f;
		dest[22] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s10 = shuffle_fn( source[23] ).sb5;
		dest[23] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S10 = shuffle_fn( source[24] ).s51;
		dest[24] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S01 = shuffle_fn( source[25] ).Sc8;
		dest[25] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S01 = shuffle_fn( source[26] ).S49;
		dest[26] = tmp;
		tmp = (uint2)((uint)0);
		tmp.S10 = shuffle_fn( source[27] ).SE3;
		dest[27] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s10 = shuffle_fn( source[28] ).Se4;
		dest[28] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s01 = shuffle_fn( source[29] ).sab;
		dest[29] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s01 = shuffle_fn( source[30] ).S54;
		dest[30] = tmp;
		tmp = (uint2)((uint)0);
		tmp.s01 = shuffle_fn( source[31] ).s85;
		dest[31] = tmp;
}
