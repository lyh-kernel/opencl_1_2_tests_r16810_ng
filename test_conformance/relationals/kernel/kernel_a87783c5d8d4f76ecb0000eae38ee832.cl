
__kernel void sample_test(__global uchar8 *sourceA, __global uchar8 *sourceB, __global uchar8 *sourceC, __global uchar8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
