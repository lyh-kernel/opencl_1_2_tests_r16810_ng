__kernel void sample_test( __global uchar8 *secondSource, __global uchar8 *source, __global uchar4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uchar8 src1 , src2;
  uchar4 tmp;
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 0 ];
			uchar8 src2 = secondSource[ 0 ];
			uchar4 mask = (uchar4)( 2, 2, 7, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 1 ];
			uchar8 src2 = secondSource[ 1 ];
			uchar4 mask = (uchar4)( 6, 6, 13, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 2 ];
			uchar8 src2 = secondSource[ 2 ];
			uchar4 mask = (uchar4)( 8, 10, 7, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 3 ];
			uchar8 src2 = secondSource[ 3 ];
			uchar4 mask = (uchar4)( 9, 14, 3, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 4 ];
			uchar8 src2 = secondSource[ 4 ];
			uchar4 mask = (uchar4)( 8, 4, 5, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 5 ];
			uchar8 src2 = secondSource[ 5 ];
			uchar4 mask = (uchar4)( 1, 12, 12, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 6 ];
			uchar8 src2 = secondSource[ 6 ];
			uchar4 mask = (uchar4)( 5, 14, 4, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 7 ];
			uchar8 src2 = secondSource[ 7 ];
			uchar4 mask = (uchar4)( 0, 12, 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 8 ];
			uchar8 src2 = secondSource[ 8 ];
			uchar4 mask = (uchar4)( 4, 11, 5, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 9 ];
			uchar8 src2 = secondSource[ 9 ];
			uchar4 mask = (uchar4)( 11, 7, 5, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 10 ];
			uchar8 src2 = secondSource[ 10 ];
			uchar4 mask = (uchar4)( 12, 12, 9, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 11 ];
			uchar8 src2 = secondSource[ 11 ];
			uchar4 mask = (uchar4)( 5, 6, 2, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 12 ];
			uchar8 src2 = secondSource[ 12 ];
			uchar4 mask = (uchar4)( 0, 0, 6, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 13 ];
			uchar8 src2 = secondSource[ 13 ];
			uchar4 mask = (uchar4)( 6, 8, 5, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 14 ];
			uchar8 src2 = secondSource[ 14 ];
			uchar4 mask = (uchar4)( 12, 13, 1, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 15 ];
			uchar8 src2 = secondSource[ 15 ];
			uchar4 mask = (uchar4)( 1, 7, 3, 13 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 16 ];
			uchar8 src2 = secondSource[ 16 ];
			uchar4 mask = (uchar4)( 8, 4, 11, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 17 ];
			uchar8 src2 = secondSource[ 17 ];
			uchar4 mask = (uchar4)( 14, 2, 4, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 18 ];
			uchar8 src2 = secondSource[ 18 ];
			uchar4 mask = (uchar4)( 2, 9, 8, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 19 ];
			uchar8 src2 = secondSource[ 19 ];
			uchar4 mask = (uchar4)( 4, 10, 15, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 20 ];
			uchar8 src2 = secondSource[ 20 ];
			uchar4 mask = (uchar4)( 0, 7, 3, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 21 ];
			uchar8 src2 = secondSource[ 21 ];
			uchar4 mask = (uchar4)( 12, 11, 6, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 22 ];
			uchar8 src2 = secondSource[ 22 ];
			uchar4 mask = (uchar4)( 8, 5, 13, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 23 ];
			uchar8 src2 = secondSource[ 23 ];
			uchar4 mask = (uchar4)( 10, 2, 9, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 24 ];
			uchar8 src2 = secondSource[ 24 ];
			uchar4 mask = (uchar4)( 10, 10, 1, 14 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 25 ];
			uchar8 src2 = secondSource[ 25 ];
			uchar4 mask = (uchar4)( 1, 6, 9, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 26 ];
			uchar8 src2 = secondSource[ 26 ];
			uchar4 mask = (uchar4)( 5, 6, 15, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 27 ];
			uchar8 src2 = secondSource[ 27 ];
			uchar4 mask = (uchar4)( 13, 2, 14, 14 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 28 ];
			uchar8 src2 = secondSource[ 28 ];
			uchar4 mask = (uchar4)( 2, 0, 9, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 29 ];
			uchar8 src2 = secondSource[ 29 ];
			uchar4 mask = (uchar4)( 0, 5, 11, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 30 ];
			uchar8 src2 = secondSource[ 30 ];
			uchar4 mask = (uchar4)( 0, 3, 10, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (uchar4)((uchar)0);
		{
			uchar8 src1 = source[ 31 ];
			uchar8 src2 = secondSource[ 31 ];
			uchar4 mask = (uchar4)( 3, 14, 9, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
