
__kernel void sample_test(__global float8 *sourceA, __global float8 *sourceB, __global float8 *sourceC, __global float8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
