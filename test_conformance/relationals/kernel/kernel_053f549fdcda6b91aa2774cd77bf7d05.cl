int3 shuffle_fn( int3 source );
int3 shuffle_fn( int3 source ) { return source; }

__kernel void sample_test( __global int *source, __global int16 *dest )
{
    if (get_global_id(0) != 0) return;
	 //int3 src1 /*, src2*/;
  int16 tmp;
		tmp = (int16)((int)0);
		tmp.s6 = shuffle_fn( vload3(0, source) ).S0;
		dest[0] = tmp;
		tmp = (int16)((int)0);
		tmp.sC = shuffle_fn( vload3(1, source) ).s0;
		dest[1] = tmp;
		tmp = (int16)((int)0);
		tmp.S1 = shuffle_fn( vload3(2, source) ).S1;
		dest[2] = tmp;
		tmp = (int16)((int)0);
		tmp.s4 = shuffle_fn( vload3(3, source) ).s1;
		dest[3] = tmp;
		tmp = (int16)((int)0);
		tmp.sD = shuffle_fn( vload3(4, source) ).s0;
		dest[4] = tmp;
		tmp = (int16)((int)0);
		tmp.S5 = shuffle_fn( vload3(5, source) ).S2;
		dest[5] = tmp;
		tmp = (int16)((int)0);
		tmp.s9 = shuffle_fn( vload3(6, source) ).s0;
		dest[6] = tmp;
		tmp = (int16)((int)0);
		tmp.s9 = shuffle_fn( vload3(7, source) ).s1;
		dest[7] = tmp;
		tmp = (int16)((int)0);
		tmp.Sb = shuffle_fn( vload3(8, source) ).s1;
		dest[8] = tmp;
		tmp = (int16)((int)0);
		tmp.s2 = shuffle_fn( vload3(9, source) ).s0;
		dest[9] = tmp;
		tmp = (int16)((int)0);
		tmp.s4 = shuffle_fn( vload3(10, source) ).s1;
		dest[10] = tmp;
		tmp = (int16)((int)0);
		tmp.Sb2 = shuffle_fn( vload3(11, source) ).S00;
		dest[11] = tmp;
		tmp = (int16)((int)0);
		tmp.sDe = shuffle_fn( vload3(12, source) ).S00;
		dest[12] = tmp;
		tmp = (int16)((int)0);
		tmp.Sb6 = shuffle_fn( vload3(13, source) ).S02;
		dest[13] = tmp;
		tmp = (int16)((int)0);
		tmp.s69 = shuffle_fn( vload3(14, source) ).s02;
		dest[14] = tmp;
		tmp = (int16)((int)0);
		tmp.s32 = shuffle_fn( vload3(15, source) ).S22;
		dest[15] = tmp;
		tmp = (int16)((int)0);
		tmp.SF8 = shuffle_fn( vload3(16, source) ).S00;
		dest[16] = tmp;
		tmp = (int16)((int)0);
		tmp.s01 = shuffle_fn( vload3(17, source) ).S22;
		dest[17] = tmp;
		tmp = (int16)((int)0);
		tmp.S2A = shuffle_fn( vload3(18, source) ).s20;
		dest[18] = tmp;
		tmp = (int16)((int)0);
		tmp.S83 = shuffle_fn( vload3(19, source) ).s11;
		dest[19] = tmp;
		tmp = (int16)((int)0);
		tmp.S1F = shuffle_fn( vload3(20, source) ).s11;
		dest[20] = tmp;
		tmp = (int16)((int)0);
		tmp.s2C = shuffle_fn( vload3(21, source) ).S01;
		dest[21] = tmp;
		tmp = (int16)((int)0);
		tmp.s92 = shuffle_fn( vload3(22, source) ).s22;
		dest[22] = tmp;
		tmp = (int16)((int)0);
		tmp.S90 = shuffle_fn( vload3(23, source) ).s21;
		dest[23] = tmp;
		tmp = (int16)((int)0);
		tmp.S75 = shuffle_fn( vload3(24, source) ).S01;
		dest[24] = tmp;
		tmp = (int16)((int)0);
		tmp.s2e = shuffle_fn( vload3(25, source) ).s22;
		dest[25] = tmp;
		tmp = (int16)((int)0);
		tmp.sd4 = shuffle_fn( vload3(26, source) ).s00;
		dest[26] = tmp;
		tmp = (int16)((int)0);
		tmp.S45 = shuffle_fn( vload3(27, source) ).S00;
		dest[27] = tmp;
		tmp = (int16)((int)0);
		tmp.S0E = shuffle_fn( vload3(28, source) ).s22;
		dest[28] = tmp;
		tmp = (int16)((int)0);
		tmp.se6 = shuffle_fn( vload3(29, source) ).s20;
		dest[29] = tmp;
		tmp = (int16)((int)0);
		tmp.sC9 = shuffle_fn( vload3(30, source) ).s01;
		dest[30] = tmp;
		tmp = (int16)((int)0);
		tmp.sc9 = shuffle_fn( vload3(31, source) ).s11;
		dest[31] = tmp;
}
