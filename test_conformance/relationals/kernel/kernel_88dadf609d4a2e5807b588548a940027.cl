
__kernel void sample_test(__global float16 *sourceA, __global float16 *sourceB, __global float16 *sourceC, __global float16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
