__kernel void sample_test( __global float16 *source, __global float16 *dest )
{
    if (get_global_id(0) != 0) return;
	 //float16 src1 /*, src2*/;
  float16 tmp;
		tmp = (float16)((float)0);
		tmp.s9 = source[0].S1;
		dest[0] = tmp;
		tmp = (float16)((float)0);
		tmp.s3 = source[1].S5;
		dest[1] = tmp;
		tmp = (float16)((float)0);
		tmp.se5 = source[2].S44;
		dest[2] = tmp;
		tmp = (float16)((float)0);
		tmp.S1A = source[3].s11;
		dest[3] = tmp;
		tmp = (float16)((float)0);
		tmp.S9b = source[4].Sc5;
		dest[4] = tmp;
		tmp = (float16)((float)0);
		tmp.SEB = source[5].s9A;
		dest[5] = tmp;
		tmp = (float16)((float)0);
		tmp.sa51e = source[6].S0252;
		dest[6] = tmp;
		tmp = (float16)((float)0);
		tmp.S167f = source[7].s8B11;
		dest[7] = tmp;
		tmp = (float16)((float)0);
		tmp.S7D6B = source[8].sa340;
		dest[8] = tmp;
		tmp = (float16)((float)0);
		tmp.S950A = source[9].S67Af;
		dest[9] = tmp;
		tmp = (float16)((float)0);
		tmp.s84aE = source[10].ScfBF;
		dest[10] = tmp;
		tmp = (float16)((float)0);
		tmp.sC4E2 = source[11].S550a;
		dest[11] = tmp;
		tmp = (float16)((float)0);
		tmp.se872 = source[12].s8D02;
		dest[12] = tmp;
		tmp = (float16)((float)0);
		tmp.sf345 = source[13].S03dA;
		dest[13] = tmp;
		tmp = (float16)((float)0);
		tmp.s6935DE20 = source[14].S0d944747;
		dest[14] = tmp;
		tmp = (float16)((float)0);
		tmp.S02C53Fd4 = source[15].S5d5Bf7A2;
		dest[15] = tmp;
		tmp = (float16)((float)0);
		tmp.s749EA10d = source[16].s65072a1D;
		dest[16] = tmp;
		tmp = (float16)((float)0);
		tmp.s38BD157E = source[17].Sc7995918;
		dest[17] = tmp;
		tmp = (float16)((float)0);
		tmp.S0A48bf15 = source[18].s734baEC8;
		dest[18] = tmp;
		tmp = (float16)((float)0);
		tmp.sD0Ab97fC = source[19].Sc2faB414;
		dest[19] = tmp;
		tmp = (float16)((float)0);
		tmp.s13CD5Fb7 = source[20].s6eA21666;
		dest[20] = tmp;
		tmp = (float16)((float)0);
		tmp.s235608dB = source[21].SABaf7963;
		dest[21] = tmp;
		tmp = (float16)((float)0);
		tmp.safD140C3 = source[22].S96671316;
		dest[22] = tmp;
		tmp = (float16)((float)0);
		tmp.SfcE046B2 = source[23].s76aAe95b;
		dest[23] = tmp;
		tmp = (float16)((float)0);
		tmp.sab18D546 = source[24].s9564e417;
		dest[24] = tmp;
		tmp = (float16)((float)0);
		tmp.sca46E3D9 = source[25].s1D7aa9fa;
		dest[25] = tmp;
		tmp = (float16)((float)0);
		tmp.S4c5b27Fe = source[26].S402d0AFf;
		dest[26] = tmp;
		tmp = (float16)((float)0);
		tmp.SEd60812C = source[27].S7Dbf35DD;
		dest[27] = tmp;
		tmp = (float16)((float)0);
		tmp.S39e4dA70 = source[28].S3775b325;
		dest[28] = tmp;
		tmp = (float16)((float)0);
		tmp.s4FEa89d7 = source[29].s696c66E9;
		dest[29] = tmp;
		tmp = (float16)((float)0);
		tmp.s8943eCbd1650A7f2 = source[30].s821AA526db0da06B;
		dest[30] = tmp;
		tmp = (float16)((float)0);
		tmp.S69bD1c0E43258f7a = source[31].Sd2713DE974300e85;
		dest[31] = tmp;
}
