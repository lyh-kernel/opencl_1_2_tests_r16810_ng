
__kernel void sample_test(__global int8 *sourceA, __global int8 *sourceB, __global int8 *sourceC, __global int8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
