
__kernel void sample_test(__global short4 *sourceA, __global short4 *sourceB, __global short4 *sourceC, __global short4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
