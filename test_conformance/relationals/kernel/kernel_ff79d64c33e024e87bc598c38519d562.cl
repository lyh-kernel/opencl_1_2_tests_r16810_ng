
__kernel void sample_test(__global ulong4 *sourceA, __global ulong4 *sourceB, __global ulong4 *sourceC, __global ulong4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
