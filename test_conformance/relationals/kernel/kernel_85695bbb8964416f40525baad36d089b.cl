
__kernel void sample_test(__global uint2 *sourceA, __global uint2 *sourceB, __global uint2 *sourceC, __global uint2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
