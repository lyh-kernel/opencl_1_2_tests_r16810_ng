__kernel void sample_test( __global float8 *source, __global float4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //float8 src1 /*, src2*/;
  float4 tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[0].S0;
     dest[0] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[0].SF;
     dest[1] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[1].S3;
     dest[2] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[1].SF;
     dest[3] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[2].s7;
     dest[4] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[2].SE;
     dest[5] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[3].s1;
     dest[6] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[3].SE;
     dest[7] = tmp;
		tmp = (float4)((float)0);
     tmp.S2 = ((__global float16 *)source)[4].s7;
     tmp.S0 = ((__global float16 *)source)[4].S3;
     dest[8] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[4].s9;
     tmp.s1 = ((__global float16 *)source)[4].s8;
     dest[9] = tmp;
		tmp = (float4)((float)0);
     tmp.s3 = ((__global float16 *)source)[5].S3;
     tmp.s1 = ((__global float16 *)source)[5].s4;
     dest[10] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[5].SD;
     tmp.s0 = ((__global float16 *)source)[5].Sd;
     dest[11] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[6].s1;
     tmp.s3 = ((__global float16 *)source)[6].s6;
     dest[12] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[6].sF;
     tmp.S2 = ((__global float16 *)source)[6].S8;
     dest[13] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[7].s7;
     tmp.s3 = ((__global float16 *)source)[7].S2;
     dest[14] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[7].sd;
     tmp.S2 = ((__global float16 *)source)[7].sF;
     dest[15] = tmp;
		tmp = (float4)((float)0);
     tmp.S1 = ((__global float16 *)source)[8].s4;
     tmp.S2 = ((__global float16 *)source)[8].S1;
     dest[16] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[8].sA;
     tmp.s0 = ((__global float16 *)source)[8].SC;
     dest[17] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[9].s0;
     tmp.S0 = ((__global float16 *)source)[9].S5;
     dest[18] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[9].Se;
     tmp.S1 = ((__global float16 *)source)[9].S8;
     dest[19] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[10].s7;
     tmp.S2 = ((__global float16 *)source)[10].S3;
     dest[20] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[10].S9;
     tmp.s0 = ((__global float16 *)source)[10].sE;
     dest[21] = tmp;
		tmp = (float4)((float)0);
     tmp.s3 = ((__global float16 *)source)[11].s2;
     tmp.s0 = ((__global float16 *)source)[11].s0;
     dest[22] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[11].S8;
     tmp.S2 = ((__global float16 *)source)[11].SA;
     dest[23] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[12].S5;
     tmp.s1 = ((__global float16 *)source)[12].s7;
     tmp.S2 = ((__global float16 *)source)[12].S2;
     tmp.s3 = ((__global float16 *)source)[12].S2;
     dest[24] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[12].SF;
     tmp.s2 = ((__global float16 *)source)[12].sD;
     tmp.s3 = ((__global float16 *)source)[12].sD;
     tmp.s0 = ((__global float16 *)source)[12].s9;
     dest[25] = tmp;
		tmp = (float4)((float)0);
     tmp.S0 = ((__global float16 *)source)[13].s1;
     tmp.S2 = ((__global float16 *)source)[13].s2;
     tmp.s1 = ((__global float16 *)source)[13].s5;
     tmp.S3 = ((__global float16 *)source)[13].S7;
     dest[26] = tmp;
		tmp = (float4)((float)0);
     tmp.s1 = ((__global float16 *)source)[13].Sb;
     tmp.s2 = ((__global float16 *)source)[13].se;
     tmp.S0 = ((__global float16 *)source)[13].S9;
     tmp.S3 = ((__global float16 *)source)[13].sa;
     dest[27] = tmp;
		tmp = (float4)((float)0);
     tmp.s0 = ((__global float16 *)source)[14].s2;
     tmp.S3 = ((__global float16 *)source)[14].s7;
     tmp.s1 = ((__global float16 *)source)[14].S2;
     tmp.s2 = ((__global float16 *)source)[14].S2;
     dest[28] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[14].sb;
     tmp.S1 = ((__global float16 *)source)[14].SC;
     tmp.s3 = ((__global float16 *)source)[14].SF;
     tmp.s0 = ((__global float16 *)source)[14].Sa;
     dest[29] = tmp;
		tmp = (float4)((float)0);
     tmp.S3 = ((__global float16 *)source)[15].S4;
     tmp.S1 = ((__global float16 *)source)[15].s6;
     tmp.S0 = ((__global float16 *)source)[15].s3;
     tmp.s2 = ((__global float16 *)source)[15].s5;
     dest[30] = tmp;
		tmp = (float4)((float)0);
     tmp.s2 = ((__global float16 *)source)[15].S9;
     tmp.s0 = ((__global float16 *)source)[15].se;
     tmp.S3 = ((__global float16 *)source)[15].S9;
     tmp.s1 = ((__global float16 *)source)[15].SB;
     dest[31] = tmp;
}
