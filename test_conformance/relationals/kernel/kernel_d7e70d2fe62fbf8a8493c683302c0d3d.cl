
__kernel void sample_test(__global ulong *sourceA, __global ulong *sourceB, __global ulong *sourceC, __global ulong *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
