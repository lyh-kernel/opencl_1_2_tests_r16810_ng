__kernel void sample_test( __global ushort16 *secondSource, __global ushort16 *source, __global ushort4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //ushort16 src1 , src2;
  ushort4 tmp;
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 0 ];
			ushort16 src2 = secondSource[ 0 ];
			ushort4 mask = (ushort4)( 4, 17, 21, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 1 ];
			ushort16 src2 = secondSource[ 1 ];
			ushort4 mask = (ushort4)( 31, 5, 23, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 2 ];
			ushort16 src2 = secondSource[ 2 ];
			ushort4 mask = (ushort4)( 24, 17, 10, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 3 ];
			ushort16 src2 = secondSource[ 3 ];
			ushort4 mask = (ushort4)( 19, 14, 0, 6 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 4 ];
			ushort16 src2 = secondSource[ 4 ];
			ushort4 mask = (ushort4)( 21, 13, 18, 12 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 5 ];
			ushort16 src2 = secondSource[ 5 ];
			ushort4 mask = (ushort4)( 19, 20, 26, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 6 ];
			ushort16 src2 = secondSource[ 6 ];
			ushort4 mask = (ushort4)( 15, 2, 30, 25 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 7 ];
			ushort16 src2 = secondSource[ 7 ];
			ushort4 mask = (ushort4)( 17, 25, 9, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 8 ];
			ushort16 src2 = secondSource[ 8 ];
			ushort4 mask = (ushort4)( 1, 0, 24, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 9 ];
			ushort16 src2 = secondSource[ 9 ];
			ushort4 mask = (ushort4)( 4, 17, 24, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 10 ];
			ushort16 src2 = secondSource[ 10 ];
			ushort4 mask = (ushort4)( 19, 26, 15, 20 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 11 ];
			ushort16 src2 = secondSource[ 11 ];
			ushort4 mask = (ushort4)( 15, 23, 2, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 12 ];
			ushort16 src2 = secondSource[ 12 ];
			ushort4 mask = (ushort4)( 7, 4, 20, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 13 ];
			ushort16 src2 = secondSource[ 13 ];
			ushort4 mask = (ushort4)( 27, 15, 7, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 14 ];
			ushort16 src2 = secondSource[ 14 ];
			ushort4 mask = (ushort4)( 2, 0, 4, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 15 ];
			ushort16 src2 = secondSource[ 15 ];
			ushort4 mask = (ushort4)( 7, 4, 1, 18 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 16 ];
			ushort16 src2 = secondSource[ 16 ];
			ushort4 mask = (ushort4)( 8, 6, 9, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 17 ];
			ushort16 src2 = secondSource[ 17 ];
			ushort4 mask = (ushort4)( 20, 29, 25, 29 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 18 ];
			ushort16 src2 = secondSource[ 18 ];
			ushort4 mask = (ushort4)( 14, 4, 10, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 19 ];
			ushort16 src2 = secondSource[ 19 ];
			ushort4 mask = (ushort4)( 12, 3, 19, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 20 ];
			ushort16 src2 = secondSource[ 20 ];
			ushort4 mask = (ushort4)( 0, 13, 9, 13 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 21 ];
			ushort16 src2 = secondSource[ 21 ];
			ushort4 mask = (ushort4)( 17, 10, 27, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 22 ];
			ushort16 src2 = secondSource[ 22 ];
			ushort4 mask = (ushort4)( 2, 6, 30, 20 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 23 ];
			ushort16 src2 = secondSource[ 23 ];
			ushort4 mask = (ushort4)( 8, 6, 15, 29 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 24 ];
			ushort16 src2 = secondSource[ 24 ];
			ushort4 mask = (ushort4)( 3, 12, 25, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 25 ];
			ushort16 src2 = secondSource[ 25 ];
			ushort4 mask = (ushort4)( 25, 15, 29, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 26 ];
			ushort16 src2 = secondSource[ 26 ];
			ushort4 mask = (ushort4)( 19, 19, 29, 21 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 27 ];
			ushort16 src2 = secondSource[ 27 ];
			ushort4 mask = (ushort4)( 26, 3, 13, 23 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 28 ];
			ushort16 src2 = secondSource[ 28 ];
			ushort4 mask = (ushort4)( 29, 13, 9, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 29 ];
			ushort16 src2 = secondSource[ 29 ];
			ushort4 mask = (ushort4)( 14, 30, 26, 29 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 30 ];
			ushort16 src2 = secondSource[ 30 ];
			ushort4 mask = (ushort4)( 19, 16, 10, 29 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (ushort4)((ushort)0);
		{
			ushort16 src1 = source[ 31 ];
			ushort16 src2 = secondSource[ 31 ];
			ushort4 mask = (ushort4)( 28, 15, 0, 31 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
