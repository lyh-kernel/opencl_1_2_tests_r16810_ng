
__kernel void sample_test(__global ulong2 *sourceA, __global ulong2 *sourceB, __global ulong2 *sourceC, __global ulong2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
