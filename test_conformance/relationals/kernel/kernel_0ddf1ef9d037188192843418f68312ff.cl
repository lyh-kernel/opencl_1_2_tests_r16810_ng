
__kernel void sample_test(__global short *sourceA, __global short *sourceB, __global short *sourceC, __global short *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
