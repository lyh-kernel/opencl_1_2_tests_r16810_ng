uint4 shuffle_fn( uint4 source );
uint4 shuffle_fn( uint4 source ) { return source; }

__kernel void sample_test( __global uint4 *source, __global uint *dest )
{
    if (get_global_id(0) != 0) return;
	 //uint4 src1 /*, src2*/;
  uint3 tmp;
		tmp = (uint)((uint)0);
		tmp.s0 = shuffle_fn( source[0] ).s0;
               vstore3(tmp, 0, dest);
		tmp = (uint)((uint)0);
		tmp.S0 = shuffle_fn( source[1] ).s3;
               vstore3(tmp, 1, dest);
		tmp = (uint)((uint)0);
		tmp.S0 = shuffle_fn( source[2] ).S3;
               vstore3(tmp, 2, dest);
		tmp = (uint)((uint)0);
		tmp.S1 = shuffle_fn( source[3] ).S0;
               vstore3(tmp, 3, dest);
		tmp = (uint)((uint)0);
		tmp.s0 = shuffle_fn( source[4] ).S3;
               vstore3(tmp, 4, dest);
		tmp = (uint)((uint)0);
		tmp.s2 = shuffle_fn( source[5] ).s2;
               vstore3(tmp, 5, dest);
		tmp = (uint)((uint)0);
		tmp.S1 = shuffle_fn( source[6] ).S3;
               vstore3(tmp, 6, dest);
		tmp = (uint)((uint)0);
		tmp.S2 = shuffle_fn( source[7] ).S1;
               vstore3(tmp, 7, dest);
		tmp = (uint)((uint)0);
		tmp.s0 = shuffle_fn( source[8] ).S0;
               vstore3(tmp, 8, dest);
		tmp = (uint)((uint)0);
		tmp.S0 = shuffle_fn( source[9] ).S1;
               vstore3(tmp, 9, dest);
		tmp = (uint)((uint)0);
		tmp.s0 = shuffle_fn( source[10] ).S2;
               vstore3(tmp, 10, dest);
		tmp = (uint)((uint)0);
		tmp.S20 = shuffle_fn( source[11] ).s13;
               vstore3(tmp, 11, dest);
		tmp = (uint)((uint)0);
		tmp.S20 = shuffle_fn( source[12] ).s01;
               vstore3(tmp, 12, dest);
		tmp = (uint)((uint)0);
		tmp.s10 = shuffle_fn( source[13] ).s23;
               vstore3(tmp, 13, dest);
		tmp = (uint)((uint)0);
		tmp.S21 = shuffle_fn( source[14] ).s02;
               vstore3(tmp, 14, dest);
		tmp = (uint)((uint)0);
		tmp.S20 = shuffle_fn( source[15] ).s11;
               vstore3(tmp, 15, dest);
		tmp = (uint)((uint)0);
		tmp.s02 = shuffle_fn( source[16] ).s32;
               vstore3(tmp, 16, dest);
		tmp = (uint)((uint)0);
		tmp.s21 = shuffle_fn( source[17] ).S21;
               vstore3(tmp, 17, dest);
		tmp = (uint)((uint)0);
		tmp.S10 = shuffle_fn( source[18] ).s11;
               vstore3(tmp, 18, dest);
		tmp = (uint)((uint)0);
		tmp.s02 = shuffle_fn( source[19] ).S31;
               vstore3(tmp, 19, dest);
		tmp = (uint)((uint)0);
		tmp.S12 = shuffle_fn( source[20] ).s12;
               vstore3(tmp, 20, dest);
		tmp = (uint)((uint)0);
		tmp.s20 = shuffle_fn( source[21] ).S03;
               vstore3(tmp, 21, dest);
		tmp = (uint)((uint)0);
		tmp.S10 = shuffle_fn( source[22] ).S10;
               vstore3(tmp, 22, dest);
		tmp = (uint)((uint)0);
		tmp.s02 = shuffle_fn( source[23] ).s32;
               vstore3(tmp, 23, dest);
		tmp = (uint)((uint)0);
		tmp.S12 = shuffle_fn( source[24] ).s20;
               vstore3(tmp, 24, dest);
		tmp = (uint)((uint)0);
		tmp.s12 = shuffle_fn( source[25] ).S02;
               vstore3(tmp, 25, dest);
		tmp = (uint)((uint)0);
		tmp.S21 = shuffle_fn( source[26] ).s21;
               vstore3(tmp, 26, dest);
		tmp = (uint)((uint)0);
		tmp.S21 = shuffle_fn( source[27] ).S11;
               vstore3(tmp, 27, dest);
		tmp = (uint)((uint)0);
		tmp.S12 = shuffle_fn( source[28] ).s03;
               vstore3(tmp, 28, dest);
		tmp = (uint)((uint)0);
		tmp.s01 = shuffle_fn( source[29] ).s00;
               vstore3(tmp, 29, dest);
		tmp = (uint)((uint)0);
		tmp.s21 = shuffle_fn( source[30] ).S11;
               vstore3(tmp, 30, dest);
		tmp = (uint)((uint)0);
		tmp.s01 = shuffle_fn( source[31] ).S33;
               vstore3(tmp, 31, dest);
}
