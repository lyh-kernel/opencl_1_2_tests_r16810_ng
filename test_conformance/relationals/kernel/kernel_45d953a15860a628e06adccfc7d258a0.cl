
__kernel void sample_test(__global char *sourceA, __global char *sourceB, __global char *sourceC, __global char *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
