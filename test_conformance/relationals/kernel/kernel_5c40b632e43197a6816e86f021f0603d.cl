short4 shuffle_fn( short4 source );
short4 shuffle_fn( short4 source ) { return source; }

__kernel void sample_test( __global short4 *source, __global short *dest )
{
    if (get_global_id(0) != 0) return;
	 //short4 src1 /*, src2*/;
  short3 tmp;
		tmp = (short)((short)0);
		tmp.s0 = shuffle_fn( source[0] ).S1;
               vstore3(tmp, 0, dest);
		tmp = (short)((short)0);
		tmp.S1 = shuffle_fn( source[1] ).s2;
               vstore3(tmp, 1, dest);
		tmp = (short)((short)0);
		tmp.S1 = shuffle_fn( source[2] ).s1;
               vstore3(tmp, 2, dest);
		tmp = (short)((short)0);
		tmp.S2 = shuffle_fn( source[3] ).S0;
               vstore3(tmp, 3, dest);
		tmp = (short)((short)0);
		tmp.S1 = shuffle_fn( source[4] ).S2;
               vstore3(tmp, 4, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[5] ).S1;
               vstore3(tmp, 5, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[6] ).s0;
               vstore3(tmp, 6, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[7] ).S3;
               vstore3(tmp, 7, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[8] ).s3;
               vstore3(tmp, 8, dest);
		tmp = (short)((short)0);
		tmp.S1 = shuffle_fn( source[9] ).S3;
               vstore3(tmp, 9, dest);
		tmp = (short)((short)0);
		tmp.s2 = shuffle_fn( source[10] ).s3;
               vstore3(tmp, 10, dest);
		tmp = (short)((short)0);
		tmp.S20 = shuffle_fn( source[11] ).s20;
               vstore3(tmp, 11, dest);
		tmp = (short)((short)0);
		tmp.s21 = shuffle_fn( source[12] ).S21;
               vstore3(tmp, 12, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[13] ).s11;
               vstore3(tmp, 13, dest);
		tmp = (short)((short)0);
		tmp.s12 = shuffle_fn( source[14] ).s02;
               vstore3(tmp, 14, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[15] ).S23;
               vstore3(tmp, 15, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[16] ).S03;
               vstore3(tmp, 16, dest);
		tmp = (short)((short)0);
		tmp.s12 = shuffle_fn( source[17] ).S21;
               vstore3(tmp, 17, dest);
		tmp = (short)((short)0);
		tmp.S12 = shuffle_fn( source[18] ).s00;
               vstore3(tmp, 18, dest);
		tmp = (short)((short)0);
		tmp.S01 = shuffle_fn( source[19] ).S32;
               vstore3(tmp, 19, dest);
		tmp = (short)((short)0);
		tmp.s02 = shuffle_fn( source[20] ).s00;
               vstore3(tmp, 20, dest);
		tmp = (short)((short)0);
		tmp.s21 = shuffle_fn( source[21] ).S00;
               vstore3(tmp, 21, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[22] ).s02;
               vstore3(tmp, 22, dest);
		tmp = (short)((short)0);
		tmp.s20 = shuffle_fn( source[23] ).s22;
               vstore3(tmp, 23, dest);
		tmp = (short)((short)0);
		tmp.s12 = shuffle_fn( source[24] ).S30;
               vstore3(tmp, 24, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[25] ).S33;
               vstore3(tmp, 25, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[26] ).s02;
               vstore3(tmp, 26, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[27] ).S12;
               vstore3(tmp, 27, dest);
		tmp = (short)((short)0);
		tmp.S01 = shuffle_fn( source[28] ).s31;
               vstore3(tmp, 28, dest);
		tmp = (short)((short)0);
		tmp.S01 = shuffle_fn( source[29] ).S23;
               vstore3(tmp, 29, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[30] ).S10;
               vstore3(tmp, 30, dest);
		tmp = (short)((short)0);
		tmp.S21 = shuffle_fn( source[31] ).s11;
               vstore3(tmp, 31, dest);
}
