
__kernel void sample_test(__global float *sourceA, __global float *sourceB, __global float *sourceC, __global float *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
