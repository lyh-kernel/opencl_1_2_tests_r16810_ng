
__kernel void sample_test(__global long2 *sourceA, __global long2 *sourceB, __global long2 *sourceC, __global long2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
