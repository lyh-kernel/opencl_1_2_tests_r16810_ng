
__kernel void sample_test(__global char16 *sourceA, __global char16 *sourceB, __global char16 *sourceC, __global char16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
