
__kernel void sample_test(__global char4 *sourceA, __global char4 *sourceB, __global char4 *sourceC, __global char4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
