float4 shuffle_fn( float4 source );
float4 shuffle_fn( float4 source ) { return source; }

__kernel void sample_test( __global float4 *source, __global float *dest )
{
    if (get_global_id(0) != 0) return;
	 //float4 src1 /*, src2*/;
  float3 tmp;
		tmp = (float)((float)0);
		tmp.S2 = shuffle_fn( source[0] ).s1;
               vstore3(tmp, 0, dest);
		tmp = (float)((float)0);
		tmp.s0 = shuffle_fn( source[1] ).S0;
               vstore3(tmp, 1, dest);
		tmp = (float)((float)0);
		tmp.S0 = shuffle_fn( source[2] ).s1;
               vstore3(tmp, 2, dest);
		tmp = (float)((float)0);
		tmp.S1 = shuffle_fn( source[3] ).S2;
               vstore3(tmp, 3, dest);
		tmp = (float)((float)0);
		tmp.S1 = shuffle_fn( source[4] ).s0;
               vstore3(tmp, 4, dest);
		tmp = (float)((float)0);
		tmp.S0 = shuffle_fn( source[5] ).S3;
               vstore3(tmp, 5, dest);
		tmp = (float)((float)0);
		tmp.S2 = shuffle_fn( source[6] ).s0;
               vstore3(tmp, 6, dest);
		tmp = (float)((float)0);
		tmp.S0 = shuffle_fn( source[7] ).s2;
               vstore3(tmp, 7, dest);
		tmp = (float)((float)0);
		tmp.S0 = shuffle_fn( source[8] ).s3;
               vstore3(tmp, 8, dest);
		tmp = (float)((float)0);
		tmp.S0 = shuffle_fn( source[9] ).S3;
               vstore3(tmp, 9, dest);
		tmp = (float)((float)0);
		tmp.S2 = shuffle_fn( source[10] ).S2;
               vstore3(tmp, 10, dest);
		tmp = (float)((float)0);
		tmp.S01 = shuffle_fn( source[11] ).S20;
               vstore3(tmp, 11, dest);
		tmp = (float)((float)0);
		tmp.S02 = shuffle_fn( source[12] ).S12;
               vstore3(tmp, 12, dest);
		tmp = (float)((float)0);
		tmp.S20 = shuffle_fn( source[13] ).s21;
               vstore3(tmp, 13, dest);
		tmp = (float)((float)0);
		tmp.s20 = shuffle_fn( source[14] ).s20;
               vstore3(tmp, 14, dest);
		tmp = (float)((float)0);
		tmp.s01 = shuffle_fn( source[15] ).s31;
               vstore3(tmp, 15, dest);
		tmp = (float)((float)0);
		tmp.s21 = shuffle_fn( source[16] ).S13;
               vstore3(tmp, 16, dest);
		tmp = (float)((float)0);
		tmp.s21 = shuffle_fn( source[17] ).s30;
               vstore3(tmp, 17, dest);
		tmp = (float)((float)0);
		tmp.s20 = shuffle_fn( source[18] ).S21;
               vstore3(tmp, 18, dest);
		tmp = (float)((float)0);
		tmp.s02 = shuffle_fn( source[19] ).S00;
               vstore3(tmp, 19, dest);
		tmp = (float)((float)0);
		tmp.s21 = shuffle_fn( source[20] ).s10;
               vstore3(tmp, 20, dest);
		tmp = (float)((float)0);
		tmp.S21 = shuffle_fn( source[21] ).s33;
               vstore3(tmp, 21, dest);
		tmp = (float)((float)0);
		tmp.S21 = shuffle_fn( source[22] ).s13;
               vstore3(tmp, 22, dest);
		tmp = (float)((float)0);
		tmp.s01 = shuffle_fn( source[23] ).s22;
               vstore3(tmp, 23, dest);
		tmp = (float)((float)0);
		tmp.S20 = shuffle_fn( source[24] ).s12;
               vstore3(tmp, 24, dest);
		tmp = (float)((float)0);
		tmp.s20 = shuffle_fn( source[25] ).s32;
               vstore3(tmp, 25, dest);
		tmp = (float)((float)0);
		tmp.s20 = shuffle_fn( source[26] ).S13;
               vstore3(tmp, 26, dest);
		tmp = (float)((float)0);
		tmp.s20 = shuffle_fn( source[27] ).S13;
               vstore3(tmp, 27, dest);
		tmp = (float)((float)0);
		tmp.S21 = shuffle_fn( source[28] ).S00;
               vstore3(tmp, 28, dest);
		tmp = (float)((float)0);
		tmp.s01 = shuffle_fn( source[29] ).s30;
               vstore3(tmp, 29, dest);
		tmp = (float)((float)0);
		tmp.s01 = shuffle_fn( source[30] ).s21;
               vstore3(tmp, 30, dest);
		tmp = (float)((float)0);
		tmp.s10 = shuffle_fn( source[31] ).s33;
               vstore3(tmp, 31, dest);
}
