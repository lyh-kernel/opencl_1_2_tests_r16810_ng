
__kernel void sample_test(__global int16 *sourceA, __global int16 *sourceB, __global int16 *sourceC, __global int16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
