__kernel void sample_test(__global float16 *sourceA, __global float16 *sourceB, __global int16 *destValues, __global int16 *destValuesB)
{
    int  tid = get_global_id(0);
    destValues[tid] = islessequal( sourceA[tid], sourceB[tid] );
    destValuesB[tid] = sourceA[tid] <= sourceB[tid];

}
