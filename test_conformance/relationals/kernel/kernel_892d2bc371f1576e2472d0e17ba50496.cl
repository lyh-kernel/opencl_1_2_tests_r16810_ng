
__kernel void sample_test(__global uint *sourceA, __global uint *sourceB, __global uint *sourceC, __global uint *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
