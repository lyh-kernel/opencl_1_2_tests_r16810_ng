uint16 shuffle_fn( uint16 source );
uint16 shuffle_fn( uint16 source ) { return source; }

__kernel void sample_test( __global uint16 *source, __global uint4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uint16 src1 /*, src2*/;
  uint4 tmp;
		tmp = (uint4)((uint)0);
		tmp.s3 = shuffle_fn( source[0] ).Sc;
		dest[0] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2 = shuffle_fn( source[1] ).S9;
		dest[1] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s2 = shuffle_fn( source[2] ).s3;
		dest[2] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S2 = shuffle_fn( source[3] ).s5;
		dest[3] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0 = shuffle_fn( source[4] ).SC;
		dest[4] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s3 = shuffle_fn( source[5] ).S1;
		dest[5] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S1 = shuffle_fn( source[6] ).s2;
		dest[6] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S1 = shuffle_fn( source[7] ).sF;
		dest[7] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S03 = shuffle_fn( source[8] ).SAc;
		dest[8] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s02 = shuffle_fn( source[9] ).sab;
		dest[9] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S32 = shuffle_fn( source[10] ).S3B;
		dest[10] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s32 = shuffle_fn( source[11] ).sEf;
		dest[11] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S21 = shuffle_fn( source[12] ).s51;
		dest[12] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S13 = shuffle_fn( source[13] ).S09;
		dest[13] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S01 = shuffle_fn( source[14] ).s75;
		dest[14] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s03 = shuffle_fn( source[15] ).s7B;
		dest[15] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s20 = shuffle_fn( source[16] ).s88;
		dest[16] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s20 = shuffle_fn( source[17] ).SdE;
		dest[17] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s12 = shuffle_fn( source[18] ).se4;
		dest[18] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S20 = shuffle_fn( source[19] ).sA7;
		dest[19] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s30 = shuffle_fn( source[20] ).sd0;
		dest[20] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s01 = shuffle_fn( source[21] ).S74;
		dest[21] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S03 = shuffle_fn( source[22] ).s6D;
		dest[22] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s12 = shuffle_fn( source[23] ).S8c;
		dest[23] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S0321 = shuffle_fn( source[24] ).s6b7f;
		dest[24] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S2310 = shuffle_fn( source[25] ).S554b;
		dest[25] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S1302 = shuffle_fn( source[26] ).S740e;
		dest[26] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0231 = shuffle_fn( source[27] ).sd5F4;
		dest[27] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0231 = shuffle_fn( source[28] ).s5ea0;
		dest[28] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0321 = shuffle_fn( source[29] ).s17dc;
		dest[29] = tmp;
		tmp = (uint4)((uint)0);
		tmp.S2310 = shuffle_fn( source[30] ).s45b1;
		dest[30] = tmp;
		tmp = (uint4)((uint)0);
		tmp.s0312 = shuffle_fn( source[31] ).S2E47;
		dest[31] = tmp;
}
