
__kernel void sample_test(__global uint8 *sourceA, __global uint8 *sourceB, __global uint8 *sourceC, __global uint8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
