ulong3 shuffle_fn( ulong3 source );
ulong3 shuffle_fn( ulong3 source ) { return source; }

__kernel void sample_test( __global ulong *source, __global ulong8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //ulong3 src1 /*, src2*/;
  ulong8 tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S7 = shuffle_fn( vload3(0, source) ).s2;
		dest[0] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s5 = shuffle_fn( vload3(1, source) ).S2;
		dest[1] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S0 = shuffle_fn( vload3(2, source) ).s1;
		dest[2] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s6 = shuffle_fn( vload3(3, source) ).S0;
		dest[3] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S3 = shuffle_fn( vload3(4, source) ).s0;
		dest[4] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s0 = shuffle_fn( vload3(5, source) ).s0;
		dest[5] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S1 = shuffle_fn( vload3(6, source) ).s1;
		dest[6] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S0 = shuffle_fn( vload3(7, source) ).s2;
		dest[7] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S6 = shuffle_fn( vload3(8, source) ).S1;
		dest[8] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S3 = shuffle_fn( vload3(9, source) ).s0;
		dest[9] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s0 = shuffle_fn( vload3(10, source) ).S0;
		dest[10] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S67 = shuffle_fn( vload3(11, source) ).S22;
		dest[11] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s76 = shuffle_fn( vload3(12, source) ).s02;
		dest[12] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s45 = shuffle_fn( vload3(13, source) ).s01;
		dest[13] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S71 = shuffle_fn( vload3(14, source) ).S20;
		dest[14] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s71 = shuffle_fn( vload3(15, source) ).s10;
		dest[15] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S64 = shuffle_fn( vload3(16, source) ).s22;
		dest[16] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S50 = shuffle_fn( vload3(17, source) ).S12;
		dest[17] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S67 = shuffle_fn( vload3(18, source) ).s12;
		dest[18] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s74 = shuffle_fn( vload3(19, source) ).S01;
		dest[19] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S06 = shuffle_fn( vload3(20, source) ).s21;
		dest[20] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s47 = shuffle_fn( vload3(21, source) ).S21;
		dest[21] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s26 = shuffle_fn( vload3(22, source) ).S11;
		dest[22] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S63 = shuffle_fn( vload3(23, source) ).s02;
		dest[23] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S15 = shuffle_fn( vload3(24, source) ).S00;
		dest[24] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S21 = shuffle_fn( vload3(25, source) ).S21;
		dest[25] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S24 = shuffle_fn( vload3(26, source) ).s21;
		dest[26] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s35 = shuffle_fn( vload3(27, source) ).s22;
		dest[27] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S12 = shuffle_fn( vload3(28, source) ).s11;
		dest[28] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S62 = shuffle_fn( vload3(29, source) ).s12;
		dest[29] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.S67 = shuffle_fn( vload3(30, source) ).s10;
		dest[30] = tmp;
		tmp = (ulong8)((ulong)0);
		tmp.s74 = shuffle_fn( vload3(31, source) ).S10;
		dest[31] = tmp;
}
