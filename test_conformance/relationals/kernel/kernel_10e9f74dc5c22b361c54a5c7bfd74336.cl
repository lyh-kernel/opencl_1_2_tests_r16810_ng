__kernel void sample_test( __global uint16 *secondSource, __global uint16 *source, __global uint8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uint16 src1 , src2;
  uint8 tmp;
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 0 ];
			uint16 src2 = secondSource[ 0 ];
			uint8 mask = (uint8)( 16, 12, 10, 17, 31, 22, 3, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 1 ];
			uint16 src2 = secondSource[ 1 ];
			uint8 mask = (uint8)( 31, 25, 29, 12, 29, 23, 12, 13 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 2 ];
			uint16 src2 = secondSource[ 2 ];
			uint8 mask = (uint8)( 19, 25, 18, 25, 19, 12, 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 3 ];
			uint16 src2 = secondSource[ 3 ];
			uint8 mask = (uint8)( 26, 15, 11, 1, 29, 14, 5, 24 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 4 ];
			uint16 src2 = secondSource[ 4 ];
			uint8 mask = (uint8)( 18, 3, 19, 22, 0, 18, 6, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 5 ];
			uint16 src2 = secondSource[ 5 ];
			uint8 mask = (uint8)( 15, 30, 19, 20, 2, 5, 9, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 6 ];
			uint16 src2 = secondSource[ 6 ];
			uint8 mask = (uint8)( 17, 27, 21, 7, 26, 15, 24, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 7 ];
			uint16 src2 = secondSource[ 7 ];
			uint8 mask = (uint8)( 25, 23, 8, 24, 10, 27, 20, 21 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 8 ];
			uint16 src2 = secondSource[ 8 ];
			uint8 mask = (uint8)( 5, 11, 29, 19, 23, 15, 1, 17 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 9 ];
			uint16 src2 = secondSource[ 9 ];
			uint8 mask = (uint8)( 24, 23, 16, 20, 30, 8, 16, 18 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 10 ];
			uint16 src2 = secondSource[ 10 ];
			uint8 mask = (uint8)( 18, 22, 26, 11, 29, 9, 28, 30 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 11 ];
			uint16 src2 = secondSource[ 11 ];
			uint8 mask = (uint8)( 18, 5, 20, 12, 0, 15, 18, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 12 ];
			uint16 src2 = secondSource[ 12 ];
			uint8 mask = (uint8)( 5, 29, 18, 17, 31, 18, 11, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 13 ];
			uint16 src2 = secondSource[ 13 ];
			uint8 mask = (uint8)( 23, 10, 15, 12, 21, 18, 26, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 14 ];
			uint16 src2 = secondSource[ 14 ];
			uint8 mask = (uint8)( 22, 24, 9, 7, 27, 3, 31, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 15 ];
			uint16 src2 = secondSource[ 15 ];
			uint8 mask = (uint8)( 29, 22, 19, 31, 28, 14, 5, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 16 ];
			uint16 src2 = secondSource[ 16 ];
			uint8 mask = (uint8)( 6, 18, 8, 25, 24, 4, 7, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 17 ];
			uint16 src2 = secondSource[ 17 ];
			uint8 mask = (uint8)( 23, 23, 26, 14, 6, 18, 17, 21 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 18 ];
			uint16 src2 = secondSource[ 18 ];
			uint8 mask = (uint8)( 26, 6, 2, 4, 18, 18, 6, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 19 ];
			uint16 src2 = secondSource[ 19 ];
			uint8 mask = (uint8)( 28, 9, 28, 20, 19, 8, 11, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 20 ];
			uint16 src2 = secondSource[ 20 ];
			uint8 mask = (uint8)( 24, 23, 8, 27, 26, 21, 6, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 21 ];
			uint16 src2 = secondSource[ 21 ];
			uint8 mask = (uint8)( 4, 18, 22, 22, 22, 5, 9, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 22 ];
			uint16 src2 = secondSource[ 22 ];
			uint8 mask = (uint8)( 7, 23, 29, 19, 1, 5, 18, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 23 ];
			uint16 src2 = secondSource[ 23 ];
			uint8 mask = (uint8)( 25, 11, 30, 2, 23, 2, 25, 18 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 24 ];
			uint16 src2 = secondSource[ 24 ];
			uint8 mask = (uint8)( 29, 22, 14, 1, 2, 9, 7, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 25 ];
			uint16 src2 = secondSource[ 25 ];
			uint8 mask = (uint8)( 31, 27, 6, 11, 7, 19, 12, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 26 ];
			uint16 src2 = secondSource[ 26 ];
			uint8 mask = (uint8)( 27, 27, 25, 27, 18, 10, 16, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 27 ];
			uint16 src2 = secondSource[ 27 ];
			uint8 mask = (uint8)( 22, 21, 5, 18, 18, 26, 16, 18 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 28 ];
			uint16 src2 = secondSource[ 28 ];
			uint8 mask = (uint8)( 18, 8, 25, 5, 20, 22, 29, 28 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 29 ];
			uint16 src2 = secondSource[ 29 ];
			uint8 mask = (uint8)( 19, 16, 8, 29, 12, 16, 11, 28 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 30 ];
			uint16 src2 = secondSource[ 30 ];
			uint8 mask = (uint8)( 22, 5, 21, 13, 8, 4, 29, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (uint8)((uint)0);
		{
			uint16 src1 = source[ 31 ];
			uint16 src2 = secondSource[ 31 ];
			uint8 mask = (uint8)( 1, 4, 27, 1, 13, 1, 30, 30 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
