
__kernel void sample_test(__global long8 *sourceA, __global long8 *sourceB, __global long8 *sourceC, __global long8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
