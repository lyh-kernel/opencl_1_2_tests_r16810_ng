float2 shuffle_fn( float2 source );
float2 shuffle_fn( float2 source ) { return source; }

__kernel void sample_test( __global float2 *source, __global float16 *dest )
{
    if (get_global_id(0) != 0) return;
	 //float2 src1 /*, src2*/;
  float16 tmp;
		tmp = (float16)((float)0);
		tmp.sB = shuffle_fn( source[0] ).s1;
		dest[0] = tmp;
		tmp = (float16)((float)0);
		tmp.S0 = shuffle_fn( source[1] ).s1;
		dest[1] = tmp;
		tmp = (float16)((float)0);
		tmp.S3 = shuffle_fn( source[2] ).S0;
		dest[2] = tmp;
		tmp = (float16)((float)0);
		tmp.S8 = shuffle_fn( source[3] ).S1;
		dest[3] = tmp;
		tmp = (float16)((float)0);
		tmp.S0 = shuffle_fn( source[4] ).s0;
		dest[4] = tmp;
		tmp = (float16)((float)0);
		tmp.s0 = shuffle_fn( source[5] ).S0;
		dest[5] = tmp;
		tmp = (float16)((float)0);
		tmp.s5 = shuffle_fn( source[6] ).s1;
		dest[6] = tmp;
		tmp = (float16)((float)0);
		tmp.sd = shuffle_fn( source[7] ).s0;
		dest[7] = tmp;
		tmp = (float16)((float)0);
		tmp.s4 = shuffle_fn( source[8] ).S0;
		dest[8] = tmp;
		tmp = (float16)((float)0);
		tmp.Se = shuffle_fn( source[9] ).s0;
		dest[9] = tmp;
		tmp = (float16)((float)0);
		tmp.sA = shuffle_fn( source[10] ).s0;
		dest[10] = tmp;
		tmp = (float16)((float)0);
		tmp.s4 = shuffle_fn( source[11] ).S1;
		dest[11] = tmp;
		tmp = (float16)((float)0);
		tmp.S7 = shuffle_fn( source[12] ).S1;
		dest[12] = tmp;
		tmp = (float16)((float)0);
		tmp.S3 = shuffle_fn( source[13] ).S0;
		dest[13] = tmp;
		tmp = (float16)((float)0);
		tmp.S3 = shuffle_fn( source[14] ).S0;
		dest[14] = tmp;
		tmp = (float16)((float)0);
		tmp.S7 = shuffle_fn( source[15] ).s1;
		dest[15] = tmp;
		tmp = (float16)((float)0);
		tmp.SFC = shuffle_fn( source[16] ).s00;
		dest[16] = tmp;
		tmp = (float16)((float)0);
		tmp.S1E = shuffle_fn( source[17] ).s10;
		dest[17] = tmp;
		tmp = (float16)((float)0);
		tmp.s0b = shuffle_fn( source[18] ).S11;
		dest[18] = tmp;
		tmp = (float16)((float)0);
		tmp.S8F = shuffle_fn( source[19] ).s00;
		dest[19] = tmp;
		tmp = (float16)((float)0);
		tmp.SE1 = shuffle_fn( source[20] ).S10;
		dest[20] = tmp;
		tmp = (float16)((float)0);
		tmp.s8b = shuffle_fn( source[21] ).s01;
		dest[21] = tmp;
		tmp = (float16)((float)0);
		tmp.SDc = shuffle_fn( source[22] ).S01;
		dest[22] = tmp;
		tmp = (float16)((float)0);
		tmp.Sf0 = shuffle_fn( source[23] ).s00;
		dest[23] = tmp;
		tmp = (float16)((float)0);
		tmp.S7a = shuffle_fn( source[24] ).S01;
		dest[24] = tmp;
		tmp = (float16)((float)0);
		tmp.S02 = shuffle_fn( source[25] ).S00;
		dest[25] = tmp;
		tmp = (float16)((float)0);
		tmp.Sf7 = shuffle_fn( source[26] ).s11;
		dest[26] = tmp;
		tmp = (float16)((float)0);
		tmp.s61 = shuffle_fn( source[27] ).S11;
		dest[27] = tmp;
		tmp = (float16)((float)0);
		tmp.S71 = shuffle_fn( source[28] ).S00;
		dest[28] = tmp;
		tmp = (float16)((float)0);
		tmp.S85 = shuffle_fn( source[29] ).s10;
		dest[29] = tmp;
		tmp = (float16)((float)0);
		tmp.S9f = shuffle_fn( source[30] ).S11;
		dest[30] = tmp;
		tmp = (float16)((float)0);
		tmp.sAc = shuffle_fn( source[31] ).s11;
		dest[31] = tmp;
}
