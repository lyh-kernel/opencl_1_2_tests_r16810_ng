
__kernel void sample_test(__global int4 *sourceA, __global int4 *sourceB, __global int4 *sourceC, __global int4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
