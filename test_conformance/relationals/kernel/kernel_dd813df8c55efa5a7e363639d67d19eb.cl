
__kernel void sample_test(__global short8 *sourceA, __global short8 *sourceB, __global short8 *sourceC, __global short8 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
