int4 shuffle_fn( int4 source );
int4 shuffle_fn( int4 source ) { return source; }

__kernel void sample_test( __global int4 *source, __global int8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //int4 src1 /*, src2*/;
  int8 tmp;
		tmp = (int8)((int)0);
		tmp.s4 = shuffle_fn( source[0] ).S3;
		dest[0] = tmp;
		tmp = (int8)((int)0);
		tmp.S4 = shuffle_fn( source[1] ).S2;
		dest[1] = tmp;
		tmp = (int8)((int)0);
		tmp.S0 = shuffle_fn( source[2] ).s2;
		dest[2] = tmp;
		tmp = (int8)((int)0);
		tmp.S3 = shuffle_fn( source[3] ).s1;
		dest[3] = tmp;
		tmp = (int8)((int)0);
		tmp.S0 = shuffle_fn( source[4] ).S0;
		dest[4] = tmp;
		tmp = (int8)((int)0);
		tmp.s5 = shuffle_fn( source[5] ).s0;
		dest[5] = tmp;
		tmp = (int8)((int)0);
		tmp.S0 = shuffle_fn( source[6] ).S1;
		dest[6] = tmp;
		tmp = (int8)((int)0);
		tmp.s0 = shuffle_fn( source[7] ).s3;
		dest[7] = tmp;
		tmp = (int8)((int)0);
		tmp.S31 = shuffle_fn( source[8] ).s31;
		dest[8] = tmp;
		tmp = (int8)((int)0);
		tmp.s45 = shuffle_fn( source[9] ).s01;
		dest[9] = tmp;
		tmp = (int8)((int)0);
		tmp.S46 = shuffle_fn( source[10] ).S23;
		dest[10] = tmp;
		tmp = (int8)((int)0);
		tmp.S71 = shuffle_fn( source[11] ).S30;
		dest[11] = tmp;
		tmp = (int8)((int)0);
		tmp.s52 = shuffle_fn( source[12] ).S13;
		dest[12] = tmp;
		tmp = (int8)((int)0);
		tmp.S73 = shuffle_fn( source[13] ).s03;
		dest[13] = tmp;
		tmp = (int8)((int)0);
		tmp.S53 = shuffle_fn( source[14] ).s22;
		dest[14] = tmp;
		tmp = (int8)((int)0);
		tmp.S63 = shuffle_fn( source[15] ).s10;
		dest[15] = tmp;
		tmp = (int8)((int)0);
		tmp.S53 = shuffle_fn( source[16] ).S31;
		dest[16] = tmp;
		tmp = (int8)((int)0);
		tmp.S32 = shuffle_fn( source[17] ).s30;
		dest[17] = tmp;
		tmp = (int8)((int)0);
		tmp.s15 = shuffle_fn( source[18] ).S21;
		dest[18] = tmp;
		tmp = (int8)((int)0);
		tmp.S10 = shuffle_fn( source[19] ).S02;
		dest[19] = tmp;
		tmp = (int8)((int)0);
		tmp.s72 = shuffle_fn( source[20] ).s11;
		dest[20] = tmp;
		tmp = (int8)((int)0);
		tmp.S65 = shuffle_fn( source[21] ).s13;
		dest[21] = tmp;
		tmp = (int8)((int)0);
		tmp.s60 = shuffle_fn( source[22] ).S20;
		dest[22] = tmp;
		tmp = (int8)((int)0);
		tmp.s73 = shuffle_fn( source[23] ).S33;
		dest[23] = tmp;
		tmp = (int8)((int)0);
		tmp.s6703 = shuffle_fn( source[24] ).s2301;
		dest[24] = tmp;
		tmp = (int8)((int)0);
		tmp.S1067 = shuffle_fn( source[25] ).S0312;
		dest[25] = tmp;
		tmp = (int8)((int)0);
		tmp.S1436 = shuffle_fn( source[26] ).S0333;
		dest[26] = tmp;
		tmp = (int8)((int)0);
		tmp.s3071 = shuffle_fn( source[27] ).S3220;
		dest[27] = tmp;
		tmp = (int8)((int)0);
		tmp.S2730 = shuffle_fn( source[28] ).S0213;
		dest[28] = tmp;
		tmp = (int8)((int)0);
		tmp.S1254 = shuffle_fn( source[29] ).s2132;
		dest[29] = tmp;
		tmp = (int8)((int)0);
		tmp.S4206 = shuffle_fn( source[30] ).s0302;
		dest[30] = tmp;
		tmp = (int8)((int)0);
		tmp.s0523 = shuffle_fn( source[31] ).S0000;
		dest[31] = tmp;
}
