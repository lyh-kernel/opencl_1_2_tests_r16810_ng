
__kernel void sample_test(__global uchar16 *sourceA, __global uchar16 *sourceB, __global uchar16 *sourceC, __global uchar16 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
