__kernel void sample_test( __global long16 *secondSource, __global long16 *source, __global long4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //long16 src1 , src2;
  long4 tmp;
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 0 ];
			long16 src2 = secondSource[ 0 ];
			ulong4 mask = (ulong4)( 9, 8, 4, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 1 ];
			long16 src2 = secondSource[ 1 ];
			ulong4 mask = (ulong4)( 20, 20, 17, 23 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 2 ];
			long16 src2 = secondSource[ 2 ];
			ulong4 mask = (ulong4)( 0, 29, 14, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 3 ];
			long16 src2 = secondSource[ 3 ];
			ulong4 mask = (ulong4)( 11, 30, 1, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 4 ];
			long16 src2 = secondSource[ 4 ];
			ulong4 mask = (ulong4)( 18, 21, 29, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 5 ];
			long16 src2 = secondSource[ 5 ];
			ulong4 mask = (ulong4)( 12, 8, 26, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 6 ];
			long16 src2 = secondSource[ 6 ];
			ulong4 mask = (ulong4)( 14, 28, 4, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 7 ];
			long16 src2 = secondSource[ 7 ];
			ulong4 mask = (ulong4)( 28, 1, 5, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 8 ];
			long16 src2 = secondSource[ 8 ];
			ulong4 mask = (ulong4)( 6, 3, 11, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 9 ];
			long16 src2 = secondSource[ 9 ];
			ulong4 mask = (ulong4)( 2, 6, 2, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 10 ];
			long16 src2 = secondSource[ 10 ];
			ulong4 mask = (ulong4)( 24, 0, 5, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 11 ];
			long16 src2 = secondSource[ 11 ];
			ulong4 mask = (ulong4)( 24, 11, 29, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 12 ];
			long16 src2 = secondSource[ 12 ];
			ulong4 mask = (ulong4)( 6, 12, 26, 28 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 13 ];
			long16 src2 = secondSource[ 13 ];
			ulong4 mask = (ulong4)( 28, 30, 16, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 14 ];
			long16 src2 = secondSource[ 14 ];
			ulong4 mask = (ulong4)( 30, 4, 0, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 15 ];
			long16 src2 = secondSource[ 15 ];
			ulong4 mask = (ulong4)( 20, 26, 15, 21 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 16 ];
			long16 src2 = secondSource[ 16 ];
			ulong4 mask = (ulong4)( 21, 15, 11, 12 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 17 ];
			long16 src2 = secondSource[ 17 ];
			ulong4 mask = (ulong4)( 22, 20, 28, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 18 ];
			long16 src2 = secondSource[ 18 ];
			ulong4 mask = (ulong4)( 11, 3, 30, 26 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 19 ];
			long16 src2 = secondSource[ 19 ];
			ulong4 mask = (ulong4)( 30, 19, 16, 23 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 20 ];
			long16 src2 = secondSource[ 20 ];
			ulong4 mask = (ulong4)( 0, 12, 22, 15 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 21 ];
			long16 src2 = secondSource[ 21 ];
			ulong4 mask = (ulong4)( 22, 17, 13, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 22 ];
			long16 src2 = secondSource[ 22 ];
			ulong4 mask = (ulong4)( 0, 7, 9, 12 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 23 ];
			long16 src2 = secondSource[ 23 ];
			ulong4 mask = (ulong4)( 23, 27, 21, 22 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 24 ];
			long16 src2 = secondSource[ 24 ];
			ulong4 mask = (ulong4)( 16, 31, 25, 13 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 25 ];
			long16 src2 = secondSource[ 25 ];
			ulong4 mask = (ulong4)( 17, 17, 28, 31 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 26 ];
			long16 src2 = secondSource[ 26 ];
			ulong4 mask = (ulong4)( 22, 25, 7, 27 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 27 ];
			long16 src2 = secondSource[ 27 ];
			ulong4 mask = (ulong4)( 28, 4, 19, 28 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 28 ];
			long16 src2 = secondSource[ 28 ];
			ulong4 mask = (ulong4)( 29, 4, 24, 18 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 29 ];
			long16 src2 = secondSource[ 29 ];
			ulong4 mask = (ulong4)( 18, 22, 24, 14 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 30 ];
			long16 src2 = secondSource[ 30 ];
			ulong4 mask = (ulong4)( 23, 1, 14, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (long4)((long)0);
		{
			long16 src1 = source[ 31 ];
			long16 src2 = secondSource[ 31 ];
			ulong4 mask = (ulong4)( 24, 6, 27, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
