
__kernel void sample_test(__global ushort2 *sourceA, __global ushort2 *sourceB, __global ushort2 *sourceC, __global ushort2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
