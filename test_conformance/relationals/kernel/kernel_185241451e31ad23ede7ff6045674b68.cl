
__kernel void sample_test(__global int2 *sourceA, __global int2 *sourceB, __global int2 *sourceC, __global int2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = select( sourceA[tid], sourceB[tid], sourceC[tid] );

}
