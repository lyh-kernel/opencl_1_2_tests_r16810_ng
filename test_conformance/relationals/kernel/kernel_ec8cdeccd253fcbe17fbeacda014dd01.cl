__kernel void sample_test( __global uchar16 *source, __global uchar2 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uchar16 src1 /*, src2*/;
  uchar2 tmp;
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 0 ];
			uchar2 mask = (uchar2)( 7, 14 );
			tmp = shuffle( src1, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 1 ];
			uchar2 mask = (uchar2)( 4, 0 );
			tmp = shuffle( src1, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 2 ];
			uchar2 mask = (uchar2)( 4, 8 );
			tmp = shuffle( src1, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 3 ];
			uchar2 mask = (uchar2)( 15, 9 );
			tmp = shuffle( src1, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 4 ];
			uchar2 mask = (uchar2)( 0, 0 );
			tmp = shuffle( src1, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 5 ];
			uchar2 mask = (uchar2)( 0, 9 );
			tmp = shuffle( src1, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 6 ];
			uchar2 mask = (uchar2)( 14, 13 );
			tmp = shuffle( src1, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 7 ];
			uchar2 mask = (uchar2)( 2, 9 );
			tmp = shuffle( src1, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 8 ];
			uchar2 mask = (uchar2)( 0, 2 );
			tmp = shuffle( src1, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 9 ];
			uchar2 mask = (uchar2)( 7, 8 );
			tmp = shuffle( src1, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 10 ];
			uchar2 mask = (uchar2)( 14, 11 );
			tmp = shuffle( src1, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 11 ];
			uchar2 mask = (uchar2)( 4, 10 );
			tmp = shuffle( src1, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 12 ];
			uchar2 mask = (uchar2)( 14, 9 );
			tmp = shuffle( src1, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 13 ];
			uchar2 mask = (uchar2)( 13, 6 );
			tmp = shuffle( src1, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 14 ];
			uchar2 mask = (uchar2)( 4, 1 );
			tmp = shuffle( src1, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 15 ];
			uchar2 mask = (uchar2)( 6, 1 );
			tmp = shuffle( src1, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 16 ];
			uchar2 mask = (uchar2)( 14, 9 );
			tmp = shuffle( src1, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 17 ];
			uchar2 mask = (uchar2)( 1, 5 );
			tmp = shuffle( src1, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 18 ];
			uchar2 mask = (uchar2)( 0, 0 );
			tmp = shuffle( src1, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 19 ];
			uchar2 mask = (uchar2)( 3, 2 );
			tmp = shuffle( src1, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 20 ];
			uchar2 mask = (uchar2)( 3, 11 );
			tmp = shuffle( src1, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 21 ];
			uchar2 mask = (uchar2)( 2, 12 );
			tmp = shuffle( src1, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 22 ];
			uchar2 mask = (uchar2)( 0, 9 );
			tmp = shuffle( src1, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 23 ];
			uchar2 mask = (uchar2)( 11, 9 );
			tmp = shuffle( src1, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 24 ];
			uchar2 mask = (uchar2)( 2, 4 );
			tmp = shuffle( src1, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 25 ];
			uchar2 mask = (uchar2)( 7, 7 );
			tmp = shuffle( src1, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 26 ];
			uchar2 mask = (uchar2)( 1, 15 );
			tmp = shuffle( src1, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 27 ];
			uchar2 mask = (uchar2)( 5, 6 );
			tmp = shuffle( src1, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 28 ];
			uchar2 mask = (uchar2)( 6, 9 );
			tmp = shuffle( src1, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 29 ];
			uchar2 mask = (uchar2)( 14, 5 );
			tmp = shuffle( src1, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 30 ];
			uchar2 mask = (uchar2)( 11, 7 );
			tmp = shuffle( src1, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (uchar2)((uchar)0);
		{
			uchar16 src1 = source[ 31 ];
			uchar2 mask = (uchar2)( 14, 12 );
			tmp = shuffle( src1, mask );
			dest[ 31 ] = tmp;
		}
}
