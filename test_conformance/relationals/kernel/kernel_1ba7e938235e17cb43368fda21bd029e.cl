
__kernel void sample_test(__global uint4 *sourceA, __global uint4 *sourceB, __global uint4 *sourceC, __global uint4 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
