
__kernel void sample_test(__global short3 *sourceA, __global int *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = any(vload3(tid, (__global short *)sourceA));

}
