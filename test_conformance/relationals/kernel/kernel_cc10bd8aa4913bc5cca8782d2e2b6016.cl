__kernel void sample_test( __global ulong8 *secondSource, __global ulong8 *source, __global ulong8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //ulong8 src1 , src2;
  ulong8 tmp;
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 0 ];
			ulong8 src2 = secondSource[ 0 ];
			ulong8 mask = (ulong8)( 9, 6, 9, 3, 14, 2, 10, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 1 ];
			ulong8 src2 = secondSource[ 1 ];
			ulong8 mask = (ulong8)( 12, 4, 6, 7, 11, 11, 15, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 2 ];
			ulong8 src2 = secondSource[ 2 ];
			ulong8 mask = (ulong8)( 6, 3, 3, 11, 10, 9, 3, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 3 ];
			ulong8 src2 = secondSource[ 3 ];
			ulong8 mask = (ulong8)( 0, 4, 14, 0, 3, 15, 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 4 ];
			ulong8 src2 = secondSource[ 4 ];
			ulong8 mask = (ulong8)( 7, 1, 2, 7, 2, 0, 11, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 5 ];
			ulong8 src2 = secondSource[ 5 ];
			ulong8 mask = (ulong8)( 2, 15, 0, 2, 14, 1, 2, 10 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 6 ];
			ulong8 src2 = secondSource[ 6 ];
			ulong8 mask = (ulong8)( 3, 6, 13, 4, 13, 14, 14, 13 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 7 ];
			ulong8 src2 = secondSource[ 7 ];
			ulong8 mask = (ulong8)( 12, 8, 5, 7, 1, 9, 8, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 8 ];
			ulong8 src2 = secondSource[ 8 ];
			ulong8 mask = (ulong8)( 2, 3, 1, 6, 6, 3, 15, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 9 ];
			ulong8 src2 = secondSource[ 9 ];
			ulong8 mask = (ulong8)( 0, 8, 6, 10, 15, 13, 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 10 ];
			ulong8 src2 = secondSource[ 10 ];
			ulong8 mask = (ulong8)( 15, 1, 14, 7, 11, 12, 10, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 11 ];
			ulong8 src2 = secondSource[ 11 ];
			ulong8 mask = (ulong8)( 14, 9, 10, 6, 12, 0, 0, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 12 ];
			ulong8 src2 = secondSource[ 12 ];
			ulong8 mask = (ulong8)( 14, 0, 14, 11, 5, 13, 5, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 13 ];
			ulong8 src2 = secondSource[ 13 ];
			ulong8 mask = (ulong8)( 9, 6, 7, 8, 7, 8, 0, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 14 ];
			ulong8 src2 = secondSource[ 14 ];
			ulong8 mask = (ulong8)( 5, 0, 1, 4, 11, 8, 9, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 15 ];
			ulong8 src2 = secondSource[ 15 ];
			ulong8 mask = (ulong8)( 7, 9, 3, 9, 12, 7, 7, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 16 ];
			ulong8 src2 = secondSource[ 16 ];
			ulong8 mask = (ulong8)( 12, 11, 8, 3, 8, 14, 3, 14 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 17 ];
			ulong8 src2 = secondSource[ 17 ];
			ulong8 mask = (ulong8)( 13, 6, 1, 4, 5, 3, 9, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 18 ];
			ulong8 src2 = secondSource[ 18 ];
			ulong8 mask = (ulong8)( 12, 2, 8, 2, 1, 11, 10, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 19 ];
			ulong8 src2 = secondSource[ 19 ];
			ulong8 mask = (ulong8)( 1, 0, 6, 3, 14, 4, 3, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 20 ];
			ulong8 src2 = secondSource[ 20 ];
			ulong8 mask = (ulong8)( 8, 15, 4, 0, 2, 14, 11, 9 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 21 ];
			ulong8 src2 = secondSource[ 21 ];
			ulong8 mask = (ulong8)( 14, 8, 4, 5, 0, 8, 15, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 22 ];
			ulong8 src2 = secondSource[ 22 ];
			ulong8 mask = (ulong8)( 10, 11, 3, 1, 3, 4, 7, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 23 ];
			ulong8 src2 = secondSource[ 23 ];
			ulong8 mask = (ulong8)( 7, 11, 6, 15, 4, 7, 5, 14 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 24 ];
			ulong8 src2 = secondSource[ 24 ];
			ulong8 mask = (ulong8)( 1, 0, 3, 10, 5, 6, 3, 12 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 25 ];
			ulong8 src2 = secondSource[ 25 ];
			ulong8 mask = (ulong8)( 3, 12, 15, 8, 2, 12, 6, 11 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 26 ];
			ulong8 src2 = secondSource[ 26 ];
			ulong8 mask = (ulong8)( 14, 10, 10, 8, 15, 11, 1, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 27 ];
			ulong8 src2 = secondSource[ 27 ];
			ulong8 mask = (ulong8)( 7, 7, 15, 15, 11, 1, 13, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 28 ];
			ulong8 src2 = secondSource[ 28 ];
			ulong8 mask = (ulong8)( 5, 8, 9, 15, 6, 6, 14, 8 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 29 ];
			ulong8 src2 = secondSource[ 29 ];
			ulong8 mask = (ulong8)( 11, 15, 5, 1, 5, 4, 12, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 30 ];
			ulong8 src2 = secondSource[ 30 ];
			ulong8 mask = (ulong8)( 8, 6, 0, 15, 12, 0, 1, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (ulong8)((ulong)0);
		{
			ulong8 src1 = source[ 31 ];
			ulong8 src2 = secondSource[ 31 ];
			ulong8 mask = (ulong8)( 4, 14, 3, 5, 14, 5, 13, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
