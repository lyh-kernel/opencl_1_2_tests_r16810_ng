uchar2 shuffle_fn( uchar2 source );
uchar2 shuffle_fn( uchar2 source ) { return source; }

__kernel void sample_test( __global uchar2 *source, __global uchar4 *dest )
{
    if (get_global_id(0) != 0) return;
	 //uchar2 src1 /*, src2*/;
  uchar4 tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S1 = shuffle_fn( source[0] ).s0;
		dest[0] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s2 = shuffle_fn( source[1] ).s1;
		dest[1] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s2 = shuffle_fn( source[2] ).S0;
		dest[2] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S1 = shuffle_fn( source[3] ).S0;
		dest[3] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S0 = shuffle_fn( source[4] ).s0;
		dest[4] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S3 = shuffle_fn( source[5] ).s1;
		dest[5] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S3 = shuffle_fn( source[6] ).s0;
		dest[6] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S3 = shuffle_fn( source[7] ).S1;
		dest[7] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S0 = shuffle_fn( source[8] ).s1;
		dest[8] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s1 = shuffle_fn( source[9] ).S1;
		dest[9] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S3 = shuffle_fn( source[10] ).s1;
		dest[10] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s2 = shuffle_fn( source[11] ).S1;
		dest[11] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S1 = shuffle_fn( source[12] ).S0;
		dest[12] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s3 = shuffle_fn( source[13] ).S0;
		dest[13] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s2 = shuffle_fn( source[14] ).s1;
		dest[14] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S2 = shuffle_fn( source[15] ).S1;
		dest[15] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s31 = shuffle_fn( source[16] ).S01;
		dest[16] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s32 = shuffle_fn( source[17] ).s11;
		dest[17] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S12 = shuffle_fn( source[18] ).s01;
		dest[18] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s31 = shuffle_fn( source[19] ).S01;
		dest[19] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s30 = shuffle_fn( source[20] ).S11;
		dest[20] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S01 = shuffle_fn( source[21] ).s10;
		dest[21] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s31 = shuffle_fn( source[22] ).s01;
		dest[22] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s32 = shuffle_fn( source[23] ).S01;
		dest[23] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s20 = shuffle_fn( source[24] ).s10;
		dest[24] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s23 = shuffle_fn( source[25] ).s00;
		dest[25] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S01 = shuffle_fn( source[26] ).s11;
		dest[26] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S21 = shuffle_fn( source[27] ).s10;
		dest[27] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S31 = shuffle_fn( source[28] ).S11;
		dest[28] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s10 = shuffle_fn( source[29] ).s10;
		dest[29] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.s02 = shuffle_fn( source[30] ).s11;
		dest[30] = tmp;
		tmp = (uchar4)((uchar)0);
		tmp.S01 = shuffle_fn( source[31] ).s11;
		dest[31] = tmp;
}
