__kernel void sample_test( __global int4 *secondSource, __global int4 *source, __global int2 *dest )
{
    if (get_global_id(0) != 0) return;
	 //int4 src1 , src2;
  int2 tmp;
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 0 ];
			int4 src2 = secondSource[ 0 ];
			uint2 mask = (uint2)( 2, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 0 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 1 ];
			int4 src2 = secondSource[ 1 ];
			uint2 mask = (uint2)( 5, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 1 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 2 ];
			int4 src2 = secondSource[ 2 ];
			uint2 mask = (uint2)( 5, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 2 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 3 ];
			int4 src2 = secondSource[ 3 ];
			uint2 mask = (uint2)( 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 3 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 4 ];
			int4 src2 = secondSource[ 4 ];
			uint2 mask = (uint2)( 4, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 4 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 5 ];
			int4 src2 = secondSource[ 5 ];
			uint2 mask = (uint2)( 5, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 5 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 6 ];
			int4 src2 = secondSource[ 6 ];
			uint2 mask = (uint2)( 1, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 6 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 7 ];
			int4 src2 = secondSource[ 7 ];
			uint2 mask = (uint2)( 7, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 7 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 8 ];
			int4 src2 = secondSource[ 8 ];
			uint2 mask = (uint2)( 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 8 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 9 ];
			int4 src2 = secondSource[ 9 ];
			uint2 mask = (uint2)( 6, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 9 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 10 ];
			int4 src2 = secondSource[ 10 ];
			uint2 mask = (uint2)( 0, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 10 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 11 ];
			int4 src2 = secondSource[ 11 ];
			uint2 mask = (uint2)( 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 11 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 12 ];
			int4 src2 = secondSource[ 12 ];
			uint2 mask = (uint2)( 1, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 12 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 13 ];
			int4 src2 = secondSource[ 13 ];
			uint2 mask = (uint2)( 1, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 13 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 14 ];
			int4 src2 = secondSource[ 14 ];
			uint2 mask = (uint2)( 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 14 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 15 ];
			int4 src2 = secondSource[ 15 ];
			uint2 mask = (uint2)( 0, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 15 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 16 ];
			int4 src2 = secondSource[ 16 ];
			uint2 mask = (uint2)( 3, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 16 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 17 ];
			int4 src2 = secondSource[ 17 ];
			uint2 mask = (uint2)( 5, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 17 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 18 ];
			int4 src2 = secondSource[ 18 ];
			uint2 mask = (uint2)( 5, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 18 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 19 ];
			int4 src2 = secondSource[ 19 ];
			uint2 mask = (uint2)( 7, 6 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 19 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 20 ];
			int4 src2 = secondSource[ 20 ];
			uint2 mask = (uint2)( 0, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 20 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 21 ];
			int4 src2 = secondSource[ 21 ];
			uint2 mask = (uint2)( 4, 3 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 21 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 22 ];
			int4 src2 = secondSource[ 22 ];
			uint2 mask = (uint2)( 3, 0 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 22 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 23 ];
			int4 src2 = secondSource[ 23 ];
			uint2 mask = (uint2)( 6, 2 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 23 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 24 ];
			int4 src2 = secondSource[ 24 ];
			uint2 mask = (uint2)( 6, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 24 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 25 ];
			int4 src2 = secondSource[ 25 ];
			uint2 mask = (uint2)( 5, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 25 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 26 ];
			int4 src2 = secondSource[ 26 ];
			uint2 mask = (uint2)( 6, 4 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 26 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 27 ];
			int4 src2 = secondSource[ 27 ];
			uint2 mask = (uint2)( 7, 5 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 27 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 28 ];
			int4 src2 = secondSource[ 28 ];
			uint2 mask = (uint2)( 3, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 28 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 29 ];
			int4 src2 = secondSource[ 29 ];
			uint2 mask = (uint2)( 0, 7 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 29 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 30 ];
			int4 src2 = secondSource[ 30 ];
			uint2 mask = (uint2)( 6, 6 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 30 ] = tmp;
		}
		tmp = (int2)((int)0);
		{
			int4 src1 = source[ 31 ];
			int4 src2 = secondSource[ 31 ];
			uint2 mask = (uint2)( 2, 1 );
			tmp = shuffle2( src1, src2, mask );
			dest[ 31 ] = tmp;
		}
}
