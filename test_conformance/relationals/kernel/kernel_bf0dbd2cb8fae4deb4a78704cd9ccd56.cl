short16 shuffle_fn( short16 source );
short16 shuffle_fn( short16 source ) { return source; }

__kernel void sample_test( __global short16 *source, __global short8 *dest )
{
    if (get_global_id(0) != 0) return;
	 //short16 src1 /*, src2*/;
  short8 tmp;
		tmp = (short8)((short)0);
		tmp.s2 = shuffle_fn( source[0] ).sB;
		dest[0] = tmp;
		tmp = (short8)((short)0);
		tmp.S6 = shuffle_fn( source[1] ).S2;
		dest[1] = tmp;
		tmp = (short8)((short)0);
		tmp.S5 = shuffle_fn( source[2] ).s5;
		dest[2] = tmp;
		tmp = (short8)((short)0);
		tmp.s4 = shuffle_fn( source[3] ).S6;
		dest[3] = tmp;
		tmp = (short8)((short)0);
		tmp.S24 = shuffle_fn( source[4] ).S54;
		dest[4] = tmp;
		tmp = (short8)((short)0);
		tmp.S71 = shuffle_fn( source[5] ).s35;
		dest[5] = tmp;
		tmp = (short8)((short)0);
		tmp.s75 = shuffle_fn( source[6] ).s67;
		dest[6] = tmp;
		tmp = (short8)((short)0);
		tmp.s51 = shuffle_fn( source[7] ).SE8;
		dest[7] = tmp;
		tmp = (short8)((short)0);
		tmp.S13 = shuffle_fn( source[8] ).s48;
		dest[8] = tmp;
		tmp = (short8)((short)0);
		tmp.S26 = shuffle_fn( source[9] ).S08;
		dest[9] = tmp;
		tmp = (short8)((short)0);
		tmp.S42 = shuffle_fn( source[10] ).S52;
		dest[10] = tmp;
		tmp = (short8)((short)0);
		tmp.S13 = shuffle_fn( source[11] ).s39;
		dest[11] = tmp;
		tmp = (short8)((short)0);
		tmp.s0615 = shuffle_fn( source[12] ).SB5b5;
		dest[12] = tmp;
		tmp = (short8)((short)0);
		tmp.S3256 = shuffle_fn( source[13] ).S4c61;
		dest[13] = tmp;
		tmp = (short8)((short)0);
		tmp.S7256 = shuffle_fn( source[14] ).S9cb6;
		dest[14] = tmp;
		tmp = (short8)((short)0);
		tmp.S6073 = shuffle_fn( source[15] ).S7D85;
		dest[15] = tmp;
		tmp = (short8)((short)0);
		tmp.s6472 = shuffle_fn( source[16] ).sb382;
		dest[16] = tmp;
		tmp = (short8)((short)0);
		tmp.s5472 = shuffle_fn( source[17] ).sD810;
		dest[17] = tmp;
		tmp = (short8)((short)0);
		tmp.S4701 = shuffle_fn( source[18] ).S359E;
		dest[18] = tmp;
		tmp = (short8)((short)0);
		tmp.s4365 = shuffle_fn( source[19] ).saF25;
		dest[19] = tmp;
		tmp = (short8)((short)0);
		tmp.S2071 = shuffle_fn( source[20] ).S01Ab;
		dest[20] = tmp;
		tmp = (short8)((short)0);
		tmp.S0241 = shuffle_fn( source[21] ).s0306;
		dest[21] = tmp;
		tmp = (short8)((short)0);
		tmp.S5042 = shuffle_fn( source[22] ).s6242;
		dest[22] = tmp;
		tmp = (short8)((short)0);
		tmp.s2354 = shuffle_fn( source[23] ).Sc893;
		dest[23] = tmp;
		tmp = (short8)((short)0);
		tmp.S0137 = shuffle_fn( source[24] ).s7A14;
		dest[24] = tmp;
		tmp = (short8)((short)0);
		tmp.S0436 = shuffle_fn( source[25] ).S7132;
		dest[25] = tmp;
		tmp = (short8)((short)0);
		tmp.S2501 = shuffle_fn( source[26] ).sdf38;
		dest[26] = tmp;
		tmp = (short8)((short)0);
		tmp.s2670 = shuffle_fn( source[27] ).s9514;
		dest[27] = tmp;
		tmp = (short8)((short)0);
		tmp.s43271605 = shuffle_fn( source[28] ).Se352b1b2;
		dest[28] = tmp;
		tmp = (short8)((short)0);
		tmp.S54721360 = shuffle_fn( source[29] ).s0e7402e4;
		dest[29] = tmp;
		tmp = (short8)((short)0);
		tmp.s27630451 = shuffle_fn( source[30] ).s7B7A222F;
		dest[30] = tmp;
		tmp = (short8)((short)0);
		tmp.s14236075 = shuffle_fn( source[31] ).Sf1E59023;
		dest[31] = tmp;
}
