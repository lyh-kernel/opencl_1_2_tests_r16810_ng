
__kernel void sample_test(__global uchar2 *sourceA, __global uchar2 *sourceB, __global uchar2 *sourceC, __global uchar2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
