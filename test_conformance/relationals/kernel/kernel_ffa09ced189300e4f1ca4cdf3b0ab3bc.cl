
__kernel void sample_test(__global short2 *sourceA, __global short2 *sourceB, __global short2 *sourceC, __global short2 *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = bitselect( sourceA[tid], sourceB[tid], sourceC[tid] );

}
