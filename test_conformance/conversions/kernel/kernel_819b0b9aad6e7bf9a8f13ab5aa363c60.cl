__kernel void test_convert_short4_sat_rtz_float4( __global float4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4_sat_rtz( src[i] );
}
