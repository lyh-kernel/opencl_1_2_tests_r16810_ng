__kernel void test_convert_uint2_rtp_ulong2( __global ulong2 *src, __global uint2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint2_rtp( src[i] );
}
