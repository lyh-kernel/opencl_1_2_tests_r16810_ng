__kernel void test_convert_uint3_sat_rtz_short3( __global short *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   if( i + 1 < get_global_size(0))
       vstore3( convert_uint3_sat_rtz( vload3( i, src)), i, dest );
   else
   {
       short3 in;
       uint3 out;
       if( 0 == (i & 1) )
           in.y = src[3*i+1];
       in.x = src[3*i];
       out = convert_uint3_sat_rtz( in ); 
       dest[3*i] = out.x;
       if( 0 == (i & 1) )
           dest[3*i+1] = out.y;
   }
}
