__kernel void test_convert_short8_sat_rte_char8( __global char8 *src, __global short8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short8_sat_rte( src[i] );
}
