__kernel void test_convert_int16_rte_short16( __global short16 *src, __global int16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int16_rte( src[i] );
}
