__kernel void test_convert_long4_sat_uchar4( __global uchar4 *src, __global long4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long4_sat( src[i] );
}
