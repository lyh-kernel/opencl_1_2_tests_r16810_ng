__kernel void test_convert_short4_rtp_ushort4( __global ushort4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4_rtp( src[i] );
}
