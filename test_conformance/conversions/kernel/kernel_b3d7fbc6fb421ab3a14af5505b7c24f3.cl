__kernel void test_convert_int16_sat_rtp_long16( __global long16 *src, __global int16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int16_sat_rtp( src[i] );
}
