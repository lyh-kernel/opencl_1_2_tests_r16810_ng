__kernel void test_implicit_ushort_ulong( __global ulong *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
