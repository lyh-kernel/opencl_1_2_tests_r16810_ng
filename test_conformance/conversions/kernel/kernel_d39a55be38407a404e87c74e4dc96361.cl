__kernel void test_convert_char16_rtz_char16( __global char16 *src, __global char16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char16_rtz( src[i] );
}
