__kernel void test_convert_uchar_sat_rtn_ulong( __global ulong *src, __global uchar *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar_sat_rtn( src[i] );
}
