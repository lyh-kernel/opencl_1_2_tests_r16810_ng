__kernel void test_convert_uchar_rte_uint( __global uint *src, __global uchar *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar_rte( src[i] );
}
