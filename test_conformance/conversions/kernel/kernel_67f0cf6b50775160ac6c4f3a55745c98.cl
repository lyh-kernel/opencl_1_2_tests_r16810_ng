__kernel void test_convert_char_short( __global short *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char( src[i] );
}
