__kernel void test_convert_float16_rtp_char16( __global char16 *src, __global float16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float16_rtp( src[i] );
}
