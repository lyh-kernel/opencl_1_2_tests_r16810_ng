__kernel void test_convert_short2_sat_rte_char2( __global char2 *src, __global short2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short2_sat_rte( src[i] );
}
