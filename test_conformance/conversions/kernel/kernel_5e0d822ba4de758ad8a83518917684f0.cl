__kernel void test_convert_ushort_sat_rte_char( __global char *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_sat_rte( src[i] );
}
