__kernel void test_convert_uint4_sat_rtn_uint4( __global uint4 *src, __global uint4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint4_sat_rtn( src[i] );
}
