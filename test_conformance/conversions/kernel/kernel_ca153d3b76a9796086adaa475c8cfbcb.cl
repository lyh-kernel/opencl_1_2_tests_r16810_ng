__kernel void test_convert_int3_float3( __global float *src, __global int *dest )
{
   size_t i = get_global_id(0);
   if( i + 1 < get_global_size(0))
       vstore3( convert_int3( vload3( i, src)), i, dest );
   else
   {
       float3 in;
       int3 out;
       if( 0 == (i & 1) )
           in.y = src[3*i+1];
       in.x = src[3*i];
       out = convert_int3( in ); 
       dest[3*i] = out.x;
       if( 0 == (i & 1) )
           dest[3*i+1] = out.y;
   }
}
