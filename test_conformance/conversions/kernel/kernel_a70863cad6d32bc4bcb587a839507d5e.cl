__kernel void test_convert_short4_int4( __global int4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4( src[i] );
}
