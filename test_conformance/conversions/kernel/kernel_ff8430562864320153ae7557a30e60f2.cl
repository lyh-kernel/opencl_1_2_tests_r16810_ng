__kernel void test_convert_ulong16_rtp_float16( __global float16 *src, __global ulong16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong16_rtp( src[i] );
}
