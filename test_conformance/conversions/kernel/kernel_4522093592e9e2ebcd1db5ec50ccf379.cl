__kernel void test_convert_float8_rte_uint8( __global uint8 *src, __global float8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float8_rte( src[i] );
}
