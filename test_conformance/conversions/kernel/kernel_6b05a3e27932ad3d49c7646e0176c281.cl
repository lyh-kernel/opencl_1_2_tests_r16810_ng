__kernel void test_convert_uint_sat_float( __global float *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint_sat( src[i] );
}
