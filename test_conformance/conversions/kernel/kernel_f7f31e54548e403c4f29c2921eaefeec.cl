__kernel void test_convert_int2_rtp_long2( __global long2 *src, __global int2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int2_rtp( src[i] );
}
