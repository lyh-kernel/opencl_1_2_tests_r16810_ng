__kernel void test_convert_ulong8_sat_rtn_short8( __global short8 *src, __global ulong8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong8_sat_rtn( src[i] );
}
