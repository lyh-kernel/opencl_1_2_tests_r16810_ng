__kernel void test_convert_int_float( __global float *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int( src[i] );
}
