__kernel void test_convert_ulong2_sat_rtn_short2( __global short2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_sat_rtn( src[i] );
}
