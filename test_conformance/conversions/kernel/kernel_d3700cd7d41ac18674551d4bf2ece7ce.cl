__kernel void test_convert_ushort4_rtp_int4( __global int4 *src, __global ushort4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort4_rtp( src[i] );
}
