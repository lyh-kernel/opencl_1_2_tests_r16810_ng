__kernel void test_convert_uchar8_sat_rtz_int8( __global int8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_sat_rtz( src[i] );
}
