__kernel void test_convert_char_sat_int( __global int *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char_sat( src[i] );
}
