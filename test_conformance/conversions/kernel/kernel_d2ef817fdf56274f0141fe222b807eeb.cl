__kernel void test_convert_short_sat_rtp_char( __global char *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_sat_rtp( src[i] );
}
