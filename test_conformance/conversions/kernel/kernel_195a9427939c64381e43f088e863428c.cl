__kernel void test_convert_int16_sat_float16( __global float16 *src, __global int16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int16_sat( src[i] );
}
