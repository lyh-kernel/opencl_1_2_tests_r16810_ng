__kernel void test_convert_ulong_rtz_int( __global int *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong_rtz( src[i] );
}
