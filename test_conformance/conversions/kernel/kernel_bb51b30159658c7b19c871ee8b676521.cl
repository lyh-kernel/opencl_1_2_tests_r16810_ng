__kernel void test_convert_uchar2_sat_rtn_float2( __global float2 *src, __global uchar2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar2_sat_rtn( src[i] );
}
