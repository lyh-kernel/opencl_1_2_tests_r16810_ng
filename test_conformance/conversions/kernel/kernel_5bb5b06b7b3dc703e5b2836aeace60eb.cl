__kernel void test_convert_uint4_sat_rte_char4( __global char4 *src, __global uint4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint4_sat_rte( src[i] );
}
