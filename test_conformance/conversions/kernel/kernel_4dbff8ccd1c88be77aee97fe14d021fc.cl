__kernel void test_convert_float4_rtn_ushort4( __global ushort4 *src, __global float4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float4_rtn( src[i] );
}
