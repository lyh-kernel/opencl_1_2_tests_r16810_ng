__kernel void test_convert_float8_rte_short8( __global short8 *src, __global float8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float8_rte( src[i] );
}
