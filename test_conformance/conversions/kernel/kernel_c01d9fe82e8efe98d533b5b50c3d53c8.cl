__kernel void test_convert_short16_rtz_float16( __global float16 *src, __global short16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short16_rtz( src[i] );
}
