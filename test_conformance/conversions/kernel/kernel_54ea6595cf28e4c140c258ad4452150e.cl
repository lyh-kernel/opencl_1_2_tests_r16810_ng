__kernel void test_convert_short16_sat_rtp_uchar16( __global uchar16 *src, __global short16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short16_sat_rtp( src[i] );
}
