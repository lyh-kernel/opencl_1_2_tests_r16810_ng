__kernel void test_convert_int_rte_char( __global char *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int_rte( src[i] );
}
