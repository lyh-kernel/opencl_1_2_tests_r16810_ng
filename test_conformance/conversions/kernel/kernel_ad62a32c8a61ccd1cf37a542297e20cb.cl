__kernel void test_convert_long8_rtp_long8( __global long8 *src, __global long8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long8_rtp( src[i] );
}
