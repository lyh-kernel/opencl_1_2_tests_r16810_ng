__kernel void test_convert_int4_sat_short4( __global short4 *src, __global int4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int4_sat( src[i] );
}
