__kernel void test_convert_uchar16_sat_rte_char16( __global char16 *src, __global uchar16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar16_sat_rte( src[i] );
}
