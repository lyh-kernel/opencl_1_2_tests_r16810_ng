__kernel void test_implicit_int_long( __global long *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
