__kernel void test_convert_ulong2_rtz_int2( __global int2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_rtz( src[i] );
}
