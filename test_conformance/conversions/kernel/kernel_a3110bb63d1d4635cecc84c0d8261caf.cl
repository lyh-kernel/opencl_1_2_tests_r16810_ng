__kernel void test_convert_long16_rtz_char16( __global char16 *src, __global long16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long16_rtz( src[i] );
}
