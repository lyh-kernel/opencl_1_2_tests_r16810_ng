__kernel void test_convert_uchar_sat_rtz_float( __global float *src, __global uchar *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar_sat_rtz( src[i] );
}
