__kernel void test_convert_int4_rtp_ulong4( __global ulong4 *src, __global int4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int4_rtp( src[i] );
}
