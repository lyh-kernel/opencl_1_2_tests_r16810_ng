__kernel void test_convert_uchar_sat_rtp_int( __global int *src, __global uchar *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar_sat_rtp( src[i] );
}
