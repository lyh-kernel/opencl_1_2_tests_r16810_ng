__kernel void test_convert_char8_rtp_float8( __global float8 *src, __global char8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char8_rtp( src[i] );
}
