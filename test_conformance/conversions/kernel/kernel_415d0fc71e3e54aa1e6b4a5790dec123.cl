__kernel void test_convert_ushort16_sat_rtp_uint16( __global uint16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16_sat_rtp( src[i] );
}
