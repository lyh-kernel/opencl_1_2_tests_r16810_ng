__kernel void test_convert_long_uint( __global uint *src, __global long *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long( src[i] );
}
