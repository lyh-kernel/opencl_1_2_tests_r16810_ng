__kernel void test_convert_int4_rtn_uint4( __global uint4 *src, __global int4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int4_rtn( src[i] );
}
