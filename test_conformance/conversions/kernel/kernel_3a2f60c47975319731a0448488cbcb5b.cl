__kernel void test_convert_short8_rtp_ulong8( __global ulong8 *src, __global short8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short8_rtp( src[i] );
}
