__kernel void test_convert_ulong2_sat_rtp_uint2( __global uint2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_sat_rtp( src[i] );
}
