__kernel void test_convert_long16_rtp_short16( __global short16 *src, __global long16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long16_rtp( src[i] );
}
