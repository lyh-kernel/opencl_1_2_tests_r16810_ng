__kernel void test_convert_short_rtp_uint( __global uint *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_rtp( src[i] );
}
