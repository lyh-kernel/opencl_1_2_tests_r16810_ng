__kernel void test_convert_ushort8_sat_int8( __global int8 *src, __global ushort8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort8_sat( src[i] );
}
