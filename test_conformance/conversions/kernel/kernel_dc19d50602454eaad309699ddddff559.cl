__kernel void test_convert_long2_int2( __global int2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2( src[i] );
}
