__kernel void test_convert_ushort16_sat_float16( __global float16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16_sat( src[i] );
}
