__kernel void test_convert_long4_sat_long4( __global long4 *src, __global long4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long4_sat( src[i] );
}
