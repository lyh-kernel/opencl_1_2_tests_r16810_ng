__kernel void test_convert_long2_sat_rtn_ushort2( __global ushort2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2_sat_rtn( src[i] );
}
