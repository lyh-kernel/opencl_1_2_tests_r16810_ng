__kernel void test_implicit_float_ushort( __global ushort *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
