__kernel void test_convert_char4_sat_rtn_char4( __global char4 *src, __global char4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char4_sat_rtn( src[i] );
}
