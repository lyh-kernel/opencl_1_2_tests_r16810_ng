__kernel void test_convert_ulong_rte_char( __global char *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong_rte( src[i] );
}
