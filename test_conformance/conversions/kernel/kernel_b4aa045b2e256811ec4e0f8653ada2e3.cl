__kernel void test_convert_char16_sat_rte_ulong16( __global ulong16 *src, __global char16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char16_sat_rte( src[i] );
}
