__kernel void test_convert_ulong8_rtp_uchar8( __global uchar8 *src, __global ulong8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong8_rtp( src[i] );
}
