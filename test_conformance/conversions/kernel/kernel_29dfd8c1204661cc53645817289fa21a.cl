__kernel void test_convert_uchar8_sat_rtn_uint8( __global uint8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_sat_rtn( src[i] );
}
