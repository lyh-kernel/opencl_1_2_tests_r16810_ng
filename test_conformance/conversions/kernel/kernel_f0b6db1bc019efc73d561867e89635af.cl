__kernel void test_convert_uint2_sat_rtp_float2( __global float2 *src, __global uint2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint2_sat_rtp( src[i] );
}
