__kernel void test_convert_ushort2_rtn_short2( __global short2 *src, __global ushort2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort2_rtn( src[i] );
}
