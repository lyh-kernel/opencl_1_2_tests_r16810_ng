__kernel void test_convert_ulong4_sat_rtn_uint4( __global uint4 *src, __global ulong4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong4_sat_rtn( src[i] );
}
