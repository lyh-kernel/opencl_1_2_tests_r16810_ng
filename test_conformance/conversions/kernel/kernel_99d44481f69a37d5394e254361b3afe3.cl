__kernel void test_convert_short_rte_float( __global float *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_rte( src[i] );
}
