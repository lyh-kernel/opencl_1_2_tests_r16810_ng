__kernel void test_convert_ushort4_rtn_float4( __global float4 *src, __global ushort4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort4_rtn( src[i] );
}
