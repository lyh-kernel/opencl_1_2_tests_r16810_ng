__kernel void test_convert_uint_sat_rtn_int( __global int *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint_sat_rtn( src[i] );
}
