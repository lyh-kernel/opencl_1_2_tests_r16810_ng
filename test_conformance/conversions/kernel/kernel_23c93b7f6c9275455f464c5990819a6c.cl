__kernel void test_convert_uchar8_rte_char8( __global char8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_rte( src[i] );
}
