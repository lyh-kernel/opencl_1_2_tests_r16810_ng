__kernel void test_convert_uint16_rtp_int16( __global int16 *src, __global uint16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint16_rtp( src[i] );
}
