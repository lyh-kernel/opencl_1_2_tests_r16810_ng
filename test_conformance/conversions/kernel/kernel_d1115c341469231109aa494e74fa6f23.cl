__kernel void test_convert_char16_rte_uchar16( __global uchar16 *src, __global char16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char16_rte( src[i] );
}
