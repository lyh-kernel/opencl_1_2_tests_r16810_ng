__kernel void test_convert_long16_rte_float16( __global float16 *src, __global long16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long16_rte( src[i] );
}
