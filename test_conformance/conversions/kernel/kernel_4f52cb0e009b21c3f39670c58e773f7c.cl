__kernel void test_convert_ulong_float( __global float *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong( src[i] );
}
