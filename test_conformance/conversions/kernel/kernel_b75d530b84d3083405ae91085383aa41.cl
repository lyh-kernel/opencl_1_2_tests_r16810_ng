__kernel void test_convert_ushort8_rtp_float8( __global float8 *src, __global ushort8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort8_rtp( src[i] );
}
