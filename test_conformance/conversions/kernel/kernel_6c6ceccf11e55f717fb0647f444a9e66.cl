__kernel void test_convert_char8_sat_rtp_short8( __global short8 *src, __global char8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char8_sat_rtp( src[i] );
}
