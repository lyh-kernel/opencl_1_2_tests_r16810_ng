__kernel void test_implicit_char_float( __global float *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
