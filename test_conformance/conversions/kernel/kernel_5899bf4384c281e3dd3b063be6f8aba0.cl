__kernel void test_convert_char_sat_rtz_uint( __global uint *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char_sat_rtz( src[i] );
}
