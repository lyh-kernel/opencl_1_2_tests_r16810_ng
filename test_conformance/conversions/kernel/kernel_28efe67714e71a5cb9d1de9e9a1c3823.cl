__kernel void test_convert_uchar2_rtn_float2( __global float2 *src, __global uchar2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar2_rtn( src[i] );
}
