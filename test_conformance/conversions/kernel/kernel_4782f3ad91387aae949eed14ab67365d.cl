__kernel void test_convert_char_rtn_uchar( __global uchar *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char_rtn( src[i] );
}
