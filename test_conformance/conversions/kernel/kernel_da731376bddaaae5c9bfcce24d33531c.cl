__kernel void test_convert_char4_rtn_float4( __global float4 *src, __global char4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char4_rtn( src[i] );
}
