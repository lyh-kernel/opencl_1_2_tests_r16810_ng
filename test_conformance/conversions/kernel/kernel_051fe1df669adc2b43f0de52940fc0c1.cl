__kernel void test_convert_uint8_sat_ushort8( __global ushort8 *src, __global uint8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint8_sat( src[i] );
}
