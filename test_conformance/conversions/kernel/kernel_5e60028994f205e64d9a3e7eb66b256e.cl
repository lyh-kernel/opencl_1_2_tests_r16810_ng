__kernel void test_convert_float2_rtz_float2( __global float2 *src, __global float2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float2_rtz( src[i] );
}
