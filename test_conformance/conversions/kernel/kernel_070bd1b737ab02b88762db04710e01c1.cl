__kernel void test_convert_uint4_long4( __global long4 *src, __global uint4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint4( src[i] );
}
