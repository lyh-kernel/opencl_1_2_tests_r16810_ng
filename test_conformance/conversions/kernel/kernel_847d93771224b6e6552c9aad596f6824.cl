__kernel void test_implicit_short_float( __global float *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
