__kernel void test_convert_float16_rtn_short16( __global short16 *src, __global float16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float16_rtn( src[i] );
}
