__kernel void test_convert_long_sat_float( __global float *src, __global long *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long_sat( src[i] );
}
