__kernel void test_convert_ushort_sat_ushort( __global ushort *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_sat( src[i] );
}
