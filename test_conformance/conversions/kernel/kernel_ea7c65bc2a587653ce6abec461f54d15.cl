__kernel void test_convert_short_rtp_ulong( __global ulong *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_rtp( src[i] );
}
