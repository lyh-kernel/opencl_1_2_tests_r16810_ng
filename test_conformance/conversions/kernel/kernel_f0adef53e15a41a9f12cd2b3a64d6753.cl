__kernel void test_convert_ulong4_rtn_int4( __global int4 *src, __global ulong4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong4_rtn( src[i] );
}
