__kernel void test_convert_long2_rtn_char2( __global char2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2_rtn( src[i] );
}
