__kernel void test_convert_uint8_rtz_ulong8( __global ulong8 *src, __global uint8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint8_rtz( src[i] );
}
