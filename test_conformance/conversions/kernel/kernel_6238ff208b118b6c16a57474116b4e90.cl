__kernel void test_convert_long8_rtz_ulong8( __global ulong8 *src, __global long8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long8_rtz( src[i] );
}
