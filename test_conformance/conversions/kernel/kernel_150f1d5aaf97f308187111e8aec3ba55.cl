__kernel void test_implicit_float_char( __global char *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
