__kernel void test_implicit_char_int( __global int *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
