__kernel void test_convert_uint_sat_rtn_short( __global short *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint_sat_rtn( src[i] );
}
