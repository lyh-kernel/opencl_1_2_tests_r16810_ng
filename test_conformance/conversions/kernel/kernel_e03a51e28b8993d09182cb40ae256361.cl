__kernel void test_convert_ushort16_sat_rtp_char16( __global char16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16_sat_rtp( src[i] );
}
