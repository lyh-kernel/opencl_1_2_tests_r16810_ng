__kernel void test_convert_ulong2_sat_rtz_float2( __global float2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_sat_rtz( src[i] );
}
