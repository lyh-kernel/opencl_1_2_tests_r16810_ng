__kernel void test_convert_short4_sat_rtz_uint4( __global uint4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4_sat_rtz( src[i] );
}
