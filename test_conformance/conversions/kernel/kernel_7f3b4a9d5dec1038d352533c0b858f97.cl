__kernel void test_convert_ushort2_sat_long2( __global long2 *src, __global ushort2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort2_sat( src[i] );
}
