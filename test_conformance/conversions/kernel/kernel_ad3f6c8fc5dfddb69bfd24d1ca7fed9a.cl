__kernel void test_convert_long16_sat_rtp_uint16( __global uint16 *src, __global long16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long16_sat_rtp( src[i] );
}
