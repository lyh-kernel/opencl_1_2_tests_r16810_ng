__kernel void test_convert_uint4_rtz_short4( __global short4 *src, __global uint4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint4_rtz( src[i] );
}
