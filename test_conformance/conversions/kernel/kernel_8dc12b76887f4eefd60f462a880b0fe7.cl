__kernel void test_convert_ushort16_rte_long16( __global long16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16_rte( src[i] );
}
