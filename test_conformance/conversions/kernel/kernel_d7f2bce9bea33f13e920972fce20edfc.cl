__kernel void test_implicit_short_char( __global char *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
