__kernel void test_convert_uchar4_sat_rtn_int4( __global int4 *src, __global uchar4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar4_sat_rtn( src[i] );
}
