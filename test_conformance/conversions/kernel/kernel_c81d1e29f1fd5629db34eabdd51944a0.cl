__kernel void test_convert_ushort16_sat_rtn_ushort16( __global ushort16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16_sat_rtn( src[i] );
}
