__kernel void test_convert_uint_char( __global char *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint( src[i] );
}
