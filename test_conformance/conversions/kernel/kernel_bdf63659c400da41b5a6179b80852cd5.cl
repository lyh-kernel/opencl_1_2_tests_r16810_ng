__kernel void test_implicit_ulong_short( __global short *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
