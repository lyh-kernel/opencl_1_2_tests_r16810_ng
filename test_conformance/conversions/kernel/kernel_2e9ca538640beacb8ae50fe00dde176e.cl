__kernel void test_convert_char2_rtn_ushort2( __global ushort2 *src, __global char2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char2_rtn( src[i] );
}
