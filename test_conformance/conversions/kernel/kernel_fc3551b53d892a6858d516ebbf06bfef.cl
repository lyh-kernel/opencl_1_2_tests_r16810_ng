__kernel void test_convert_long8_sat_rtp_uint8( __global uint8 *src, __global long8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long8_sat_rtp( src[i] );
}
