__kernel void test_convert_float_rte_uchar( __global uchar *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float_rte( src[i] );
}
