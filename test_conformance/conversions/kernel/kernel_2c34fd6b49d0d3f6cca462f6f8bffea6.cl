__kernel void test_convert_long8_sat_rtn_char8( __global char8 *src, __global long8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long8_sat_rtn( src[i] );
}
