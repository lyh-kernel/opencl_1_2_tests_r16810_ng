__kernel void test_convert_uint_rtp_ulong( __global ulong *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint_rtp( src[i] );
}
