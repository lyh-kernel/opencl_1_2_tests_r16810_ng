__kernel void test_implicit_char_ushort( __global ushort *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
