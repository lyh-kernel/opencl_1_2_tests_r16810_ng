__kernel void test_convert_short_rtp_long( __global long *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_rtp( src[i] );
}
