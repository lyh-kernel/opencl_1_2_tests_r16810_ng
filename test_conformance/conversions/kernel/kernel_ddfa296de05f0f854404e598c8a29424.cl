__kernel void test_convert_char4_rte_short4( __global short4 *src, __global char4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char4_rte( src[i] );
}
