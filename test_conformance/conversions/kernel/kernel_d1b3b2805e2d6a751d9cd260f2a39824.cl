__kernel void test_convert_float8_rtz_long8( __global long8 *src, __global float8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float8_rtz( src[i] );
}
