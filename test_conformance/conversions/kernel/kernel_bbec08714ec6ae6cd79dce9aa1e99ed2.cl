__kernel void test_implicit_ulong_ushort( __global ushort *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
