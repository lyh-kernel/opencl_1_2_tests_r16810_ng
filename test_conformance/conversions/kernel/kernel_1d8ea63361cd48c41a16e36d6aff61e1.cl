__kernel void test_convert_char2_sat_rtp_float2( __global float2 *src, __global char2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char2_sat_rtp( src[i] );
}
