__kernel void test_convert_uint16_rtn_char16( __global char16 *src, __global uint16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint16_rtn( src[i] );
}
