__kernel void test_convert_float_rtz_long( __global long *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float_rtz( src[i] );
}
