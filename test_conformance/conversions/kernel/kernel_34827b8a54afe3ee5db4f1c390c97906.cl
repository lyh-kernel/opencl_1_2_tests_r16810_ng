__kernel void test_convert_ulong16_sat_rtn_ulong16( __global ulong16 *src, __global ulong16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong16_sat_rtn( src[i] );
}
