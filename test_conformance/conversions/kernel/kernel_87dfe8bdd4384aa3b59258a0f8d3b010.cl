__kernel void test_convert_uchar8_rtz_float8( __global float8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_rtz( src[i] );
}
