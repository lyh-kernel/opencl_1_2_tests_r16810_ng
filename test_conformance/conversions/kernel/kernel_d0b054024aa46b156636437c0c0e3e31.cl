__kernel void test_implicit_int_ulong( __global ulong *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
