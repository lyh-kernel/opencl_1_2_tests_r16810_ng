__kernel void test_convert_uchar8_sat_rtn_ushort8( __global ushort8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_sat_rtn( src[i] );
}
