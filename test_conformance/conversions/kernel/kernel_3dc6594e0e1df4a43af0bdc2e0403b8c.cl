__kernel void test_convert_ulong2_rte_char2( __global char2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_rte( src[i] );
}
