__kernel void test_convert_uint_sat_rtz_long( __global long *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint_sat_rtz( src[i] );
}
