__kernel void test_convert_char16_rtn_short16( __global short16 *src, __global char16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char16_rtn( src[i] );
}
