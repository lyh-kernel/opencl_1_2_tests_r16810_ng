__kernel void test_convert_ulong8_sat_rtp_float8( __global float8 *src, __global ulong8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong8_sat_rtp( src[i] );
}
