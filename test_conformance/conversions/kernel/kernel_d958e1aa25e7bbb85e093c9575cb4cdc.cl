__kernel void test_convert_ushort2_sat_rtp_int2( __global int2 *src, __global ushort2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort2_sat_rtp( src[i] );
}
