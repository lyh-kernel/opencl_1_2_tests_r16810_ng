__kernel void test_convert_long2_sat_rtz_float2( __global float2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2_sat_rtz( src[i] );
}
