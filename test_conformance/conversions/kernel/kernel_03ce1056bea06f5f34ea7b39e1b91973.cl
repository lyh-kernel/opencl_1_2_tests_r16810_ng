__kernel void test_convert_int8_rtp_int8( __global int8 *src, __global int8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int8_rtp( src[i] );
}
