__kernel void test_convert_short8_rte_uchar8( __global uchar8 *src, __global short8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short8_rte( src[i] );
}
