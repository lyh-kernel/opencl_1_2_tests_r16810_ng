__kernel void test_convert_int2_uchar2( __global uchar2 *src, __global int2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int2( src[i] );
}
