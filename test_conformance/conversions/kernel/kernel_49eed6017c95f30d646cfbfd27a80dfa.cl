__kernel void test_convert_ushort16_ulong16( __global ulong16 *src, __global ushort16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort16( src[i] );
}
