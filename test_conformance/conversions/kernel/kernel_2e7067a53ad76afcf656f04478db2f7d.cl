__kernel void test_convert_ulong_uint( __global uint *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong( src[i] );
}
