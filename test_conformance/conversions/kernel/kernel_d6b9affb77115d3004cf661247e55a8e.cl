__kernel void test_convert_char_sat_rtp_float( __global float *src, __global char *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char_sat_rtp( src[i] );
}
