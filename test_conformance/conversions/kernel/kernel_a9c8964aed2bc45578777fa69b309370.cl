__kernel void test_convert_long16_ulong16( __global ulong16 *src, __global long16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long16( src[i] );
}
