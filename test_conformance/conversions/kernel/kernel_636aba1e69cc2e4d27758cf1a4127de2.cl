__kernel void test_convert_ulong8_rtz_ushort8( __global ushort8 *src, __global ulong8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong8_rtz( src[i] );
}
