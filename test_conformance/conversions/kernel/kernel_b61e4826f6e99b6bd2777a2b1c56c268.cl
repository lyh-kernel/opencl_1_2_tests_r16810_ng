__kernel void test_convert_ushort8_sat_rte_char8( __global char8 *src, __global ushort8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort8_sat_rte( src[i] );
}
