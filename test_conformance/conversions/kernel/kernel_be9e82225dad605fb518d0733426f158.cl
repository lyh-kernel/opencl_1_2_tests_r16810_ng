__kernel void test_convert_short2_sat_rtz_short2( __global short2 *src, __global short2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short2_sat_rtz( src[i] );
}
