__kernel void test_convert_long8_sat_uchar8( __global uchar8 *src, __global long8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long8_sat( src[i] );
}
