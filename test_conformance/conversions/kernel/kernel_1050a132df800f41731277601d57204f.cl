__kernel void test_convert_int_sat_rtz_ushort( __global ushort *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int_sat_rtz( src[i] );
}
