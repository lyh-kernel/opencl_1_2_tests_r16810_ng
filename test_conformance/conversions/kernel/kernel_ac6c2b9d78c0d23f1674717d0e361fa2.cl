__kernel void test_convert_long_sat_rte_uchar( __global uchar *src, __global long *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long_sat_rte( src[i] );
}
