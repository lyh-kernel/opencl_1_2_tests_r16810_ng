__kernel void test_convert_ulong16_sat_rte_ushort16( __global ushort16 *src, __global ulong16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong16_sat_rte( src[i] );
}
