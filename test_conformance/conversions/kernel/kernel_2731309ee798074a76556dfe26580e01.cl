__kernel void test_convert_uint16_sat_rtz_uchar16( __global uchar16 *src, __global uint16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint16_sat_rtz( src[i] );
}
