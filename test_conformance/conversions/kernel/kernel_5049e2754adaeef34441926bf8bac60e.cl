__kernel void test_convert_float2_char2( __global char2 *src, __global float2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float2( src[i] );
}
