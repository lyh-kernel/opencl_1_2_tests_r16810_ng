__kernel void test_implicit_uchar_ulong( __global ulong *src, __global uchar *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
