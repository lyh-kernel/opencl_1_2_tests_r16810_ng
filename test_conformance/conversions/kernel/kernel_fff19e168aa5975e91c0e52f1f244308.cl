__kernel void test_convert_short16_rtp_ushort16( __global ushort16 *src, __global short16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short16_rtp( src[i] );
}
