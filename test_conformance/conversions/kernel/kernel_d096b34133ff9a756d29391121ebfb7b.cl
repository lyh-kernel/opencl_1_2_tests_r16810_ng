__kernel void test_convert_short_sat_short( __global short *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_sat( src[i] );
}
