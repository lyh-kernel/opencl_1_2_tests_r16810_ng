__kernel void test_convert_short4_ulong4( __global ulong4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4( src[i] );
}
