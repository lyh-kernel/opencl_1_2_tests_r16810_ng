__kernel void test_convert_uint4_sat_rte_float4( __global float4 *src, __global uint4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint4_sat_rte( src[i] );
}
