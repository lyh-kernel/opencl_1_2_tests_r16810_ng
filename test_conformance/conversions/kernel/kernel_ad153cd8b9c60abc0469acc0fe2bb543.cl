__kernel void test_convert_short_rte_uchar( __global uchar *src, __global short *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short_rte( src[i] );
}
