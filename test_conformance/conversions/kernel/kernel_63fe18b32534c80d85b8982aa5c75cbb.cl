__kernel void test_convert_int2_sat_ushort2( __global ushort2 *src, __global int2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int2_sat( src[i] );
}
