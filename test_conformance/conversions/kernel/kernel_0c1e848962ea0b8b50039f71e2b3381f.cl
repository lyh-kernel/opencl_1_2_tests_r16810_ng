__kernel void test_convert_float16_rtn_float16( __global float16 *src, __global float16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float16_rtn( src[i] );
}
