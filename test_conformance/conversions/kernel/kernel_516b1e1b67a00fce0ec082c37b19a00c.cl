__kernel void test_implicit_ulong_int( __global int *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
