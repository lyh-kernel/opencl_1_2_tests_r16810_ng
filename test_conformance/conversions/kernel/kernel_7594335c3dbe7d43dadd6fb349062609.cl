__kernel void test_convert_long2_sat_rtp_uchar2( __global uchar2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2_sat_rtp( src[i] );
}
