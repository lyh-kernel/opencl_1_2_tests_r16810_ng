__kernel void test_convert_uint2_sat_rtn_uchar2( __global uchar2 *src, __global uint2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint2_sat_rtn( src[i] );
}
