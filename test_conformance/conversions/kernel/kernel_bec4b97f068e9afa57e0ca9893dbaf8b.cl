__kernel void test_convert_ulong_sat_rtp_short( __global short *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong_sat_rtp( src[i] );
}
