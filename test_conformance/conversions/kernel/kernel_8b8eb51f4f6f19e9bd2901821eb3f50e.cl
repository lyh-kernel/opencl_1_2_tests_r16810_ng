__kernel void test_convert_char16_rtp_uint16( __global uint16 *src, __global char16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char16_rtp( src[i] );
}
