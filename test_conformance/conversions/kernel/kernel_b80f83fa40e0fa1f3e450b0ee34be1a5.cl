__kernel void test_implicit_long_short( __global short *src, __global long *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
