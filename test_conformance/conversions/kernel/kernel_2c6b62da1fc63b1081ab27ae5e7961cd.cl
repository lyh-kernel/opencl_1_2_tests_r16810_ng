__kernel void test_convert_long2_sat_rte_char2( __global char2 *src, __global long2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long2_sat_rte( src[i] );
}
