__kernel void test_convert_ushort_sat_uint( __global uint *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_sat( src[i] );
}
