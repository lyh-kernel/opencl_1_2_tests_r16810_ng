__kernel void test_convert_long4_ulong4( __global ulong4 *src, __global long4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long4( src[i] );
}
