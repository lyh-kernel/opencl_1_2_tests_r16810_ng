__kernel void test_convert_short4_rtn_uchar4( __global uchar4 *src, __global short4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short4_rtn( src[i] );
}
