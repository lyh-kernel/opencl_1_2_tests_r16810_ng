__kernel void test_convert_uint2_rtz_char2( __global char2 *src, __global uint2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint2_rtz( src[i] );
}
