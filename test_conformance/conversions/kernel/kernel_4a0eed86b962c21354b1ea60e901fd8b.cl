__kernel void test_implicit_uint_short( __global short *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
