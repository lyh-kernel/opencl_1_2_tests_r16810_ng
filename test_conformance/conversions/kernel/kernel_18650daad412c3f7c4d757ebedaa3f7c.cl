__kernel void test_convert_ushort_rte_ulong( __global ulong *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_rte( src[i] );
}
