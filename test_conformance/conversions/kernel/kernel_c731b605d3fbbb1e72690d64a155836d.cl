__kernel void test_convert_ulong2_sat_rte_long2( __global long2 *src, __global ulong2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong2_sat_rte( src[i] );
}
