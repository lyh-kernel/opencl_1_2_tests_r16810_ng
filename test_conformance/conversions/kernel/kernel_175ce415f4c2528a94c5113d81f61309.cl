__kernel void test_convert_float4_rtz_uint4( __global uint4 *src, __global float4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float4_rtz( src[i] );
}
