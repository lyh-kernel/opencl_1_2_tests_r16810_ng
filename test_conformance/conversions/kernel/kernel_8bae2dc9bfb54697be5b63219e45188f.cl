__kernel void test_convert_ushort4_sat_rtz_char4( __global char4 *src, __global ushort4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort4_sat_rtz( src[i] );
}
