__kernel void test_convert_uchar8_sat_rte_ulong8( __global ulong8 *src, __global uchar8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar8_sat_rte( src[i] );
}
