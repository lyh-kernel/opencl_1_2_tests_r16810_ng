__kernel void test_convert_uchar2_rtz_uchar2( __global uchar2 *src, __global uchar2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar2_rtz( src[i] );
}
