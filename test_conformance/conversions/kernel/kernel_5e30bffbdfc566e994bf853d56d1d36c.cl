__kernel void test_convert_short2_rtp_uchar2( __global uchar2 *src, __global short2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short2_rtp( src[i] );
}
