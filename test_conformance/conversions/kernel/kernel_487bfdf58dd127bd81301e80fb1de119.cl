__kernel void test_convert_float2_rtz_short2( __global short2 *src, __global float2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float2_rtz( src[i] );
}
