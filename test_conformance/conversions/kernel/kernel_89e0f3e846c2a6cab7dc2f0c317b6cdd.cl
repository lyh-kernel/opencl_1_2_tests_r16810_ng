__kernel void test_convert_char2_sat_rte_short2( __global short2 *src, __global char2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_char2_sat_rte( src[i] );
}
