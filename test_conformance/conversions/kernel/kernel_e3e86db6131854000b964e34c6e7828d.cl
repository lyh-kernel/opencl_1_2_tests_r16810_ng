__kernel void test_convert_short8_rtp_uint8( __global uint8 *src, __global short8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short8_rtp( src[i] );
}
