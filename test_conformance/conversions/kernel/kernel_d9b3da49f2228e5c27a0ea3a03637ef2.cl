__kernel void test_convert_int_sat_short( __global short *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int_sat( src[i] );
}
