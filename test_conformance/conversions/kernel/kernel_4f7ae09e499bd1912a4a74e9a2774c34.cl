__kernel void test_convert_float_rtn_char( __global char *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float_rtn( src[i] );
}
