__kernel void test_convert_uchar2_sat_rte_int2( __global int2 *src, __global uchar2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uchar2_sat_rte( src[i] );
}
