__kernel void test_convert_int4_rtz_ushort4( __global ushort4 *src, __global int4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int4_rtz( src[i] );
}
