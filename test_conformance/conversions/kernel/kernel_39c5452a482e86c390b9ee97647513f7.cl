__kernel void test_convert_ulong4_rtp_uchar4( __global uchar4 *src, __global ulong4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong4_rtp( src[i] );
}
