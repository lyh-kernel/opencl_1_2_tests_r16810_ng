__kernel void test_convert_uint16_sat_rte_ushort16( __global ushort16 *src, __global uint16 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint16_sat_rte( src[i] );
}
