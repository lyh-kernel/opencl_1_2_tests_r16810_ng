__kernel void test_convert_ulong3_uchar3( __global uchar *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   if( i + 1 < get_global_size(0))
       vstore3( convert_ulong3( vload3( i, src)), i, dest );
   else
   {
       uchar3 in;
       ulong3 out;
       if( 0 == (i & 1) )
           in.y = src[3*i+1];
       in.x = src[3*i];
       out = convert_ulong3( in ); 
       dest[3*i] = out.x;
       if( 0 == (i & 1) )
           dest[3*i+1] = out.y;
   }
}
