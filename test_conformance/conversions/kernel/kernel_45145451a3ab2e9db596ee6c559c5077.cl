__kernel void test_convert_short2_rtn_char2( __global char2 *src, __global short2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_short2_rtn( src[i] );
}
