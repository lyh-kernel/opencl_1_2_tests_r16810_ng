__kernel void test_convert_ulong_uchar( __global uchar *src, __global ulong *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ulong( src[i] );
}
