__kernel void test_convert_uint2_rtp_int2( __global int2 *src, __global uint2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint2_rtp( src[i] );
}
