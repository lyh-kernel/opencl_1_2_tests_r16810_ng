__kernel void test_convert_ushort_sat_rte_short( __global short *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_sat_rte( src[i] );
}
