__kernel void test_convert_long4_sat_rtp_float4( __global float4 *src, __global long4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long4_sat_rtp( src[i] );
}
