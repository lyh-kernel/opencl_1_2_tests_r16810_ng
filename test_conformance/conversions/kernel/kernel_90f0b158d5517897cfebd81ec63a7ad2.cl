__kernel void test_convert_ushort_rte_int( __global int *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_rte( src[i] );
}
