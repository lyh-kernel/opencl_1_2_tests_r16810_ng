__kernel void test_implicit_float_long( __global long *src, __global float *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
