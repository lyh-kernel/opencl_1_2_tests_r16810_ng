__kernel void test_convert_ushort_rtz_uchar( __global uchar *src, __global ushort *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort_rtz( src[i] );
}
