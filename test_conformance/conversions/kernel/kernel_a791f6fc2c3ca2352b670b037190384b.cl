__kernel void test_convert_float8_rte_uchar8( __global uchar8 *src, __global float8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float8_rte( src[i] );
}
