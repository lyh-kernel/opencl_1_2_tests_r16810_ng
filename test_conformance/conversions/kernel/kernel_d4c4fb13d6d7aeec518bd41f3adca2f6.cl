__kernel void test_convert_int_rtn_ulong( __global ulong *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_int_rtn( src[i] );
}
