__kernel void test_implicit_uint_ushort( __global ushort *src, __global uint *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
