__kernel void test_convert_long4_rte_uint4( __global uint4 *src, __global long4 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_long4_rte( src[i] );
}
