__kernel void test_convert_ushort8_sat_rtn_long8( __global long8 *src, __global ushort8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_ushort8_sat_rtn( src[i] );
}
