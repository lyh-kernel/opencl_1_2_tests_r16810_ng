__kernel void test_convert_float2_int2( __global int2 *src, __global float2 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_float2( src[i] );
}
