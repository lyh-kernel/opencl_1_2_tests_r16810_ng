__kernel void test_convert_uint8_sat_rtn_int8( __global int8 *src, __global uint8 *dest )
{
   size_t i = get_global_id(0);
   dest[i] = convert_uint8_sat_rtn( src[i] );
}
