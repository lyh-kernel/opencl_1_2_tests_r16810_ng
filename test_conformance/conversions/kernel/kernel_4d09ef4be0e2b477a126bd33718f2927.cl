__kernel void test_implicit_int_short( __global short *src, __global int *dest )
{
   size_t i = get_global_id(0);
   dest[i] =  src[i];
}
