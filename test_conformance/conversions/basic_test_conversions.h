/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#ifndef BASIC_TEST_CONVERSIONS_H
#define BASIC_TEST_CONVERSIONS_H

#if !defined(_WIN32)
#include <unistd.h>
#include <stdint.h>
#endif

#include "../../test_common/harness/compat.h"
#include "../../test_common/harness/errorHelpers.h"
#include "../../test_common/harness/rounding_mode.h"

#include <stdio.h>
#if defined( __APPLE__ )
    #include <OpenCL/opencl.h>
#else
    #include <CL/opencl.h>
#endif

#include "../../test_common/harness/mt19937.h"

typedef void (*Convert)( void *dest, void *src, size_t );

#define kVectorSizeCount    6
#define kMaxVectorSize      16

typedef enum
{
    kUnsaturated = 0,
    kSaturated,
    
    kSaturationModeCount
}SaturationMode;

extern Convert gConversions[kTypeCount][kTypeCount];                // [dest format][source format]
extern Convert gSaturatedConversions[kTypeCount][kTypeCount];       // [dest format][source format]
extern const char *gTypeNames[ kTypeCount ];
extern const char *gRoundingModeNames[ kRoundingModeCount ];        // { "", "_rte", "_rtp", "_rtn", "_rtz" }
extern const char *gSaturationNames[ kSaturationModeCount ];        // { "", "_sat" }
extern const char *gVectorSizeNames[kVectorSizeCount];              // { "", "2", "4", "8", "16" }
extern size_t gTypeSizes[ kTypeCount ];

//Functions for clamping floating point numbers into the representable range for the type
typedef float (*clampf)( float );
typedef double (*clampd)( double );

extern clampf gClampFloat[ kTypeCount ][kRoundingModeCount];
extern clampd gClampDouble[ kTypeCount ][kRoundingModeCount];

typedef void (*InitDataFunc)( void *dest, SaturationMode, RoundingMode, Type destType, uint64_t start, int count, MTdata d );
extern InitDataFunc gInitFunctions[ kTypeCount ];

typedef int (*CheckResults)( void *out1, void *out2, void *allowZ, uint32_t count, int vectorSize );
extern CheckResults gCheckResults[ kTypeCount ];

#endif /* BASIC_TEST_CONVERSIONS_H */

