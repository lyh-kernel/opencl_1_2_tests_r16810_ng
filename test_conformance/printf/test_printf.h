/****************************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2011 by Advanced Micro Devices, Inc. All Rights Reserved.
 //
 ****************************************************************************/

#ifndef TESTPRINTF_INCLUDED_H
#define TESTPRINTF_INCLUDED_H

#include <stdio.h>
#include <stdlib.h>

#if !defined(_WIN32)
#include <stdbool.h>
#include <stdint.h>
#endif

#include <string.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#include <OpenCL/cl_platform.h>
#else
#include <CL/opencl.h>
#include <CL/cl_platform.h>
#endif

// Enable the test to be used with ATF
#if USE_ATF
// export BUILD_WITH_ATF=1
#include <ATF/ATF.h>
#define test_start() ATFTestStart() 
#define log_info ATFLogInfo
#define log_error ATFLogError
#define test_finish() ATFTestFinish()
#else
#define test_start()
#define log_info printf
#define log_error printf
#define test_finish()
#endif // USE_ATF

#define ANALYSIS_BUFFER_SIZE 256

//-----------------------------------------
// Definitions and initializations
//-----------------------------------------

//-----------------------------------------
// Types 
//-----------------------------------------
enum Type  
 {  
	 INT,  
	 FLOAT,  
	 OCTAL,
	 UNSIGNED,
	 HEXADEC,
	 CHAR,
	 STRING,  
	 VECTOR,
	 ADDRESS_SPACE,
	 TYPE_COUNT
}; 

struct printDataGenParameters
{
	const char* genericFormat;
	const char* dataRepresentation;
	const char* vectorFormatFlag;
	const char* vectorFormatSpecifier;
	const char* dataType;
	const char* vectorSize;
	const char* addrSpaceArgumentTypeQualifier;
	const char* addrSpaceVariableTypeQualifier;
	const char* addrSpaceParameter;
	const char* addrSpacePAdd;
};

//-----------------------------------------
//Test Case
//-----------------------------------------

struct testCase
{
	unsigned int _testNum;                           //test number
	enum Type _type;                                 //(data)type for test
	//const char** _strPrint;                          //auxiliary data to build the code for kernel source 
	const char** _correctBuffer;                     //look-up table for correct results for printf
	struct printDataGenParameters* _genParameters;   //auxiliary data to build the code for kernel source 
};


extern const char* strType[];
extern testCase* allTestCase[];

size_t verifyOutputBuffer(char *analysisBuffer,testCase* pTestCase,size_t testId,cl_ulong pAddr = 0);

// Helpful macros

// The next three functions check on different return values.  Returns -1
// if the check failed
#define checkErr(err, msg)				\
    if (err != CL_SUCCESS) {				\
	log_error("%s failed errcode:%d\n", msg, err);	\
	return -1;					\
    }

#define checkZero(val, msg)				\
    if (val == 0) {					\
	log_error("%s failed errcode:%d\n", msg, err);	\
	return -1;					\
    }

#define checkNull(ptr, msg)			\
    if (!ptr) {					\
	log_error("%s failed\n", msg);		\
	return -1;				\
    }

// When a helper returns a negative one, we want to return from main
// with negative one. This helper prevents me from having to write
// this multiple time
#define checkHelperErr(err)			\
    if (err == -1) {				\
	return err;				\
    }

#endif // TESTSPRINTF_INCLUDED_H
