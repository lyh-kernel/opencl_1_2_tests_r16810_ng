kernel void test(global int *defines)
{
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#ifdef cl_khr_global_int32_base_atomics
  defines[0] = 1;
#else
  defines[0] = 0;
#endif
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : disable

#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#ifdef cl_khr_global_int32_extended_atomics
  defines[1] = 1;
#else
  defines[1] = 0;
#endif
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : disable

#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#ifdef cl_khr_local_int32_base_atomics
  defines[2] = 1;
#else
  defines[2] = 0;
#endif
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : disable

#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable
#ifdef cl_khr_local_int32_extended_atomics
  defines[3] = 1;
#else
  defines[3] = 0;
#endif
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : disable

#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#ifdef cl_khr_byte_addressable_store
  defines[4] = 1;
#else
  defines[4] = 0;
#endif
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : disable

}
