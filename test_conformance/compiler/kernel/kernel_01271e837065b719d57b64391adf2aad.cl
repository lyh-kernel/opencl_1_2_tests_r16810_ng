#include "simple_header0.h"
#include "foo/simple_header1.h"
__kernel void
CompositeKernel(
    __global float* src,
    __global float* dst )
{
    CopyBuffer0(src, dst);
    CopyBuffer1(src, dst);
}
