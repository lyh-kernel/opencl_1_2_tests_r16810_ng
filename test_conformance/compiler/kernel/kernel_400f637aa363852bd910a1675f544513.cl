#include "/home/jenkins/mxpa/conformance_test/conformance_1.2/pc/OpenCL_1_2_Tests_r16810_static/test_conformance/compiler/includeTestDirectory/testIncludeFile.h"
__kernel void include_test(__global int *src, __global int *dstA)
{
 int tid = get_global_id(0);
#ifdef HEADER_FOUND
 dstA[tid] = HEADER_FOUND;
#else
 dstA[tid] = 0;
#endif

}
