/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/

#include <stdio.h>
#include <stdlib.h>

#if !defined(_WIN32)
#include <stdbool.h>
#endif

#include <math.h>
#include <string.h>
#include "procs.h"
#include "../../test_common/harness/testHarness.h"

#if !defined(_WIN32)
#include <unistd.h>
#endif


basefn	basefn_list[] = {
            test_atomic_add,
			test_atomic_sub,
			test_atomic_xchg,
			test_atomic_min,
			test_atomic_max,
			test_atomic_inc,
			test_atomic_dec,
			test_atomic_cmpxchg,
			test_atomic_and,
			test_atomic_or,
			test_atomic_xor,

			test_atomic_add_index,
			test_atomic_add_index_bin
};

const char    *basefn_names[] = {
            "atomic_add",
			"atomic_sub",
			"atomic_xchg",
			"atomic_min",
			"atomic_max",
			"atomic_inc",
			"atomic_dec",
			"atomic_cmpxchg",
			"atomic_and",
			"atomic_or",
			"atomic_xor",
  
			"atomic_add_index",
			"atomic_add_index_bin",

			"all",
};

ct_assert((sizeof(basefn_names) / sizeof(basefn_names[0]) - 1) == (sizeof(basefn_list) / sizeof(basefn_list[0])));

int	num_fns = sizeof(basefn_names) / sizeof(char *);

int main(int argc, const char *argv[])
{
	return runTestHarness( argc, argv, num_fns, basefn_list, basefn_names, false, false, 0 );
}


