#########################################################################
# File Name: test.sh
# Author: Qin Chen
# mail: qin@multicorewareinc.com
# Created Time: Wed 29 Oct 2014 02:05:08 PM CDT
#########################################################################
#!/bin/bash

#step1 translate
for file in ./*.cl
do
  ${MXPA_ROOT_DIR}/scripts/mxpa.py $file
done

#step2 compile C output
for file in ./*.cl.c
do
  ${MXPA_BIN_DIR}/compiler/bin/clang -D_MXPA_QUERY_ -DREGISTER_KERNEL -include ${MXPA_ROOT_DIR}/runtime/BIFL/BIFL_X64/opencl_runtime.h -c $file -o $file.o 
done

#step3 compile bitcode
for file in ./*.mxpa.bc
do
  ${MXPA_BIN_DIR}/compiler/bin/clang -c $file -o $file.o
done

#step4 compile leonRegister.c
#cp ${MXPA_ROOT_DIR}/scripts/kernel_compile/*.c .
#${MXPA_BIN_DIR}/compiler/bin/clang -D_MXPA_QUERY_ -DREGISTER_KERNEL -std=c99 -fPIC -O3 -c -include-pch ${MXPA_BIN_DIR}/runtime/BIFL/BIFL_X64/opencl_runtime_release.h.pch -w leonRegister.c 


