
__kernel void test_fn( __global ushort *src, __global char2 *dst )
{
	int tid = get_global_id( 0 );
	char2 tmp = as_char2( src[ tid ] );
   dst[ tid ] = tmp;
}
