
__kernel void test_fn( __global ushort4 *srcValues, __global uint *offsets, __global ushort4 *destBuffer, uint alignmentOffset )
{
    __private ushort4 sPrivateStorage[ 16 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (ushort4)(ushort)0;

   vstore4( srcValues[ tid ], offsets[ tid ], ( (__private ushort *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private ushort *sp = (__private ushort*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global ushort *dp = (__global ushort*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
