
__kernel void test_fn( __global uint *src, __global int *dst )
{
	int tid = get_global_id( 0 );
	int tmp = as_int( src[ tid ] );
   dst[ tid ] = tmp;
}
