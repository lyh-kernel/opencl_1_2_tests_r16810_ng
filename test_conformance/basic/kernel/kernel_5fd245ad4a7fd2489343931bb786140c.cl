__kernel void test_fn( __constant ushort *src, __global uint *offsets, __global uint *alignmentOffsets, __global ushort *results )
{
    int tid = get_global_id( 0 );
    ushort3 tmp = vload3( offsets[ tid ], ( (__constant ushort *) src ) + alignmentOffsets[ tid ] );
   results[ 3*tid   ] = tmp.s0;
   results[ 3*tid+1 ] = tmp.s1;
   results[ 3*tid+2 ] = tmp.s2;
}
