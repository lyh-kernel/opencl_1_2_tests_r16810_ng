__kernel void test_conversion(__global ushort *sourceValues, __global ushort8 *destValues )
{
    int  tid = get_global_id(0);
    ushort  src = sourceValues[tid];

    destValues[tid] = (ushort8)src;

}
