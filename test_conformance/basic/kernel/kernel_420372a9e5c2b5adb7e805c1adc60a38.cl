
__kernel void test_fn( __global uint2 *srcValues, __global uint *offsets, __global uint2 *destBuffer, uint alignmentOffset )
{
    __private uint2 sPrivateStorage[ 16 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (uint2)(uint)0;

   vstore2( srcValues[ tid ], offsets[ tid ], ( (__private uint *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private uint *sp = (__private uint*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global uint *dp = (__global uint*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
