__kernel void test_odd_int16(__global int16 *srcA, __global int8 *dst)
{
    int  tid = get_global_id(0);

    int8 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
