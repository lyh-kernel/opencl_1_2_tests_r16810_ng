
__kernel void test_fn( const __global short *src, __global short *dst, __local short *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: shortshortshort
 int i;
 prefetch( (const __global short*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
