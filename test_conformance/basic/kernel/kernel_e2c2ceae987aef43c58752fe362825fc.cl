
__kernel void test_fn( const __global ushort8 *src, __global ushort8 *dst, __local ushort8 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ushort8ushortushort8
 int i;
 prefetch( (const __global ushort8*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
