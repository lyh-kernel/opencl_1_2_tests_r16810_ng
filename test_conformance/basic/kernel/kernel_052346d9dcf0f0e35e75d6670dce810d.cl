
__kernel void test_fn( __global long16 *srcValues, __global uint *offsets, __global long16 *destBuffer, uint alignmentOffset )
{
    __private long16 sPrivateStorage[ 2 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (long16)(long)0;

   vstore16( srcValues[ tid ], offsets[ tid ], ( (__private long *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private long *sp = (__private long*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global long *dp = (__global long*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
