
__kernel void test_fn( const __global int16 *src, __global int16 *dst, __local int16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: int16intint16
 int i;
 prefetch( (const __global int16*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
