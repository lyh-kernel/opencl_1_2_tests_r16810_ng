
kernel void test(global uint *results, local char *mem0, local char2 *mem2, local char2 *mem3, local char4 *mem4, local char8 *mem8, local char16 *mem16)
{
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
