__kernel void test_copy(__global unsigned int *src, __global unsigned int *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = src[tid];
}
