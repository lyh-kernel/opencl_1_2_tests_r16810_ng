__kernel void test_fn( __global float8 *srcValues, __global uint *offsets, __global float *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
    vstore8( srcValues[ tid ], offsets[ tid ], destBuffer + alignmentOffset );
}
