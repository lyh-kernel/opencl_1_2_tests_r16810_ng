__kernel void test_hi_uchar3(__global uchar3 *srcA, __global uchar2 *dst)
{
    int  tid = get_global_id(0);

    uchar2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
