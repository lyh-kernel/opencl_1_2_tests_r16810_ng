__kernel void test_hi_ulong4(__global ulong4 *srcA, __global ulong2 *dst)
{
    int  tid = get_global_id(0);

    ulong2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
