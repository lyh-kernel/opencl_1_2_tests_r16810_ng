__kernel void test_odd_long4(__global long4 *srcA, __global long2 *dst)
{
    int  tid = get_global_id(0);

    long2 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
