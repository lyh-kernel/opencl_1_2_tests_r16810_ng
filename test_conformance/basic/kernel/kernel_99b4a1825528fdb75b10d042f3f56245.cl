
__kernel void test_fn( const __global long *src, __global long *dst, __local long *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: longlonglong
 int i;
 prefetch( (const __global long*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
