__kernel void test_even_long16(__global long16 *srcA, __global long8 *dst)
{
    int  tid = get_global_id(0);

    long8 tmp = srcA[tid].even;
    dst[tid] = tmp;
}
