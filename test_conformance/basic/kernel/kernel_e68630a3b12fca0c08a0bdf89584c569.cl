
__kernel void test_fn( __global short8 *src, __global uchar16 *dst )
{
	int tid = get_global_id( 0 );
	uchar16 tmp = as_uchar16( src[ tid ] );
   dst[ tid ] = tmp;
}
