__kernel void test_fn( __global uchar8 *srcValues, __global uint *offsets, __global uchar *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
    vstore8( srcValues[ tid ], offsets[ tid ], destBuffer + alignmentOffset );
}
