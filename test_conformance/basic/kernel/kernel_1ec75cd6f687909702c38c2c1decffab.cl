
__kernel void test_fn( const __global uint4 *src, __global uint4 *dst, __local uint4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: uint4uintuint4
 int i;
 prefetch( (const __global uint4*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
