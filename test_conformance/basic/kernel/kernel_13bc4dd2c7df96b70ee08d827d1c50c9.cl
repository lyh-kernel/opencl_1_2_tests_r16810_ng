__kernel void test_hi_short2(__global short2 *srcA, __global short *dst)
{
    int  tid = get_global_id(0);

    short tmp = ((short2)(srcA[tid].s0, srcA[tid].s1)).hi;
    dst[tid] = tmp;
}
