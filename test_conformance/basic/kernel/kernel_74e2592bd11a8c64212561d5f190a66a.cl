__kernel void test_fn( __constant int *src, __global uint *offsets, __global uint *alignmentOffsets, __global int16 *results )
{
    int tid = get_global_id( 0 );
    int16 tmp = vload16( offsets[ tid ], ( (__constant int *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
