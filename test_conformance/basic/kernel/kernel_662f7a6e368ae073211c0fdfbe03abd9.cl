__kernel void test_conversion(__global uint *sourceValues, __global uint2 *destValues )
{
    int  tid = get_global_id(0);
    uint  src = sourceValues[tid];

    destValues[tid] = (uint2)src;

}
