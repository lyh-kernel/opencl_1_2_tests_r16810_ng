
__kernel void test_fn( __global int4 *src, __global char16 *dst )
{
	int tid = get_global_id( 0 );
	char16 tmp = as_char16( src[ tid ] );
   dst[ tid ] = tmp;
}
