__kernel void test_fn( __global float *src, __global uint *offsets, __global uint *alignmentOffsets, __global float2 *results )
{
    int tid = get_global_id( 0 );
    float2 tmp = vload2( offsets[ tid ], ( (__global float *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
