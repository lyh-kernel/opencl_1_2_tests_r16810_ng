
__kernel void test_fn( __global short16 *src, __global uint8 *dst )
{
	int tid = get_global_id( 0 );
	uint8 tmp = as_uint8( src[ tid ] );
   dst[ tid ] = tmp;
}
