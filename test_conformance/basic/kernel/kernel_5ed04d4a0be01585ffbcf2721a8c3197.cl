
__kernel void test_fn( const __global long4 *src, __global long4 *dst, __local long4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: long4longlong4
 int i;
 prefetch( (const __global long4*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
