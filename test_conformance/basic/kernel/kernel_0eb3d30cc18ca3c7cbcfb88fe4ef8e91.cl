__kernel void test_even_uchar2(__global uchar2 *srcA, __global uchar *dst)
{
    int  tid = get_global_id(0);

    uchar tmp = ((uchar2)(srcA[tid].s0, srcA[tid].s1)).even;
    dst[tid] = tmp;
}
