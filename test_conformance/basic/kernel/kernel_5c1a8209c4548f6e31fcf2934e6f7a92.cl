__kernel void test_odd_int2(__global int2 *srcA, __global int *dst)
{
    int  tid = get_global_id(0);

    int tmp = ((int2)(srcA[tid].s0, srcA[tid].s1)).odd;
    dst[tid] = tmp;
}
