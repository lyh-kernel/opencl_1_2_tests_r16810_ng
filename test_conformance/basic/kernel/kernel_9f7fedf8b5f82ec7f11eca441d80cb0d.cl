__kernel void test_hi_short16(__global short16 *srcA, __global short8 *dst)
{
    int  tid = get_global_id(0);

    short8 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
