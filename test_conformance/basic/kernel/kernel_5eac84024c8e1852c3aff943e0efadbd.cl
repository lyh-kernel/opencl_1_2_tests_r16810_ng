
kernel void test(global ulong *results, global long *mem0, global long2 *mem2, global long2 *mem3, global long4 *mem4, global long8 *mem8, global long16 *mem16)
{
   results[0] = (ulong)&mem0[0];
   results[1] = (ulong)&mem2[0];
   results[2] = (ulong)&mem3[0];
   results[3] = (ulong)&mem4[0];
   results[4] = (ulong)&mem8[0];
   results[5] = (ulong)&mem16[0];
}
