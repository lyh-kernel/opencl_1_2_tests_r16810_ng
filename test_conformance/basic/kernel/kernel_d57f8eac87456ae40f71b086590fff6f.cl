__kernel void test_conversion(__global uchar *sourceValues, __global uchar2 *destValues )
{
    int  tid = get_global_id(0);
    uchar  src = sourceValues[tid];

    destValues[tid] = (uchar2)src;

}
