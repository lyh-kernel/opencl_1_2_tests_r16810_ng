
__kernel void test_fn( __global ushort *src, __global short *dst )
{
	int tid = get_global_id( 0 );
	short tmp = as_short( src[ tid ] );
   dst[ tid ] = tmp;
}
