__kernel void test_lo_float3(__global float3 *srcA, __global float2 *dst)
{
    int  tid = get_global_id(0);

    float2 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
