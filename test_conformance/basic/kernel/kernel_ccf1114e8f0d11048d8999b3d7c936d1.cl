__kernel void test_hi_ushort8(__global ushort8 *srcA, __global ushort4 *dst)
{
    int  tid = get_global_id(0);

    ushort4 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
