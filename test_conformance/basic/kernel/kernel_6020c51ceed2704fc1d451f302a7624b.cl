
__kernel void test_fn( __global uint2 *src, __global int2 *dst )
{
	int tid = get_global_id( 0 );
	int2 tmp = as_int2( src[ tid ] );
   dst[ tid ] = tmp;
}
