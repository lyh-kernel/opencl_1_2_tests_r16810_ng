
__kernel void test_fn( __global short16 *src, __global long4 *dst )
{
	int tid = get_global_id( 0 );
	long4 tmp = as_long4( src[ tid ] );
   dst[ tid ] = tmp;
}
