__kernel void test_hi_short4(__global short4 *srcA, __global short2 *dst)
{
    int  tid = get_global_id(0);

    short2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
