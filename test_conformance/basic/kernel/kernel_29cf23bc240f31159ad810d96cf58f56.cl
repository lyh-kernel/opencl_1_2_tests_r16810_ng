
__kernel void test_fn( __global char *src, __global uchar *dst )
{
	int tid = get_global_id( 0 );
	uchar tmp = as_uchar( src[ tid ] );
   dst[ tid ] = tmp;
}
