
__kernel void test_fn( const __global short4 *src, __global short4 *dst, __local short4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: short4shortshort4
 int i;
 prefetch( (const __global short4*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
