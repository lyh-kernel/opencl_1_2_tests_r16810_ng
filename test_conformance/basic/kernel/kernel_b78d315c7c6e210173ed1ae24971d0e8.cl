
__kernel void test_fn( __global uint *src, __global float *dst )
{
	int tid = get_global_id( 0 );
	float3 tmp = as_float3( vload3(tid,src) );
   vstore3(tmp,tid,dst);
}
