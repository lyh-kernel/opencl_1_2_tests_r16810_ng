#define PRIV_TYPE ushort16
#define PRIV_SIZE 4
__kernel void test_fn( __global ushort16 *src, __global uint *offsets, __global uint *alignmentOffsets, __global ushort16 *results )
{
    __private PRIV_TYPE sPrivateStorage[ PRIV_SIZE ];
    int tid = get_global_id( 0 );

    for( int i = 0; i < 4; i++ )
      sPrivateStorage[ i ] = src[ i ];

    ushort16 tmp = vload16( offsets[ tid ], ( (__private ushort *) sPrivateStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
