
__kernel void test_fn( __global uchar4 *srcValues, __global uint *offsets, __global uchar4 *destBuffer, uint alignmentOffset )
{
    __private uchar4 sPrivateStorage[ 32 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (uchar4)(uchar)0;

   vstore4( srcValues[ tid ], offsets[ tid ], ( (__private uchar *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private uchar *sp = (__private uchar*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global uchar *dp = (__global uchar*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
