__kernel void test_hi_short3(__global short3 *srcA, __global short2 *dst)
{
    int  tid = get_global_id(0);

    short2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
