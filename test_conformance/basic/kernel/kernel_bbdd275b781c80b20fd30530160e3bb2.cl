
__kernel void test_fn( __global float *src, __global short2 *dst )
{
	int tid = get_global_id( 0 );
	short2 tmp = as_short2( src[ tid ] );
   dst[ tid ] = tmp;
}
