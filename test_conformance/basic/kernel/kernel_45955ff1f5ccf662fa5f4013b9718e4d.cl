
__kernel void test_fn( const __global ulong *src, __global ulong *dst, __local ulong *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ulongulongulong
 int i;
 prefetch( (const __global ulong*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
