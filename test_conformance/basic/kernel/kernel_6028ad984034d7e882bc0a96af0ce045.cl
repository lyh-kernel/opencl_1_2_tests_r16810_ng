__kernel void test_odd_ulong2(__global ulong2 *srcA, __global ulong *dst)
{
    int  tid = get_global_id(0);

    ulong tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
