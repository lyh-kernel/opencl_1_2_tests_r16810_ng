
__kernel void test_fn( const __global ushort *src, __global ushort *dst, __local ushort *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ushortushortushort
 int i;
 prefetch( (const __global ushort*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
