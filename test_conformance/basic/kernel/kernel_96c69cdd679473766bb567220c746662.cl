__kernel void test_fn(__local uint8 *sSharedStorage, __global uint8 *src, __global uint *offsets, __global uint *alignmentOffsets, __global uint8 *results )
{
    int tid = get_global_id( 0 );
   int lid = get_local_id( 0 );

    if( lid == 0 )
    {
        for( int i = 0; i < 4; i++ )
           sSharedStorage[ i ] = src[ i ];
    }
   barrier( CLK_LOCAL_MEM_FENCE );

    uint8 tmp = vload8( offsets[ tid ], ( (__local uint *) sSharedStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
