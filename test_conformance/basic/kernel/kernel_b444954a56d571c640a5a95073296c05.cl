__kernel void test_fn( __constant float *src, __global uint *offsets, __global uint *alignmentOffsets, __global float8 *results )
{
    int tid = get_global_id( 0 );
    float8 tmp = vload8( offsets[ tid ], ( (__constant float *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
