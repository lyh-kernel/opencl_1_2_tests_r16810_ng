__kernel void test_fn( __global float *src, __global uint *offsets, __global uint *alignmentOffsets, __global float16 *results )
{
    int tid = get_global_id( 0 );
    float16 tmp = vload16( offsets[ tid ], ( (__global float *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
