__kernel void test_hi_char3(__global char3 *srcA, __global char2 *dst)
{
    int  tid = get_global_id(0);

    char2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
