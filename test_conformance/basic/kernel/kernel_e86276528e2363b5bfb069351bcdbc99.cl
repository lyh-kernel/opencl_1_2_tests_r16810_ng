
__kernel void test_fn( __global int4 *src, __global uint *dst )
{
	int tid = get_global_id( 0 );
	uint3 tmp = as_uint3( src[ tid ] );
   vstore3(tmp,tid,dst);
}
