__kernel void test_long_mul(__global long *srcA, __global long *srcB, __global long *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] * srcB[tid];
}
