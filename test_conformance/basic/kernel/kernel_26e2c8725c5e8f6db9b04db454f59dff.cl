
__kernel void test_fn( __global int4 *src, __global float *dst )
{
	int tid = get_global_id( 0 );
	float3 tmp = as_float3( src[ tid ] );
   vstore3(tmp,tid,dst);
}
