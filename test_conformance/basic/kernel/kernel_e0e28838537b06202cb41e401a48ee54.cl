__kernel void test_odd_int3(__global int3 *srcA, __global int2 *dst)
{
    int  tid = get_global_id(0);

    int2 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
