__kernel void test_odd_char8(__global char8 *srcA, __global char4 *dst)
{
    int  tid = get_global_id(0);

    char4 tmp = ((char8)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3, srcA[tid].s4, srcA[tid].s5, srcA[tid].s6, srcA[tid].s7)).odd;
    dst[tid] = tmp;
}
