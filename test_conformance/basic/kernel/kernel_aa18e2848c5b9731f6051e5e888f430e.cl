
__kernel void test_vector_creation(__global short *src, __global short *result) {
	vstore3( (short3)(src[0],src[1],src[2]), 0, result );
	vstore3( (short3)(src[0],vload2(0,src+1)), 1, result );
	vstore3( (short3)(vload2(0,src+0),src[2]), 2, result );
	vstore3( (short3)(vload3(0,src+0)), 3, result );
}

