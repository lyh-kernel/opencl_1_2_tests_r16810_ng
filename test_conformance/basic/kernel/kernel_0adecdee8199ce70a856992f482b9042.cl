
__kernel void test_fn( const __global int2 *src, __global int2 *dst, __local int2 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: int2intint2
 int i;
 prefetch( (const __global int2*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
