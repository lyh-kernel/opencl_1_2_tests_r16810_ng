
__kernel void test_fn( const __global short2 *src, __global short2 *dst, __local short2 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: short2shortshort2
 int i;
 prefetch( (const __global short2*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
