
__kernel void test_fn( __global int *src, __global uint *dst )
{
	int tid = get_global_id( 0 );
	uint3 tmp = as_uint3( vload3(tid,src) );
   vstore3(tmp,tid,dst);
}
