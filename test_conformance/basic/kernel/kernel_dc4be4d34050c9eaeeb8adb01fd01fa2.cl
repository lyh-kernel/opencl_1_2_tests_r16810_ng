__kernel void test_even_char8(__global char8 *srcA, __global char4 *dst)
{
    int  tid = get_global_id(0);

    char4 tmp = srcA[tid].even;
    dst[tid] = tmp;
}
