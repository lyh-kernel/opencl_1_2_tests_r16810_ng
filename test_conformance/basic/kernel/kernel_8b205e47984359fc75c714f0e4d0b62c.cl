
__kernel void test_fn( const __global ulong16 *src, __global ulong16 *dst, __local ulong16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ulong16ulongulong16
 int i;
 prefetch( (const __global ulong16*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
