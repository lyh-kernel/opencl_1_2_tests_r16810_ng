__kernel void test_even_ulong16(__global ulong16 *srcA, __global ulong8 *dst)
{
    int  tid = get_global_id(0);

    ulong8 tmp = srcA[tid].even;
    dst[tid] = tmp;
}
