
__kernel void test_fn( __global float4 *srcValues, __global uint *offsets, __global float4 *destBuffer, uint alignmentOffset )
{
    __private float4 sPrivateStorage[ 8 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (float4)(float)0;

   vstore4( srcValues[ tid ], offsets[ tid ], ( (__private float *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private float *sp = (__private float*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global float *dp = (__global float*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
