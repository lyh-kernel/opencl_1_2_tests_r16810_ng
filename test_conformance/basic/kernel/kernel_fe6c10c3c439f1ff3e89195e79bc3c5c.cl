
kernel void test(global uint *results, constant float *mem0, constant float2 *mem2, constant float2 *mem3, constant float4 *mem4, constant float8 *mem8, constant float16 *mem16)
{
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
