__kernel void test_fn( __constant uchar *src, __global uint *offsets, __global uint *alignmentOffsets, __global uchar2 *results )
{
    int tid = get_global_id( 0 );
    uchar2 tmp = vload2( offsets[ tid ], ( (__constant uchar *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
