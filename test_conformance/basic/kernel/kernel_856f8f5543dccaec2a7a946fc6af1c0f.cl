__kernel void test_lo_float16(__global float16 *srcA, __global float8 *dst)
{
    int  tid = get_global_id(0);

    float8 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
