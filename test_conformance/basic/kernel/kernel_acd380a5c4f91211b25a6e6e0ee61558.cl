
__kernel void test_fn( __global long *src, __global ulong4 *dst )
{
	int tid = get_global_id( 0 );
	ulong4 tmp = as_ulong4( vload3(tid,src) );
   dst[ tid ] = tmp;
}
