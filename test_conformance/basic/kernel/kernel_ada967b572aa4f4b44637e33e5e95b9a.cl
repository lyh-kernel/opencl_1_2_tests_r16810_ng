__kernel void test_lo_long2(__global long2 *srcA, __global long *dst)
{
    int  tid = get_global_id(0);

    long tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
