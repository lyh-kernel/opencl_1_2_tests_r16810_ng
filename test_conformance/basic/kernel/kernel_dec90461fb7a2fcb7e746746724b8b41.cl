__kernel void test_conversion(__global float *sourceValues, __global float2 *destValues )
{
    int  tid = get_global_id(0);
    float  src = sourceValues[tid];

    destValues[tid] = (float2)src;

}
