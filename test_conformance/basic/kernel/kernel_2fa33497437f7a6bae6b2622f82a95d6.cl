__kernel void test_fn( __global uchar *src, __global uint *offsets, __global uint *alignmentOffsets, __global uchar4 *results )
{
    int tid = get_global_id( 0 );
    uchar4 tmp = vload4( offsets[ tid ], ( (__global uchar *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
