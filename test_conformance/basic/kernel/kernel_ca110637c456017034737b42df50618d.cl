
__kernel void test_fn( __global long16 *src, __global ulong16 *dst )
{
	int tid = get_global_id( 0 );
	ulong16 tmp = as_ulong16( src[ tid ] );
   dst[ tid ] = tmp;
}
