__kernel void test_odd_uint16(__global uint16 *srcA, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    uint8 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
