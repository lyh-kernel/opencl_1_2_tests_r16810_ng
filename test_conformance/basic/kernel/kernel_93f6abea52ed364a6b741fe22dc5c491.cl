__kernel void test_hi_float4(__global float4 *srcA, __global float2 *dst)
{
    int  tid = get_global_id(0);

    float2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
