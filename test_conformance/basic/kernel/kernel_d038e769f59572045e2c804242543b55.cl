
__kernel void test_fn( const __global int4 *src, __global int4 *dst, __local int4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 int i;
 for(i=0; i<copiesPerWorkItem; i++)
  localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ] = (int4)(int)0;
	barrier( CLK_LOCAL_MEM_FENCE );
 for(i=0; i<copiesPerWorkItem; i++)
  localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
	barrier( CLK_LOCAL_MEM_FENCE );
	event_t event;
	event = async_work_group_copy((__global int4*)(dst+copiesPerWorkgroup*get_group_id(0)), (__local const int4*)localBuffer, (size_t)copiesPerWorkgroup, (event_t)0 );
	wait_group_events( 1, &event );
}
