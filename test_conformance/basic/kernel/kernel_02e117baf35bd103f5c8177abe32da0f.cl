__kernel void test_fn( __global int *src, __global uint *offsets, __global uint *alignmentOffsets, __global int8 *results )
{
    int tid = get_global_id( 0 );
    int8 tmp = vload8( offsets[ tid ], ( (__global int *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
