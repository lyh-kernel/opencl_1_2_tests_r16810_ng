
__kernel void test_fn(__local long4 *sSharedStorage, __global long4 *srcValues, __global uint *offsets, __global long4 *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
 sSharedStorage[ offsets[tid] ] = (long4)(long)0;
 sSharedStorage[ offsets[tid] +1 ] =  sSharedStorage[ offsets[tid] ];
   barrier( CLK_LOCAL_MEM_FENCE );

    vstore4( srcValues[ tid ], offsets[ tid ], ( (__local long *)sSharedStorage ) + alignmentOffset );

   barrier( CLK_LOCAL_MEM_FENCE );

  int i;
  __local long *sp = (__local long*) (sSharedStorage + offsets[tid]) + alignmentOffset;
  __global long *dp = (__global long*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; (size_t)i < sizeof( sSharedStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
