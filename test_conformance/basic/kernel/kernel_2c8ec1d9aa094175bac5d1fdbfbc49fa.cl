__kernel void test_fn(__local float2 *sSharedStorage, __global float2 *src, __global uint *offsets, __global uint *alignmentOffsets, __global float2 *results )
{
    int tid = get_global_id( 0 );
   int lid = get_local_id( 0 );

    if( lid == 0 )
    {
        for( int i = 0; i < 16; i++ )
           sSharedStorage[ i ] = src[ i ];
    }
   barrier( CLK_LOCAL_MEM_FENCE );

    float2 tmp = vload2( offsets[ tid ], ( (__local float *) sSharedStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
