
__kernel void test_fn( __global uint4 *src, __global float4 *dst )
{
	int tid = get_global_id( 0 );
	float4 tmp = as_float4( src[ tid ] );
   dst[ tid ] = tmp;
}
