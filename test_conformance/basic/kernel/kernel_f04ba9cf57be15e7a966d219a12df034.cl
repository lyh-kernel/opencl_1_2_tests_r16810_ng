
__kernel void test_fn( const __global long8 *src, __global long8 *dst, __local long8 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: long8longlong8
 int i;
 prefetch( (const __global long8*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
