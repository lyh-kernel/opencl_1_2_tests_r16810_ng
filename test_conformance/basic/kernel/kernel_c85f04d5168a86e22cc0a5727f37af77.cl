
__kernel void test_fn( __global ulong2 *src, __global long2 *dst )
{
	int tid = get_global_id( 0 );
	long2 tmp = as_long2( src[ tid ] );
   dst[ tid ] = tmp;
}
