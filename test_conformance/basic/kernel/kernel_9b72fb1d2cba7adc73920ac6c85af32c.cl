__kernel void test_hi_long4(__global long4 *srcA, __global long2 *dst)
{
    int  tid = get_global_id(0);

    long2 tmp = ((long4)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3)).hi;
    dst[tid] = tmp;
}
