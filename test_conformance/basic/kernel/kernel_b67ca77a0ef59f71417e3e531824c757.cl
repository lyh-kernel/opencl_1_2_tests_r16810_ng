__kernel void test_even_uint2(__global uint2 *srcA, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint tmp = srcA[tid].even;
    dst[tid] = tmp;
}
