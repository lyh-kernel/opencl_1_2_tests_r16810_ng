__kernel void test_hi_uint3(__global uint3 *srcA, __global uint2 *dst)
{
    int  tid = get_global_id(0);

    uint2 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
