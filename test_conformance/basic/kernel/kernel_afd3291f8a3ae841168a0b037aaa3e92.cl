__kernel void test_lo_uint16(__global uint16 *srcA, __global uint8 *dst)
{
    int  tid = get_global_id(0);

    uint8 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
