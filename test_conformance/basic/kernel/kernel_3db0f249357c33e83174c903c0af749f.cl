__kernel void test_fn( __global ushort *src, __global uint *offsets, __global uint *alignmentOffsets, __global ushort16 *results )
{
    int tid = get_global_id( 0 );
    ushort16 tmp = vload16( offsets[ tid ], ( (__global ushort *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
