
__kernel void test_fn( __global uint2 *src, __global ulong *dst )
{
	int tid = get_global_id( 0 );
	ulong tmp = as_ulong( src[ tid ] );
   dst[ tid ] = tmp;
}
