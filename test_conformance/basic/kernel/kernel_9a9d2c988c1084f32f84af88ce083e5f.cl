
__kernel void test_fn( const __global float *src, __global float *dst, __local float *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: floatfloatfloat
 int i;
 prefetch( (const __global float*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
