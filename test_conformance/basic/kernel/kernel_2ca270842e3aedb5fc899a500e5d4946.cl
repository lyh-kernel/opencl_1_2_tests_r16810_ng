__kernel void test_conversion(__global short *sourceValues, __global short4 *destValues )
{
    int  tid = get_global_id(0);
    short  src = sourceValues[tid];

    destValues[tid] = (short4)src;

}
