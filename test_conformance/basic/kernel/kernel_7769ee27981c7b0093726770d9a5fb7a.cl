
__kernel void test_fn( const __global uint *src, __global uint *dst, __local uint *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: uintuintuint
 int i;
 prefetch( (const __global uint*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
