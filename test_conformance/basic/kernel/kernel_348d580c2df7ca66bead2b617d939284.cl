__kernel void test_fn(__local ulong2 *sSharedStorage, __global ulong2 *src, __global uint *offsets, __global uint *alignmentOffsets, __global ulong2 *results )
{
    int tid = get_global_id( 0 );
   int lid = get_local_id( 0 );

    if( lid == 0 )
    {
        for( int i = 0; i < 512; i++ )
           sSharedStorage[ i ] = src[ i ];
    }
   barrier( CLK_LOCAL_MEM_FENCE );

    ulong2 tmp = vload2( offsets[ tid ], ( (__local ulong *) sSharedStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
