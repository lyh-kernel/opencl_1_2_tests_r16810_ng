
__kernel void test_fn( __global char *src, __global uchar *dst )
{
	int tid = get_global_id( 0 );
	uchar3 tmp = as_uchar3( vload3(tid,src) );
   vstore3(tmp,tid,dst);
}
