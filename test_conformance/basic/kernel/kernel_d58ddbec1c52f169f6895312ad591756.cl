
__kernel void test_fn( __global float2 *src, __global uint2 *dst )
{
	int tid = get_global_id( 0 );
	uint2 tmp = as_uint2( src[ tid ] );
   dst[ tid ] = tmp;
}
