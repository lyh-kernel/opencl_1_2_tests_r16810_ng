
__kernel void test_fn( const __global ulong4 *src, __global ulong4 *dst, __local ulong4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ulong4ulongulong4
 int i;
 prefetch( (const __global ulong4*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
