
__kernel void test_fn( const __global uchar4 *src, __global uchar4 *dst, __local uchar4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: uchar4ucharuchar4
 int i;
 prefetch( (const __global uchar4*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
