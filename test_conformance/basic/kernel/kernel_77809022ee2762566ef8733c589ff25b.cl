__kernel void test_fn(__local ushort *sSharedStorage, __global ushort *src, __global uint *offsets, __global uint *alignmentOffsets, __global ushort *results )
{
    int tid = get_global_id( 0 );
   int lid = get_local_id( 0 );

    if( lid == 0 )
    {
        for( int i = 0; i < 1365; i++ ) {
           sSharedStorage[ 3*i   ] = src[ 3*i   ];
           sSharedStorage[ 3*i +1] = src[ 3*i +1];
           sSharedStorage[ 3*i +2] = src[ 3*i +2];
        }
    }
   barrier( CLK_LOCAL_MEM_FENCE );

    ushort3 tmp = vload3( offsets[ tid ], ( (__local ushort *) sSharedStorage ) + alignmentOffsets[ tid ] );
   results[ 3*tid   ] = tmp.s0;
   results[ 3*tid +1] = tmp.s1;
   results[ 3*tid +2] = tmp.s2;
}
