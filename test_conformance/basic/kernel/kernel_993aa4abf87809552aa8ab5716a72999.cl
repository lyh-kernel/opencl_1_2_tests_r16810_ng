__kernel void test_lo_int8(__global int8 *srcA, __global int4 *dst)
{
    int  tid = get_global_id(0);

    int4 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
