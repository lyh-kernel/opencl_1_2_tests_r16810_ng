
__kernel void test_fn( const __global ulong8 *src, __global ulong8 *dst, __local ulong8 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ulong8ulongulong8
 int i;
 prefetch( (const __global ulong8*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
