__kernel void test_fn( __global float2 *srcValues, __global uint *offsets, __global float *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
    vstore2( srcValues[ tid ], offsets[ tid ], destBuffer + alignmentOffset );
}
