
__kernel void test_fn( __global short16 *src, __global float8 *dst )
{
	int tid = get_global_id( 0 );
	float8 tmp = as_float8( src[ tid ] );
   dst[ tid ] = tmp;
}
