
__kernel void test_fn( __global int16 *src, __global uint16 *dst )
{
	int tid = get_global_id( 0 );
	uint16 tmp = as_uint16( src[ tid ] );
   dst[ tid ] = tmp;
}
