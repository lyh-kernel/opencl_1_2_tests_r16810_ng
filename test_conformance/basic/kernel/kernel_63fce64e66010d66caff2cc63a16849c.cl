
__kernel __attribute__((vec_type_hint(uint))) void sample_test(__global int *src, __global int *dst)
{
    int  tid = get_global_id(0);
	 dst[tid] = src[tid];

}
