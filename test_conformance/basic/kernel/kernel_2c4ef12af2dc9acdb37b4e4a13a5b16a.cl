
__kernel void test_fn( const __global uint16 *src, __global uint16 *dst, __local uint16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 int i;
 for(i=0; i<copiesPerWorkItem; i++)
  localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ] = (uint16)(uint)0;
	barrier( CLK_LOCAL_MEM_FENCE );
 for(i=0; i<copiesPerWorkItem; i++)
  localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
	barrier( CLK_LOCAL_MEM_FENCE );
	event_t event;
	event = async_work_group_copy((__global uint16*)(dst+copiesPerWorkgroup*get_group_id(0)), (__local const uint16*)localBuffer, (size_t)copiesPerWorkgroup, (event_t)0 );
	wait_group_events( 1, &event );
}
