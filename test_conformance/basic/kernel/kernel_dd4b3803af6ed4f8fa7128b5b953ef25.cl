__kernel void test_conversion(__global char *sourceValues, __global char4 *destValues )
{
    int  tid = get_global_id(0);
    char  src = sourceValues[tid];

    destValues[tid] = (char4)src;

}
