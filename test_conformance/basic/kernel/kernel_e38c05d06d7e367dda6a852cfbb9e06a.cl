
__kernel void test_fn( __global float *src, __global uint4 *dst )
{
	int tid = get_global_id( 0 );
	uint4 tmp = as_uint4( vload3(tid,src) );
   dst[ tid ] = tmp;
}
