__kernel void test_fn( __global float *src, __global uint *offsets, __global uint *alignmentOffsets, __global float4 *results )
{
    int tid = get_global_id( 0 );
    float4 tmp = vload4( offsets[ tid ], ( (__global float *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
