
__kernel void test_fn( const __global uchar *src, __global uchar *dst, __local uchar *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: ucharucharuchar
 int i;
 prefetch( (const __global uchar*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
