
__kernel void test_fn( __global uchar *src, __global char *dst )
{
	int tid = get_global_id( 0 );
	char tmp = as_char( src[ tid ] );
   dst[ tid ] = tmp;
}
