__kernel void test_fpadd2(__global float2 *srcA, __global float2 *srcB, __global float2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] + srcB[tid];
}
