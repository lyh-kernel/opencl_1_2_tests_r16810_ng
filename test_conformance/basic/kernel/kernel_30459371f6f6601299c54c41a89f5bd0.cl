
__kernel void test_fn( const __global long2 *src, __global long2 *dst, __local long2 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: long2longlong2
 int i;
 prefetch( (const __global long2*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
