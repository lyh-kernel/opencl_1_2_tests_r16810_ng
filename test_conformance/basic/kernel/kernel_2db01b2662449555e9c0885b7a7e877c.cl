
__kernel void test_fn( __global short2 *src, __global uint *dst )
{
	int tid = get_global_id( 0 );
	uint tmp = as_uint( src[ tid ] );
   dst[ tid ] = tmp;
}
