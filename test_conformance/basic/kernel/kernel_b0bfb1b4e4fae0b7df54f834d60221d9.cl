__kernel void test_even_int2(__global int2 *srcA, __global int *dst)
{
    int  tid = get_global_id(0);

    int tmp = srcA[tid].even;
    dst[tid] = tmp;
}
