__kernel void test_fn(__local ushort16 *sSharedStorage, __global ushort16 *src, __global uint *offsets, __global uint *alignmentOffsets, __global ushort16 *results )
{
    int tid = get_global_id( 0 );
   int lid = get_local_id( 0 );

    if( lid == 0 )
    {
        for( int i = 0; i < 256; i++ )
           sSharedStorage[ i ] = src[ i ];
    }
   barrier( CLK_LOCAL_MEM_FENCE );

    ushort16 tmp = vload16( offsets[ tid ], ( (__local ushort *) sSharedStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
