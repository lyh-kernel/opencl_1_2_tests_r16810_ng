__kernel void test_lo_uchar8(__global uchar8 *srcA, __global uchar4 *dst)
{
    int  tid = get_global_id(0);

    uchar4 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
