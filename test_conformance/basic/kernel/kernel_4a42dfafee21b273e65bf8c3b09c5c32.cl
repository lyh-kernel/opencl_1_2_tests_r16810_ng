#define PRIV_TYPE int4
#define PRIV_SIZE 16
__kernel void test_fn( __global int4 *src, __global uint *offsets, __global uint *alignmentOffsets, __global int4 *results )
{
    __private PRIV_TYPE sPrivateStorage[ PRIV_SIZE ];
    int tid = get_global_id( 0 );

    for( int i = 0; i < 16; i++ )
      sPrivateStorage[ i ] = src[ i ];

    int4 tmp = vload4( offsets[ tid ], ( (__private int *) sPrivateStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
