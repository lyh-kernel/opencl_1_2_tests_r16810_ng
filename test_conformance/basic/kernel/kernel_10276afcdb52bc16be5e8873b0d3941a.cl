
__kernel void test_fn( const __global short16 *src, __global short16 *dst, __local short16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: short16shortshort16
 int i;
 prefetch( (const __global short16*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
