
__kernel void test_fn( __global int8 *src, __global ushort16 *dst )
{
	int tid = get_global_id( 0 );
	ushort16 tmp = as_ushort16( src[ tid ] );
   dst[ tid ] = tmp;
}
