__kernel void test_hi_int16(__global int16 *srcA, __global int8 *dst)
{
    int  tid = get_global_id(0);

    int8 tmp = ((int16)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3, srcA[tid].s4, srcA[tid].s5, srcA[tid].s6, srcA[tid].s7, srcA[tid].s8, srcA[tid].s9, srcA[tid].sA, srcA[tid].sB, srcA[tid].sC, srcA[tid].sD, srcA[tid].sE, srcA[tid].sf)).hi;
    dst[tid] = tmp;
}
