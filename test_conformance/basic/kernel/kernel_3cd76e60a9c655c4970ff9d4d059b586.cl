__kernel void test_int2float(__global int *src, __global float *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = (float)src[tid];

}
