__kernel void test_fn( __constant char *src, __global uint *offsets, __global uint *alignmentOffsets, __global char4 *results )
{
    int tid = get_global_id( 0 );
    char4 tmp = vload4( offsets[ tid ], ( (__constant char *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
