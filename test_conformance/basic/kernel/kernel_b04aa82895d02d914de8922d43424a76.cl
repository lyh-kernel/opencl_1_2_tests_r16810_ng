__kernel void test_lo_float8(__global float8 *srcA, __global float4 *dst)
{
    int  tid = get_global_id(0);

    float4 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
