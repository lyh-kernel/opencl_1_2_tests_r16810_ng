
__kernel void test_fn( __global uchar16 *src, __global ulong2 *dst )
{
	int tid = get_global_id( 0 );
	ulong2 tmp = as_ulong2( src[ tid ] );
   dst[ tid ] = tmp;
}
