__kernel void test_lo_float4(__global float4 *srcA, __global float2 *dst)
{
    int  tid = get_global_id(0);

    float2 tmp = ((float4)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3)).lo;
    dst[tid] = tmp;
}
