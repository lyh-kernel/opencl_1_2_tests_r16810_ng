
__kernel void test_fn( __global short *src, __global ushort4 *dst )
{
	int tid = get_global_id( 0 );
	ushort4 tmp = as_ushort4( vload3(tid,src) );
   dst[ tid ] = tmp;
}
