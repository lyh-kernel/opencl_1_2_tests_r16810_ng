__kernel void test_lo_int2(__global int2 *srcA, __global int *dst)
{
    int  tid = get_global_id(0);

    int tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
