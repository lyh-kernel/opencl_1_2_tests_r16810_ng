__kernel void test_odd_ushort2(__global ushort2 *srcA, __global ushort *dst)
{
    int  tid = get_global_id(0);

    ushort tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
