__kernel void test_long_mad2(__global long2 *srcA, __global long2 *srcB, __global long2 *srcC, __global long2 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] * srcB[tid] + srcC[tid];
}
