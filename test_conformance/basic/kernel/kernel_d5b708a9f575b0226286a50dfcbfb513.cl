#define PRIV_TYPE short4
#define PRIV_SIZE 16
__kernel void test_fn( __global short4 *src, __global uint *offsets, __global uint *alignmentOffsets, __global short4 *results )
{
    __private PRIV_TYPE sPrivateStorage[ PRIV_SIZE ];
    int tid = get_global_id( 0 );

    for( int i = 0; i < 16; i++ )
      sPrivateStorage[ i ] = src[ i ];

    short4 tmp = vload4( offsets[ tid ], ( (__private short *) sPrivateStorage ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
