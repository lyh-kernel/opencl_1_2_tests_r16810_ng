__kernel void test_hi_float2(__global float2 *srcA, __global float *dst)
{
    int  tid = get_global_id(0);

    float tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
