
__kernel void test_fn( __global uint2 *src, __global ushort4 *dst )
{
	int tid = get_global_id( 0 );
	ushort4 tmp = as_ushort4( src[ tid ] );
   dst[ tid ] = tmp;
}
