
__kernel void test_fn( const __global uint2 *src, __global uint2 *dst, __local uint2 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: uint2uintuint2
 int i;
 prefetch( (const __global uint2*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
