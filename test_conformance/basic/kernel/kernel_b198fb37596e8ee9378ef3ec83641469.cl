
kernel void test(global ulong *results, constant ulong *mem0, constant ulong2 *mem2, constant ulong2 *mem3, constant ulong4 *mem4, constant ulong8 *mem8, constant ulong16 *mem16)
{
   results[0] = (ulong)&mem0[0];
   results[1] = (ulong)&mem2[0];
   results[2] = (ulong)&mem3[0];
   results[3] = (ulong)&mem4[0];
   results[4] = (ulong)&mem8[0];
   results[5] = (ulong)&mem16[0];
}
