
__kernel void test_fn( const __global int *src, __global int *dst, __local int *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: intintint
 int i;
 prefetch( (const __global int*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
