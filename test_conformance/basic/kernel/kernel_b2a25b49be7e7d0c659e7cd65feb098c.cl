
__kernel void test_fn( __global uchar4 *src, __global char4 *dst )
{
	int tid = get_global_id( 0 );
	char4 tmp = as_char4( src[ tid ] );
   dst[ tid ] = tmp;
}
