__kernel void test_fn( __constant short *src, __global uint *offsets, __global uint *alignmentOffsets, __global short16 *results )
{
    int tid = get_global_id( 0 );
    short16 tmp = vload16( offsets[ tid ], ( (__constant short *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
