__kernel void test_fn( __global short *src, __global uint *offsets, __global uint *alignmentOffsets, __global short4 *results )
{
    int tid = get_global_id( 0 );
    short4 tmp = vload4( offsets[ tid ], ( (__global short *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
