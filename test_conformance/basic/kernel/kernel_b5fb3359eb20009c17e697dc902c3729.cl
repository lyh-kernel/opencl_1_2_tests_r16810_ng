
__kernel void test_fn( __global short4 *src, __global char8 *dst )
{
	int tid = get_global_id( 0 );
	char8 tmp = as_char8( src[ tid ] );
   dst[ tid ] = tmp;
}
