
__kernel void test_fn( __global int8 *srcValues, __global uint *offsets, __global int8 *destBuffer, uint alignmentOffset )
{
    __private int8 sPrivateStorage[ 8 ];
    int tid = get_global_id( 0 );
 sPrivateStorage[tid] = (int8)(int)0;

   vstore8( srcValues[ tid ], offsets[ tid ], ( (__private int *)sPrivateStorage ) + alignmentOffset );

  uint i;
  __private int *sp = (__private int*) (sPrivateStorage + offsets[tid]) + alignmentOffset;
  __global int *dp = (__global int*) (destBuffer + offsets[tid]) + alignmentOffset;
  for( i = 0; i < sizeof( sPrivateStorage[0]) / sizeof( *sp ); i++ ) 
       dp[i] = sp[i];
}
