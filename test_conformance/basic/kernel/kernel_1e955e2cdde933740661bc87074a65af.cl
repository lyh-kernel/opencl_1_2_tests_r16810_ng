__kernel void test_hi_uchar8(__global uchar8 *srcA, __global uchar4 *dst)
{
    int  tid = get_global_id(0);

    uchar4 tmp = ((uchar8)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3, srcA[tid].s4, srcA[tid].s5, srcA[tid].s6, srcA[tid].s7)).hi;
    dst[tid] = tmp;
}
