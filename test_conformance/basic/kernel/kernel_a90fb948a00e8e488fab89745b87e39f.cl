__kernel void test_lo_uint8(__global uint8 *srcA, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    uint4 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
