__kernel void test_hi_int16(__global int16 *srcA, __global int8 *dst)
{
    int  tid = get_global_id(0);

    int8 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
