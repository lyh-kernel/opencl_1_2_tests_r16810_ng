
__kernel void test_fn( const __global char8 *src, __global char8 *dst, __local char8 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: char8charchar8
 int i;
 prefetch( (const __global char8*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
