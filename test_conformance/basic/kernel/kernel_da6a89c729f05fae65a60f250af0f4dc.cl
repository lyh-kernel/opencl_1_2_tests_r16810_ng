
__kernel void test_fn( __global ushort16 *src, __global int8 *dst )
{
	int tid = get_global_id( 0 );
	int8 tmp = as_int8( src[ tid ] );
   dst[ tid ] = tmp;
}
