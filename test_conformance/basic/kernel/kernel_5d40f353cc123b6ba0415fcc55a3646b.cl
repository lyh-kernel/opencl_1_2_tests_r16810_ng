
__kernel void test_fn( const __global long16 *src, __global long16 *dst, __local long16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: long16longlong16
 int i;
 prefetch( (const __global long16*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
