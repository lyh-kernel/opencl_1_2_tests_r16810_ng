__kernel void test_conversion(__global ushort *sourceValues, __global ushort4 *destValues )
{
    int  tid = get_global_id(0);
    ushort  src = sourceValues[tid];

    destValues[tid] = (ushort4)src;

}
