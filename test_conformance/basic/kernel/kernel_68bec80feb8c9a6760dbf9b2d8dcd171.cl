__kernel void test_fn( __global uint4 *srcValues, __global uint *offsets, __global uint *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
    vstore4( srcValues[ tid ], offsets[ tid ], destBuffer + alignmentOffset );
}
