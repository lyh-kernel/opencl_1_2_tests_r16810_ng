__kernel void test_odd_uint8(__global uint8 *srcA, __global uint4 *dst)
{
    int  tid = get_global_id(0);

    uint4 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
