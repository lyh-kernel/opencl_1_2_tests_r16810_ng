__kernel void test_even_uint2(__global uint2 *srcA, __global uint *dst)
{
    int  tid = get_global_id(0);

    uint tmp = ((uint2)(srcA[tid].s0, srcA[tid].s1)).even;
    dst[tid] = tmp;
}
