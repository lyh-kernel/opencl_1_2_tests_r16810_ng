
kernel void test(global uint *results)
{
   local float mem0[3];
   local float2 mem2[3];
   local float3 mem3[3];
   local float4 mem4[3];
   local float8 mem8[3];
   local float16 mem16[3];
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
