__kernel void test_lo_int4(__global int4 *srcA, __global int2 *dst)
{
    int  tid = get_global_id(0);

    int2 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
