__kernel void test_fn( __constant char *src, __global uint *offsets, __global uint *alignmentOffsets, __global char16 *results )
{
    int tid = get_global_id( 0 );
    char16 tmp = vload16( offsets[ tid ], ( (__constant char *) src ) + alignmentOffsets[ tid ] );
   results[ tid ] = tmp;
}
