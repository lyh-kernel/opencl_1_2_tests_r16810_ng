
__kernel void test_fn( __global int *src, __global float *dst )
{
	int tid = get_global_id( 0 );
	float tmp = as_float( src[ tid ] );
   dst[ tid ] = tmp;
}
