
__kernel void test_fn( __global long8 *src, __global ulong8 *dst )
{
	int tid = get_global_id( 0 );
	ulong8 tmp = as_ulong8( src[ tid ] );
   dst[ tid ] = tmp;
}
