__kernel void test_hi_float8(__global float8 *srcA, __global float4 *dst)
{
    int  tid = get_global_id(0);

    float4 tmp = ((float8)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3, srcA[tid].s4, srcA[tid].s5, srcA[tid].s6, srcA[tid].s7)).hi;
    dst[tid] = tmp;
}
