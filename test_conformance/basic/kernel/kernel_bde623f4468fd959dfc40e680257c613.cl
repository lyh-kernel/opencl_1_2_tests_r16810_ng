__kernel void test_fn( __global short8 *srcValues, __global uint *offsets, __global short *destBuffer, uint alignmentOffset )
{
    int tid = get_global_id( 0 );
    vstore8( srcValues[ tid ], offsets[ tid ], destBuffer + alignmentOffset );
}
