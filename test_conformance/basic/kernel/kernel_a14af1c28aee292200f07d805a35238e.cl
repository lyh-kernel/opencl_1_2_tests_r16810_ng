__kernel void test_lo_char16(__global char16 *srcA, __global char8 *dst)
{
    int  tid = get_global_id(0);

    char8 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
