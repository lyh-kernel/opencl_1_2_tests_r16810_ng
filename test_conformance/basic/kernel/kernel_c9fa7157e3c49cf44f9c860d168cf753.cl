
kernel void test(global uint *results)
{
   local ushort mem0[3];
   local ushort2 mem2[3];
   local ushort3 mem3[3];
   local ushort4 mem4[3];
   local ushort8 mem8[3];
   local ushort16 mem16[3];
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
