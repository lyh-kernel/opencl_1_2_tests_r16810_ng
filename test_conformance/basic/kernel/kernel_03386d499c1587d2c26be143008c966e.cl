__kernel void test_hi_int2(__global int2 *srcA, __global int *dst)
{
    int  tid = get_global_id(0);

    int tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
