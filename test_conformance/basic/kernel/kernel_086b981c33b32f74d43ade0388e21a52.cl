
__kernel void test_fn( const __global int4 *src, __global int4 *dst, __local int4 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem, int stride )
{
 int i;
 for(i=0; i<copiesPerWorkItem; i++)
   localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ] = (int4)(int)0;
 barrier( CLK_LOCAL_MEM_FENCE );
 event_t event;
 event = async_work_group_strided_copy( (__local int4*)localBuffer, (__global const int4*)(src+copiesPerWorkgroup*stride*get_group_id(0)), (size_t)copiesPerWorkgroup, (size_t)stride, (event_t)0 );
 wait_group_events( 1, &event );
 for(i=0; i<copiesPerWorkItem; i++)
   dst[ get_global_id( 0 )*copiesPerWorkItem*stride+i*stride ] = localBuffer[ get_local_id( 0 )*copiesPerWorkItem+i ];
}
