__kernel void test_lo_long3(__global long3 *srcA, __global long2 *dst)
{
    int  tid = get_global_id(0);

    long2 tmp = srcA[tid].lo;
    dst[tid] = tmp;
}
