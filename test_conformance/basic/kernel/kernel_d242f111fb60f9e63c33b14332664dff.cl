__kernel void test_hi_long8(__global long8 *srcA, __global long4 *dst)
{
    int  tid = get_global_id(0);

    long4 tmp = srcA[tid].hi;
    dst[tid] = tmp;
}
