
__kernel void test_fn( __global short *src, __global ushort *dst )
{
	int tid = get_global_id( 0 );
	ushort tmp = as_ushort( src[ tid ] );
   dst[ tid ] = tmp;
}
