__kernel void test_odd_uchar8(__global uchar8 *srcA, __global uchar4 *dst)
{
    int  tid = get_global_id(0);

    uchar4 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
