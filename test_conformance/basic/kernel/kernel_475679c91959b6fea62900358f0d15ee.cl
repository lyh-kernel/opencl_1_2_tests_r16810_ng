__kernel void test_long_sub4(__global long4 *srcA, __global long4 *srcB, __global long4 *dst)
{
    int  tid = get_global_id(0);

    dst[tid] = srcA[tid] - srcB[tid];
}
