
__kernel void test_fn( __global ushort16 *src, __global short16 *dst )
{
	int tid = get_global_id( 0 );
	short16 tmp = as_short16( src[ tid ] );
   dst[ tid ] = tmp;
}
