__kernel void test_odd_int2(__global int2 *srcA, __global int *dst)
{
    int  tid = get_global_id(0);

    int tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
