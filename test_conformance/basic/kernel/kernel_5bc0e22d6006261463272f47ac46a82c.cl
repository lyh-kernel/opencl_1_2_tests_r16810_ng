
kernel void test(global uint *results, local ushort *mem0, local ushort2 *mem2, local ushort2 *mem3, local ushort4 *mem4, local ushort8 *mem8, local ushort16 *mem16)
{
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
