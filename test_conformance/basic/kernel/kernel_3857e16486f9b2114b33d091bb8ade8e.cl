
__kernel void test_fn( const __global char16 *src, __global char16 *dst, __local char16 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: char16charchar16
 int i;
 prefetch( (const __global char16*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
