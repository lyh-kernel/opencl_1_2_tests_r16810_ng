__kernel void test_odd_ushort16(__global ushort16 *srcA, __global ushort8 *dst)
{
    int  tid = get_global_id(0);

    ushort8 tmp = srcA[tid].odd;
    dst[tid] = tmp;
}
