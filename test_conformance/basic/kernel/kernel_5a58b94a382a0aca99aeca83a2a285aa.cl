__kernel void test_lo_ushort2(__global ushort2 *srcA, __global ushort *dst)
{
    int  tid = get_global_id(0);

    ushort tmp = ((ushort2)(srcA[tid].s0, srcA[tid].s1)).lo;
    dst[tid] = tmp;
}
