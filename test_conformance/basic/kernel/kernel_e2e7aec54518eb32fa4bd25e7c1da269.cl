
__kernel void test_fn( __global uchar4 *src, __global char *dst )
{
	int tid = get_global_id( 0 );
	char3 tmp = as_char3( src[ tid ] );
   vstore3(tmp,tid,dst);
}
