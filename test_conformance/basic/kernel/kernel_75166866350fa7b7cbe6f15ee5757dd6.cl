__kernel void test_hi_int4(__global int4 *srcA, __global int2 *dst)
{
    int  tid = get_global_id(0);

    int2 tmp = ((int4)(srcA[tid].s0, srcA[tid].s1, srcA[tid].s2, srcA[tid].s3)).hi;
    dst[tid] = tmp;
}
