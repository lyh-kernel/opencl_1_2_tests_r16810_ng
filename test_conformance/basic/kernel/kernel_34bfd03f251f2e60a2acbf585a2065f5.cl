
kernel void test(global uint *results)
{
   private int mem0[3];
   private int2 mem2[3];
   private int3 mem3[3];
   private int4 mem4[3];
   private int8 mem8[3];
   private int16 mem16[3];
   results[0] = (uint)&mem0[0];
   results[1] = (uint)&mem2[0];
   results[2] = (uint)&mem3[0];
   results[3] = (uint)&mem4[0];
   results[4] = (uint)&mem8[0];
   results[5] = (uint)&mem16[0];
}
