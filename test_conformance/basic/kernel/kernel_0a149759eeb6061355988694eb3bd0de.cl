
__kernel void test_fn( const __global uchar8 *src, __global uchar8 *dst, __local uchar8 *localBuffer, int copiesPerWorkgroup, int copiesPerWorkItem )
{
 // Ignore this: uchar8ucharuchar8
 int i;
 prefetch( (const __global uchar8*)(src+copiesPerWorkItem*get_global_id(0)), copiesPerWorkItem);
 for(i=0; i<copiesPerWorkItem; i++)
  dst[ get_global_id( 0 )*copiesPerWorkItem+i ] = src[ get_global_id( 0 )*copiesPerWorkItem+i ];
}
