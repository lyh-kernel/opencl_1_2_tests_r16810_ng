/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2010-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

/* test to make sure that cl_platform.h works standalone */
#if defined( __APPLE__ )
    #include <OpenCL/cl_platform.h>
#else
    #include <CL/cl_platform.h>
#endif
#include <stdio.h>

int main( void )
{
    printf("cl_platform.h standalone test PASSED.\n");
    return 0;
}
