/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2010-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

/* test to make sure that cl.h works standalone */
#if defined( __APPLE__ )
    #include <OpenCL/opencl.h>
#else
    #include <CL/opencl.h>
#endif
#include <stdio.h>

int main( void )
{
    printf("opencl.h standalone test PASSED.\n");
    return 0;
}
