/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2010-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

/* test to make sure that cl_gl.h works standalone */
#if defined( __APPLE__ )
    #include <OpenCL/cl_gl.h>
#else
    #include <CL/cl_gl.h>
#endif
#include <stdio.h>

int main( void )
{
    printf("cl_gl.h standalone test PASSED.\n");
    return 0;
}
