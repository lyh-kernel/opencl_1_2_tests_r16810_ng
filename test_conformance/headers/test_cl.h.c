/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2010-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/


/* test to make sure that cl.h works standalone */
#if defined( __APPLE__ )
    #include <OpenCL/cl.h>
#else
    #include <CL/cl.h>
#endif
#include <stdio.h>

int main( void )
{
    printf("cl.h standalone test PASSED.\n");
    return 0;
}
