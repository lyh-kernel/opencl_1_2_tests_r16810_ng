__kernel void select_uchar_char(__global uchar *dest, __global uchar *src1,
 __global uchar *src2, __global char *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
