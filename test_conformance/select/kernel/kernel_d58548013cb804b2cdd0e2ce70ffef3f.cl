__kernel void select_ulong16_ulong16(__global ulong16 *dest, __global ulong16 *src1,
 __global ulong16 *src2, __global ulong16 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
