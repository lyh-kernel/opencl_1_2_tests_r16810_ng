__kernel void select_uint2_uint2(__global uint2 *dest, __global uint2 *src1,
 __global uint2 *src2, __global uint2 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
