__kernel void select_short16_short16(__global short16 *dest, __global short16 *src1,
 __global short16 *src2, __global short16 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
