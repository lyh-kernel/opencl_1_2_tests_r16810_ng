__kernel void select_short4_short4(__global short4 *dest, __global short4 *src1,
 __global short4 *src2, __global short4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
