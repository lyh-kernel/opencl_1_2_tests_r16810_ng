__kernel void select_char_uchar(__global char *dest, __global char *src1,
 __global char *src2, __global uchar *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
