__kernel void select_ushort2_short2(__global ushort2 *dest, __global ushort2 *src1,
 __global ushort2 *src2, __global short2 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
