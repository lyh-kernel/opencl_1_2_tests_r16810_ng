__kernel void select_uint_int(__global uint *dest, __global uint *src1,
 __global uint *src2, __global int *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
