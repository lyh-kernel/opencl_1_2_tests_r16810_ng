__kernel void select_ushort_short(__global ushort *dest, __global ushort *src1,
 __global ushort *src2, __global short *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
