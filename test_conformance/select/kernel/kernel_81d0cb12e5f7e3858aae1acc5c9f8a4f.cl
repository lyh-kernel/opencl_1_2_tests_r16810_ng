__kernel void select_float8_uint8(__global float8 *dest, __global float8 *src1,
 __global float8 *src2, __global uint8 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
