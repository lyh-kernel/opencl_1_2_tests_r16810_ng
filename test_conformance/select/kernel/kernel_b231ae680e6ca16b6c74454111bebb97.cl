__kernel void select_ulong_ulong(__global ulong *dest, __global ulong *src1,
 __global ulong *src2, __global ulong *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
