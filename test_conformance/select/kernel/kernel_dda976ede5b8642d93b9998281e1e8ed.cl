__kernel void select_char2_uchar2(__global char2 *dest, __global char2 *src1,
 __global char2 *src2, __global uchar2 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
