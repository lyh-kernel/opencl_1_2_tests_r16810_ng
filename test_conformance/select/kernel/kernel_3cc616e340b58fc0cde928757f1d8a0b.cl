__kernel void select_float_int(__global float *dest, __global float *src1,
 __global float *src2, __global int *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
