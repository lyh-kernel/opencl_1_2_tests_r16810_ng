__kernel void select_float16_int16(__global float16 *dest, __global float16 *src1,
 __global float16 *src2, __global int16 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
