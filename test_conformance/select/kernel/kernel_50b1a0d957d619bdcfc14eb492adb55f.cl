__kernel void select_ulong2_ulong2(__global ulong2 *dest, __global ulong2 *src1,
 __global ulong2 *src2, __global ulong2 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
