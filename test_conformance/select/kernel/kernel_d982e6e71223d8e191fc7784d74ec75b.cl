__kernel void select_int_int(__global int *dest, __global int *src1,
 __global int *src2, __global int *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
