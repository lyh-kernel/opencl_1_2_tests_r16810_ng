__kernel void select_uint4_uint4(__global uint4 *dest, __global uint4 *src1,
 __global uint4 *src2, __global uint4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
