__kernel void select_int4_uint4(__global int4 *dest, __global int4 *src1,
 __global int4 *src2, __global uint4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
