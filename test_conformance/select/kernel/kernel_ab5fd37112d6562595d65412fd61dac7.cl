__kernel void select_ulong4_long4(__global ulong4 *dest, __global ulong4 *src1,
 __global ulong4 *src2, __global long4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
