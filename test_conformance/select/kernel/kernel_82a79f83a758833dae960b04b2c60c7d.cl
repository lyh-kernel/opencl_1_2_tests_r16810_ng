__kernel void select_ushort4_short4(__global ushort4 *dest, __global ushort4 *src1,
 __global ushort4 *src2, __global short4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
