__kernel void select_float2_int2(__global float2 *dest, __global float2 *src1,
 __global float2 *src2, __global int2 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
