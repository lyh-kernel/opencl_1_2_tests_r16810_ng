__kernel void select_long4_long4(__global long4 *dest, __global long4 *src1,
 __global long4 *src2, __global long4 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
