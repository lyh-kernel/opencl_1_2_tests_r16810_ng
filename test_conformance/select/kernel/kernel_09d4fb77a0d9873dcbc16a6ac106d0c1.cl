__kernel void select_char8_uchar8(__global char8 *dest, __global char8 *src1,
 __global char8 *src2, __global uchar8 *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
