__kernel void select_short_short(__global short *dest, __global short *src1,
 __global short *src2, __global short *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
