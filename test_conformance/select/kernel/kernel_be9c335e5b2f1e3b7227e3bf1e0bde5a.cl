__kernel void select_long_ulong(__global long *dest, __global long *src1,
 __global long *src2, __global ulong *cmp)
{
   size_t tid = get_global_id(0);
   if( tid < get_global_size(0) )
       dest[tid] = select(src1[tid], src2[tid], cmp[tid]);
}
