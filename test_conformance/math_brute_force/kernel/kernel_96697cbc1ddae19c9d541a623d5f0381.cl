__kernel void math_kernel2( __global float2* out, __global float2* in1, __global int2* in2 )
{
   int i = get_global_id(0);
   out[i] = ldexp( in1[i], in2[i] );
}
