__kernel void math_kernel2( __global float2* out, __global float2* in)
{
   int i = get_global_id(0);
   out[i] = rsqrt( in[i] );
}
