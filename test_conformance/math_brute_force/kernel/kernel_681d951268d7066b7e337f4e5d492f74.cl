__kernel void math_kernel2( __global float2* out, __global int2* out2, __global float2* in)
{
   int i = get_global_id(0);
   out[i] = lgamma_r( in[i], out2 + i );
}
