__kernel void math_kernel2( __global float2* out, __global float2* out2, __global float2* in)
{
   int i = get_global_id(0);
   out[i] = modf( in[i], out2 + i );
}
