__kernel void math_kernel4( __global float4* out, __global float4* in1, __global float4* in2,  __global float4* in3 )
{
   int i = get_global_id(0);
   out[i] = mad( in1[i], in2[i], in3[i] );
}
