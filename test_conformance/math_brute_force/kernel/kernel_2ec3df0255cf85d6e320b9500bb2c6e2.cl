__kernel void divide_kernel8( __global float8* out, __global float8* in1, __global float8* in2 )
{
   size_t i = get_global_id(0);
   out[i] =  in1[i] / in2[i];
}
