__kernel void math_kernel16( __global float16* out, __global int16* out2, __global float16* in1, __global float16* in2)
{
   int i = get_global_id(0);
   out[i] = remquo( in1[i], in2[i], out2 + i );
}
