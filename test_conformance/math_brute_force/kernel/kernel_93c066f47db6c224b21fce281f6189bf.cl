__kernel void math_kernel( __global int* out, __global float* in)
{
   int i = get_global_id(0);
   out[i] = ilogb( in[i] );
}
