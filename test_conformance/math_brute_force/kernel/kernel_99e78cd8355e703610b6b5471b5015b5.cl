__kernel void math_kernel4( __global int4* out, __global float4* in)
{
   int i = get_global_id(0);
   out[i] = isnormal( in[i] );
}
