__kernel void math_kernel8( __global float8* out, __global float8* in)
{
   int i = get_global_id(0);
   out[i] = half_tan( in[i] );
}
