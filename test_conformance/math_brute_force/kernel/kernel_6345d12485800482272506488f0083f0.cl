__kernel void math_kernel4( __global float4* out, __global float4* in1, __global float4* in2 )
{
   int i = get_global_id(0);
   out[i] = half_powr( in1[i], in2[i] );
}
