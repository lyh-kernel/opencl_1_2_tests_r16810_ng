__kernel void math_kernel4( __global float4* out, __global float4* in)
{
   int i = get_global_id(0);
   out[i] = atan( in[i] );
}
