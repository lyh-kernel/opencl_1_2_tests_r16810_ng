__kernel void add_kernel16( __global float16* out, __global float16* in1, __global float16* in2 )
{
   size_t i = get_global_id(0);
   out[i] =  in1[i] + in2[i];
}
