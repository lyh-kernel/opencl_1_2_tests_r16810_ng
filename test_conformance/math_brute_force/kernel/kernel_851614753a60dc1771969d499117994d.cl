__kernel void math_kernel4( __global float4* out, __global int4* out2, __global float4* in)
{
   int i = get_global_id(0);
   out[i] = frexp( in[i], out2 + i );
}
