__kernel void math_kernel( __global float* out, __global float* in1, __global int* in2 )
{
   int i = get_global_id(0);
   out[i] = rootn( in1[i], in2[i] );
}
