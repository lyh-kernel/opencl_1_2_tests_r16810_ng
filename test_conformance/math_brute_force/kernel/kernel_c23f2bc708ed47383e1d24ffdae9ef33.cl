__kernel void math_kernel8( __global float8* out, __global float8* out2, __global float8* in)
{
   int i = get_global_id(0);
   out[i] = fract( in[i], out2 + i );
}
