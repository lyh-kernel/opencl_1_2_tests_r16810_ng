__kernel void math_kernel16( __global float16* out, __global float16* in1, __global float16* in2,  __global float16* in3 )
{
   int i = get_global_id(0);
   out[i] = fma( in1[i], in2[i], in3[i] );
}
