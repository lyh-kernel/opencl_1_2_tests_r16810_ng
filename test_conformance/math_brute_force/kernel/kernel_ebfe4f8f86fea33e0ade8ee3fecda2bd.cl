__kernel void math_kernel( __global int* out, __global float* in1, __global float* in2 )
{
   int i = get_global_id(0);
   out[i] = isunordered( in1[i], in2[i] );
}
