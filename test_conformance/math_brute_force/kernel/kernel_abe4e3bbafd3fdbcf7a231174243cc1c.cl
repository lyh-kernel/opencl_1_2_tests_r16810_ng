__kernel void math_kernel( __global float* out, __global float* in1, __global float* in2,  __global float* in3 )
{
   int i = get_global_id(0);
   out[i] = fma( in1[i], in2[i], in3[i] );
}
