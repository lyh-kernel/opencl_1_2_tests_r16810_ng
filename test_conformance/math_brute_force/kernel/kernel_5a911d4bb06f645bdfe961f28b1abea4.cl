__kernel void add_kernel( __global float* out, __global float* in1, __global float* in2 )
{
   size_t i = get_global_id(0);
   out[i] =  in1[i] + in2[i];
}
