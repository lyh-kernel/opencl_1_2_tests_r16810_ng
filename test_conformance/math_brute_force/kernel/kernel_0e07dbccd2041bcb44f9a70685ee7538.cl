__kernel void math_kernel8( __global float8* out, __global int8* out2, __global float8* in)
{
   int i = get_global_id(0);
   out[i] = frexp( in[i], out2 + i );
}
