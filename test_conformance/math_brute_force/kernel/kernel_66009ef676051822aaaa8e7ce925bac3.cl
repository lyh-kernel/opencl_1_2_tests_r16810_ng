__kernel void math_kernel( __global float* out, __global float* in)
{
   int i = get_global_id(0);
   out[i] = sinh( in[i] );
}
