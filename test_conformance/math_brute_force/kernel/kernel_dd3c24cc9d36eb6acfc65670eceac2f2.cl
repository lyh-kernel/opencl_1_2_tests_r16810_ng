__kernel void math_kernel8( __global float8* out, __global int8* out2, __global float8* in1, __global float8* in2)
{
   int i = get_global_id(0);
   out[i] = remquo( in1[i], in2[i], out2 + i );
}
