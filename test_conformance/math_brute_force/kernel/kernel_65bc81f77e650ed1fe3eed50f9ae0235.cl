__kernel void math_kernel3( __global float* out, __global float* in, __global int* in2)
{
   size_t i = get_global_id(0);
   if( i + 1 < get_global_size(0) )
   {
       float3 f0 = vload3( 0, in + 3 * i );
       int3 i0 = vload3( 0, in2 + 3 * i );
       f0 = pown( f0, i0 );
       vstore3( f0, 0, out + 3*i );
   }
   else
   {
       size_t parity = i & 1;   // Figure out how many elements are left over after BUFFER_SIZE % (3*sizeof(float)). Assume power of two buffer size 
       float3 f0;
       int3 i0;
       switch( parity )
       {
           case 1:
               f0 = (float3)( in[3*i], NAN, NAN ); 
               i0 = (int3)( in2[3*i], 0xdead, 0xdead ); 
               break;
           case 0:
               f0 = (float3)( in[3*i], in[3*i+1], NAN ); 
               i0 = (int3)( in2[3*i], in2[3*i+1], 0xdead ); 
               break;
       }
       f0 = pown( f0, i0 );
       switch( parity )
       {
           case 0:
               out[3*i+1] = f0.y; 
               // fall through
           case 1:
               out[3*i] = f0.x; 
               break;
       }
   }
}
