__kernel void GetRoundingMode( __global int *out )
{
   volatile float a = 0x1.0p23f;
   volatile float b = -0x1.0p23f;
   out[0] = (a + 0x1.fffffep-1f == a) && (b - 0x1.fffffep-1f == b);
}
