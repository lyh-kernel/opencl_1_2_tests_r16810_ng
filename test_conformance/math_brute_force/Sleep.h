/******************************************************************
//
//  OpenCL Conformance Tests
// 
//  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
//
******************************************************************/


#ifndef SLEEP_H
#define SLEEP_H

void PreventSleep( void );
void ResumeSleep( void );

#endif /* SLEEP_H */


