__kernel void math_kernel16( __global float16* out, __global float16* out2, __global float16* in)
{
   int i = get_global_id(0);
   out[i] = sincos( in[i], out2 + i );
}
