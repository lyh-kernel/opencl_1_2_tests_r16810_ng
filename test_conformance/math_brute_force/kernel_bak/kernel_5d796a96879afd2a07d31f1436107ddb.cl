__kernel void math_kernel( __global float* out, __global float* out2, __global float* in)
{
   int i = get_global_id(0);
   out[i] = sincos( in[i], out2 + i );
}
