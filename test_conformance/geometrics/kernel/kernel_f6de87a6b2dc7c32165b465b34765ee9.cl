__kernel void sample_test(__global float *sourceA, __global float *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = fast_normalize( sourceA[tid] );

}
