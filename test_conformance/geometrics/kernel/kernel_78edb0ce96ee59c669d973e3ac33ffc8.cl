__kernel void sample_test(__global float4 *sourceA, __global float4 *sourceB, __global float *destValues)
{
    int  tid = get_global_id(0);
    destValues[tid] = dot( sourceA[tid], sourceB[tid] );

}
