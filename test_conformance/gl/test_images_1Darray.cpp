/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#include "common.h" 
#include "testBase.h"

#if defined( __APPLE__ )
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
    #include <CL/cl_gl.h>    
#endif

int test_images_read_1Darray( cl_device_id device, cl_context context, 
  cl_command_queue queue, int )
{
  size_t nformats = sizeof(common_formats) / sizeof(common_formats[0]);
  
  size_t sizes[] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
  size_t nsizes = sizeof(sizes) / sizeof(sizes[0]);
    
  GLenum targets[] = { GL_TEXTURE_1D_ARRAY };
  size_t ntargets = sizeof(targets) / sizeof(targets[0]);
  
  return test_images_read_common(device, context, queue, common_formats, 
      nformats, targets, ntargets, sizes, nsizes);
}

int test_images_write_1Darray( cl_device_id device, cl_context context, 
  cl_command_queue queue, int numElements )
{
  int error = 0;
  size_t i;
  const size_t nsizes = 6;
  sizevec_t sizes[nsizes];
  
  GLenum targets[] = { GL_TEXTURE_1D_ARRAY };
  size_t ntargets = sizeof(targets) / sizeof(targets[0]);
  size_t nformats = sizeof(common_formats) / sizeof(common_formats[0]);

  RandomSeed seed( gRandomSeed );

  // Generate some random sizes (within reasonable ranges)
  for (i = 0; i < nsizes; i++) {
    sizes[i].width  = random_in_range( 10, 4000, seed );
    sizes[i].height = random_in_range( 16, 512, seed );
    sizes[i].depth  = 1;
  }

  return test_images_write_common( device, context, queue, common_formats, 
    nformats, targets, ntargets, sizes, nsizes );
}

int test_images_1Darray_getinfo( cl_device_id device, cl_context context, 
  cl_command_queue queue, int )
{
  size_t nformats = sizeof(common_formats) / sizeof(common_formats[0]);
  
  size_t sizes[] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
  size_t nsizes = sizeof(sizes) / sizeof(sizes[0]);
    
  GLenum targets[] = { GL_TEXTURE_1D_ARRAY };
  size_t ntargets = sizeof(targets) / sizeof(targets[0]);
  
  return test_images_get_info_common(device, context, queue, common_formats, 
      nformats, targets, ntargets, sizes, nsizes);
}