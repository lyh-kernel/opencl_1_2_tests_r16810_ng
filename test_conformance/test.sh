#########################################################################
# File Name: test.sh
# Author: Qin Chen
# mail: qin@multicorewareinc.com
# Created Time: Wed 29 Oct 2014 02:05:08 PM CDT
#########################################################################
#!/bin/bash

#step1 translate
rm shaveRegister.h leonRegister.h *.o
for file in ./*.cl
do
  ${MXPA_ROOT_DIR}/scripts/mxpa.py $file
done

#step2 compile C output
for file in ./*.cl.c
do
  ${MXPA_BIN_DIR}/compiler/bin/clang -D_MXPA_QUERY_ -DREGISTER_KERNEL -std=c99 -fPIC -O3 -c -include-pch ${MXPA_BIN_DIR}/runtime/BIFL/BIFL_X64/opencl_runtime_release.h.pch -w $file
done

#step3 compile bitcode
echo "" > test.dummy.c
${MXPA_BIN_DIR}/compiler/bin/clang -emit-llvm -c -o test.dummy.bc test.dummy.c
for file in ./*.mxpa.bc
do
  ${MXPA_BIN_DIR}/compiler/bin/llvm-link test.dummy.bc $file -o $file
  ${MXPA_BIN_DIR}/compiler/bin/clang -D_MXPA_QUERY_ -DREGISTER_KERNEL -std=c99 -fPIC -O3 -c -include-pch ${MXPA_BIN_DIR}/runtime/BIFL/BIFL_X64/opencl_runtime_release.h.pch -w $file
done

#step4 compile leonRegister.c
#cp ${MXPA_ROOT_DIR}/scripts/kernel_compile/*.c .
#${MXPA_BIN_DIR}/compiler/bin/clang -D_MXPA_QUERY_ -DREGISTER_KERNEL -std=c99 -fPIC -O3 -c -include-pch ${MXPA_BIN_DIR}/runtime/BIFL/BIFL_X64/opencl_runtime_release.h.pch -w leonRegister.c 


