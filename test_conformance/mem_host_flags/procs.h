/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/


#ifndef __PROCS_H__
#define __PROCS_H__

#include "testBase.h"

#define NUM_FLAGS 4

extern int test_mem_host_read_only_buffer(cl_device_id deviceID, cl_context context,
                                          cl_command_queue queue, int num_elements);
extern int test_mem_host_read_only_subbuffer(cl_device_id deviceID, cl_context context,
                                             cl_command_queue queue, int num_elements);

extern int test_mem_host_write_only_buffer(cl_device_id deviceID, cl_context context,
                                           cl_command_queue queue, int num_elements);
extern int test_mem_host_write_only_subbuffer(cl_device_id deviceID, cl_context context,
                                              cl_command_queue queue, int num_elements);

extern int test_mem_host_no_access_buffer(cl_device_id deviceID, cl_context context,
                                          cl_command_queue queue, int num_elements);
extern int test_mem_host_no_access_subbuffer(cl_device_id deviceID, cl_context context,
                                             cl_command_queue queue, int num_elements);

extern int test_mem_host_read_only_image(cl_device_id deviceID, cl_context context,
                                         cl_command_queue queue, int num_elements);
extern int test_mem_host_write_only_image(cl_device_id deviceID, cl_context context,
                                          cl_command_queue queue, int num_elements);
extern int test_mem_host_no_access_image(cl_device_id deviceID, cl_context context,
                                         cl_command_queue queue, int num_elements);

#endif // #ifndef __PROCS_H__
