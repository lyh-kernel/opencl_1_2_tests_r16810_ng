/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#ifndef _setup_h
#define _setup_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gl_headers.h"
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif


// Note: the idea here is to have every platform define their own setup.cpp file that implements a GLEnvironment 
// subclass internally, then return it as a definition for GLEnvironment::Create

class GLEnvironment
{
	public:
		GLEnvironment() {}
		virtual ~GLEnvironment() {}

 		virtual int Init( int *argc, char **argv, int use_opengl_32 ) = 0;
		virtual cl_context CreateCLContext( void ) = 0;
		virtual int SupportsCLGLInterop( cl_device_type device_type) = 0;
	
		static GLEnvironment *	Instance( void );
	
	
};

#endif // _setup_h
