/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#ifndef _genericThread_h
#define _genericThread_h

#include <stdio.h>

class genericThread 
{
	public:

		virtual ~genericThread() {}
		
		bool	Start( void );
		void *	Join( void );
		
	protected:

		virtual void *	IRun( void ) = 0;
	
	private:

		void* mHandle;
	
		static void * IStaticReflector( void * data );
};

#endif // _genericThread_h

