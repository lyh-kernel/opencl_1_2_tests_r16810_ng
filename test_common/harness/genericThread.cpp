/******************************************************************
 //
 //  OpenCL Conformance Tests
 // 
 //  Copyright:	(c) 2008-2011 by Apple Inc. All Rights Reserved.
 //
 ******************************************************************/

#include "genericThread.h"

#if defined(_WIN32)
#include <windows.h>
#else // !_WIN32
#include <pthread.h>
#endif

void * genericThread::IStaticReflector( void * data )
{
	genericThread *t = (genericThread *)data;
	return t->IRun();
}

bool genericThread::Start( void )
{
#if defined(_WIN32)
    mHandle = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE) IStaticReflector, this, 0, NULL );
    return ( mHandle != NULL );
#else // !_WIN32
	int error = pthread_create( (pthread_t*)&mHandle, NULL, IStaticReflector, (void *)this );
	return ( error == 0 );
#endif // !_WIN32
}

void * genericThread::Join( void )
{
#if defined(_WIN32)
    WaitForSingleObject( (HANDLE)mHandle, INFINITE );
    return NULL;
#else // !_WIN32
	void * retVal;
	int error = pthread_join( (pthread_t)mHandle, &retVal );
	if( error != 0 )
		retVal = NULL;
	return retVal;
#endif // !_WIN32
}
